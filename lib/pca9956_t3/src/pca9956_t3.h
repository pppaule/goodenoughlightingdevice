// Library for LED-Controller PCA9956A on Teensy 3.2
// Started out on 280319 by paule@goodenoughlightingdevices
#include <Arduino.h>
#include <i2c_t3.h>

#ifndef __pca9965_t3__
#define __pca9965_t3__

#define     ALLPORTS        0xFF
#define     DEFAULT_PWM     1.0
#define     DEFAULT_CURRENT 0.1

class Pca9956_t3 {
 public:;
  Pca9956_t3(void);
  void set_brightness(uint8_t port, uint8_t value);
  void begin(void);
//  void hello(void
/** Name of the PCA9956A registers (for direct register access) */

enum command_reg {
    MODE1,         /**< MODE1 register      */
    MODE2,         /**< MODE2 register      */
    LEDOUT0,       /**< LEDOUT0 register    */
    LEDOUT1,       /**< LEDOUT1 register    */
    LEDOUT2,       /**< LEDOUT2 register    */
    LEDOUT3,       /**< LEDOUT3 register    */
    LEDOUT4,       /**< LEDOUT4 register    */
    LEDOUT5,       /**< LEDOUT5 register    */
    GRPPWM,        /**< GRPPWM register     */
    GRPFREQ,       /**< GRPFREQ register    */
    PWM0,          /**< PWM0 register       */
    PWM1,          /**< PWM1 register       */
    PWM2,          /**< PWM2 register       */
    PWM3,          /**< PWM3 register       */
    PWM4,          /**< PWM4 register       */
    PWM5,          /**< PWM5 register       */
    PWM6,          /**< PWM6 register       */
    PWM7,          /**< PWM7 register       */
    PWM8,          /**< PWM8 register       */
    PWM9,          /**< PWM9 register       */
    PWM10,         /**< PWM10 register      */
    PWM11,         /**< PWM11 register      */
    PWM12,         /**< PWM12 register      */
    PWM13,         /**< PWM13 register      */
    PWM14,         /**< PWM14 register      */
    PWM15,         /**< PWM15 register      */
    PWM16,         /**< PWM16 register      */
    PWM17,         /**< PWM17 register      */
    PWM18,         /**< PWM18 register      */
    PWM19,         /**< PWM19 register      */
    PWM20,         /**< PWM20 register      */
    PWM21,         /**< PWM21 register      */
    PWM22,         /**< PWM22 register      */
    PWM23,         /**< PWM23 register      */
    IREF0,         /**< IREF0 register      */
    IREF1,         /**< IREF1 register      */
    IREF2,         /**< IREF2 register      */
    IREF3,         /**< IREF3 register      */
    IREF4,         /**< IREF4 register      */
    IREF5,         /**< IREF5 register      */
    IREF6,         /**< IREF6 register      */
    IREF7,         /**< IREF7 register      */
    IREF8,         /**< IREF8 register      */
    IREF9,         /**< IREF9 register      */
    IREF10,        /**< IREF10 register     */
    IREF11,        /**< IREF11 register     */
    IREF12,        /**< IREF12 register     */
    IREF13,        /**< IREF13 register     */
    IREF14,        /**< IREF14 register     */
    IREF15,        /**< IREF15 register     */
    IREF16,        /**< IREF16 register     */
    IREF17,        /**< IREF17 register     */
    IREF18,        /**< IREF18 register     */
    IREF19,        /**< IREF19 register     */
    IREF20,        /**< IREF20 register     */
    IREF21,        /**< IREF21 register     */
    IREF22,        /**< IREF22 register     */
    IREF23,        /**< IREF23 register     */
    OFFSET = 0x3A, /**< OFFSET register     */
    SUBADR1,       /**< SUBADR1 register    */
    SUBADR2,       /**< SUBADR2 register    */
    SUBADR3,       /**< SUBADR3 register    */
    ALLCALLADR,    /**< ALLCALLADR register */
    PWMALL,        /**< PWMALL register     */
    IREFALL,       /**< IREFALL register    */
    EFLAG0,        /**< EFLAG0 register     */
    EFLAG1,        /**< EFLAG1 register     */
    EFLAG2,        /**< EFLAG2 register     */
    EFLAG3,        /**< EFLAG3 register     */
    EFLAG4,        /**< EFLAG4 register     */
    EFLAG5,        /**< EFLAG5 register     */

    REGISTER_START = MODE1,
    LEDOUT_REGISTER_START = LEDOUT0,
    PWM_REGISTER_START = PWM0,
    IREF_REGISTER_START = IREF0,
};

protected:
    enum {
//  DEFAULT_I2C_ADDR    = 0xC0,  //changed to 0x01
    DEFAULT_I2C_ADDR    = 0x01,
    AUTO_INCREMENT      = 0x80

    };

 private:
  uint8_t _i2caddr;
  char i2c_adress = DEFAULT_I2C_ADDR;
  void reset( void );

  /** Set the output duty-cycle, specified as a percentage (float)
   *
   * @param port  Selecting output port
   *    'ALLPORTS' can be used to set all port duty-cycle same value.
   * @param v     A floating-point value representing the output duty-cycle,
   *    specified as a percentage. The value should lie between
   *    0.0f (representing on 0%) and 1.0f (representing on 99.6%).
   *    Values outside this range will have undefined behavior.
   */
  void pwm( int, uint8_t);

      /** Register write (multiple bytes) : Low level access to device register
       *
       * @param data      Pointer to an array. First 1 byte should be the writing
       * start register address
       * @param length    Length of data
       */
  void write( char*, uint8_t);

  /** Register write (single byte) : Low level access to device register
   *
   * @param reg_addr  Register address
   * @param data      Value for setting into the register
   */
  void write( char , char);

  /** Register write (multiple bytes) : Low level access to device register
   *
   * @param reg_addr  Register address
   * @param data      Pointer to an array. The values are stored in this
   * array.
   * @param length    Length of data
   */
  void read( char, char *, int);

  /** Register read (single byte) : Low level access to device register
   *
   * @param reg_addr  Register address
   * @return          Read value from register
   */
  char read( char);

  void initialize(void);
  char pwm_register_access(int port);
  char current_register_access(int port);

  const int n_of_ports = 24; //hardcoded to make it work..
};



#endif
