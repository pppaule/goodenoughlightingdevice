// Library for PCA9956A LED-Controller
// on Teensy 3.2 (although there's nothing teensy specific to it)
// based on mbed library
// v.01 280319 by Daniel Pauselius (paule@goodenoughlightingdevices.net)


#include "pca9956_t3.h"

/*******
   instatiates new PCA9956A PWM LED DRIVER on adress '??'
 *******/

Pca9956_t3::Pca9956_t3() {
//  Wire.beginTransmission(0x20);
char init_array[] = {
    Pca9956_t3::AUTO_INCREMENT | REGISTER_START, //  Command
    0x00,
    0x00, //  MODE1, MODE2
    0xAA,
    0xAA,
    0xAA,
    0xAA,
    0xAA,
    0xAA, //  LEDOUT[5:0]
    0x80,
    0x00, //  GRPPWM, GRPFREQ
  };
}
//
void Pca9956_t3::begin() {
  Pca9956_t3::initialize();
}

void Pca9956_t3::initialize(void)
{
  //  pinMode(17,OUTPUT);     //PULLING RESET HIGH WORKS!
  //  digitalWrite(17, HIGH);
    char init_array[] = {
        Pca9956_t3::AUTO_INCREMENT | REGISTER_START, //  Command
        0x00,
        0x00, //  MODE1, MODE2
        0xAA,
        0xAA,
        0xAA,
        0xAA,
        0xAA,
        0xAA, //  LEDOUT[5:0]
        0x80,
        0x00, //  GRPPWM, GRPFREQ
    };

     write(init_array, sizeof(init_array));
    pwm(ALLPORTS, 188);
}

char Pca9956_t3::pwm_register_access(int port)
{
    if (port < n_of_ports)
        return (PWM_REGISTER_START + port);
    return (PWMALL);
}

// char PCA9956A::current_register_access(int port)
// {
//     if (port < n_of_ports)
//         return (IREF_REGISTER_START + port);
//
//     return (IREFALL);
// }
//
// int PCA9956A::number_of_ports(void) { return (n_of_ports); }

// pwm-output to led n_of_port

void Pca9956_t3::pwm( int port, uint8_t value )
{
    char    reg_addr;
    reg_addr    = pwm_register_access( port );
    write( reg_addr, value);
}
void Pca9956_t3::set_brightness(uint8_t port, uint8_t value){
  pwm(port, value);
}
//
// void PCA995xA::pwm( float *vp )
// {
//     int     n_of_ports  = number_of_ports();
//     char    *data = new char[ n_of_ports + 1 ];
//
//     *data    = pwm_register_access( 0 );
//
//     for ( int i = 1; i <= n_of_ports; i++ )
//         data[ i ]   = (char)(*vp++ * 255.0);
//
//     write( data, n_of_ports+1);
//     delete data;
// }
//i2c_write function to send 1 byte

// reset function

void Pca9956_t3::reset( void ) {
    char    v   = 0x06;
    Wire.beginTransmission(i2c_adress);
    //  i2c.write( 0x00, &v, 1 );  // this is original function - seems to me that 0x00 is hardcoded standard adress
    Wire.write( &v, 1);
    Wire.endTransmission();
}

void Pca9956_t3::write( char* data, uint8_t length) {
uint8_t addr = Pca9956_t3::DEFAULT_I2C_ADDR;
*data   |= AUTO_INCREMENT;  //adding AUTO_INCREMENT to MODE1-REG
Wire.beginTransmission(i2c_adress);
Wire.write(data, length );
Wire.endTransmission();
//  write(const uint8_t* data, size_t count);
}

// i2c_write function to send multiple bytes

void Pca9956_t3::write( char reg_addr, char data )
{
    char    c[2];
    c[0]    = reg_addr;
    c[1]    = data;
    Wire.beginTransmission(i2c_adress);
    Wire.write(c, sizeof(c));
    Wire.endTransmission();
}

// i2c_read function to read multiple bytes

void Pca9956_t3::read( char reg_addr, char *data, int length ) {
    reg_addr    |= 0x80;
    Wire.beginTransmission(i2c_adress);
    Wire.write((char *)(&reg_addr), length);
    Wire.read(data, length);
    Wire.endTransmission();
}

// i2c_read function to read single byte

char Pca9956_t3::read( char reg_addr ) {
  Wire.beginTransmission(i2c_adress);
  Wire.write((char *)(&reg_addr), 1);
  Wire.read((char *)(&reg_addr), 1);
  Wire.endTransmission();
 }
//pwm(ALLPORTS, 0.0);
//current(ALLPORTS, 0.1);

//write(init_array, sizeof(init_array));
//}
