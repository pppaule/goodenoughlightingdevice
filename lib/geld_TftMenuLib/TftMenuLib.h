/*

Paule's TFT Menu Lib - Menufuehrung fuer die Good Enough Lighting device

*/
#include "Arduino.h"
#include <ILI9341_t3.h>
#include <font_LiberationMono.h>
#include <font_LiberationSans.h>
//#include <Encoder.h>
// geld 1.0
//#define ENC_A  32
//#define ENC_B  33

#define ENC_A  14
#define ENC_B  15

#ifndef tftMenuLib_h
#define tftMenuLib_h
/*
#define FONT_6 LiberationMono_6
#define FONT_7 LiberationMono_7
#define FONT_8 LiberationMono_8
#define FONT_9 LiberationMono_9
#define FONT_10 LiberationMono_10
#define FONT_12 LiberationMono_12
*/

#define FONT_6 LiberationSans_6
#define FONT_7 LiberationSans_7
#define FONT_8 LiberationSans_8
#define FONT_9 LiberationSans_9
#define FONT_10 LiberationSans_10
#define FONT_12 LiberationSans_12

#define HEADER_FONT  FONT_12
#define MENU_FONT FONT_12

#define HEADER_TXT_X 0
#define HEADER_TXT_Y 5
#define MENU_TXT_X 0
#define MENU_TXT_OFFSET_Y 30

#define MENU_TXT_COLOR GELD_YELLOW
#define MENU_BG_COLOR GELD_BLACK
#define MENU_HIGHLIGHT_COLOR GELD_BLUE
#define MENU_BORDER_COLOR GELD_GREEN

#define MENU_HEADER_TXT_COLOR GELD_YELLOW
#define MENU_HEADER_TXT_BG_COLOR GELD_BLACK

#define ALERT_BORDER_COLOR GELD_RED
#define ALERT_BG_COLOR GELD_BLACK
#define ALERT_FONT FONT_10
#define ALERT_FONT_COLOR GELD_WHITE


#define MAXLETTERS   24
#define MAXITEMS 12

//UI MACROS

#define ENCODER_UP 				(newEncPosition < (oldEncPosition-1))
#define ENCODER_DOWN			(newEncPosition > (oldEncPosition+1))
#define RESET_ENCODER 			oldEncPosition = newEncPosition

#define CHN_BUTTON_PRESSED(x)	(buttVal[x] && !old_buttVal[x])
#define RESET_CHN_BUTTON(x) 	old_buttVal[x] = buttVal[x]
#define BUTTON_PRESSED(x)   	(userVal[x] && !old_userVal[x])
#define RESET_BUTTON(x) 		old_userVal[x] = userVal[x]
// geld2019
// #define SELECT 6
// #define EXIT 7
// #define SHIFT 5
// geld2020
#define SELECT 7
#define EXIT 6
#define SHIFT 5

//Y Positionen verschiedener Menupunkte
#define MENU_LINE_HEIGTH 24
#define MENU_TOP 30   // Position of first menu item from top of screen
#define MENU_SUB_CHN_X  42
#define MENU_TITLE_SUB_CHN 54
#define MENU_TOP_SUB_CHN 84

#define MENU_SUB_CHN_RECTANGLE_X 54
#define MENU_SUB_CHN_RECTANGLE_Y 230
#define MENU_SUB_CHN_RECTANGLE_LEN 40
// LAYOUT INIT

#define offsetChnX  5
#define offsetChnY  17
#define offsetValX  15
#define offsetValY  70
#define textsizeChn  2
#define textsizeVal  3
#define textsizeChnName  1
#define textsizeDeviceName  1
#define lineLengthChn  71
#define offsetDevNamX  5
#define offsetDevNamY  35
#define offsetChnNamX  35
#define offsetChnNamY  5

#define TEXTLINEHEGHT 120 // in percent
#define MAXCHANNEL 512

#define GELD_BLACK       0x0000      /*   0,   0,   0 */
#define GELD_NAVY        0x000F      /*   0,   0, 128 */
#define GELD_DARKGREEN   0x03E0      /*   0, 128,   0 */
#define GELD_DARKCYAN    0x03EF      /*   0, 128, 128 */
#define GELD_MAROON      0x7800      /* 128,   0,   0 */
#define GELD_PURPLE      0x780F      /* 128,   0, 128 */
#define GELD_OLIVE       0x7BE0      /* 128, 128,   0 */
#define GELD_LIGHTGREY   0xC618      /* 192, 192, 192 */
#define GELD_DARKGREY    0x7BEF      /* 128, 128, 128 */
#define GELD_BLUE        0x001F      /*   0,   0, 255 */
#define GELD_GREEN       0x07E0      /*   0, 255,   0 */
#define GELD_CYAN        0x07FF      /*   0, 255, 255 */
#define GELD_RED         0xF800      /* 255,   0,   0 */
#define GELD_MAGENTA     0xF81F      /* 255,   0, 255 */
#define GELD_YELLOW      0xFFE0      /* 255, 255,   0 */
#define GELD_WHITE       0xFFFF      /* 255, 255, 255 */
#define GELD_ORANGE      0xFD20      /* 255, 165,   0 */
#define GELD_GREENYELLOW 0xAFE5      /* 173, 255,  47 */
#define GELD_PINK        0xF81F


#define GELD_GREY1					0x2945
#define GELD_GREY2					0x4A49
#define GELD_GREY3					0x7c50
#define GELD_RED2           0xF9A0

#define UP      0
#define DOWN    1


class TftMenuLib {

public:
	TftMenuLib(char **menu_items, uint8_t numMenuItems, ILI9341_t3 * tft);
	TftMenuLib(char **menu_items, void (**menu_funcs)(), uint8_t numMenuItems, ILI9341_t3 * tft);
	TftMenuLib(int x, int y, int w, int h, int cursX, int cursY, char **menuItems, void (**menu_funcs)(), uint8_t numMenuItems, ILI9341_t3 * tft);
	TftMenuLib(int x, int y, int w, int h, int cursX, int cursY, char **menuItems, void (**menu_funcs)(int), uint8_t numMenuItems, ILI9341_t3 * tft);
  ILI9341_t3 *getTft();
  typedef void (* MenuFuncs) ();

  uint16_t menu_select = 1;
  uint16_t _numItems;
  uint16_t selectedChannel = 0;
	uint16_t maxLines_submenu = 5;

  void menuHeader();
  void menuWindowInit() ;
  void menuWindow();
  void menuListInit(); /*{const char[MAXLETTERS] localMenuItem}*/;
  void menuListInit(uint16_t cursorX, uint16_t cursorY, uint16_t lineHeight = 20);
  void menuList();
	void drawList_window(uint16_t startItem);
	void setFontSize(uint16_t fontsize);
  void menuListSelect();
	void menuListSelect(boolean direction);  //Macro UP= 0 und DOWN = 1
	void menuListSelect_window(boolean direction);  //Macro UP= 0 und DOWN = 1
	void menuListSelect_window();
  void alertWindow(const char* text2Print, int x, int y, int w, int h, int cursorX, int cursorY);
  void alertWindow(const char* text, const char* text2ndRow, int x, int y, int w, int h, int cursorX, int cursorY);
  //void menuWindow(int x, int y, int w, int h, int cursorX, int cursorY, TftMenuLib *parentClass);
  void setSelectedChannel(uint16_t selectedChannel);
  void setNumMenu(uint8_t numMenu) ;
  void setTextColor(uint16_t foreColor, uint16_t backColor);
  void comeBack();
	void reset_submenu(void);
//  char **_pMenuItems;
protected:

  ILI9341_t3 *_tft;

//  Encoder *_encoder(ENC_A, ENC_B)
  void _menuWindowInit();

    void lastItemRect(uint8_t lastItem);
    void selectedItemRect(uint8_t selectedMenuItem);
		void lastItemRect_window(uint8_t lastItem);
    void selectedItemRect_window(uint8_t selectedMenuItem);

   void (**_pMenuFuncs)();
  char **_pMenuItems;
  boolean _menu_init = 0;
  unsigned int _screenWidth;
  unsigned int _screenHeight;

  unsigned int _x = 0;
  unsigned int _y = 0;
  unsigned int _w = 0;
  unsigned int _h = 0;
  unsigned int _cursX = 0;
  unsigned int _cursY = 0;
  uint16_t _lineHeight;
	uint16_t _maxLines_per_window = 7; // submenu-window Maxlines + 2 lines for header

private:
	uint16_t _startItem;
	uint16_t _endItem;
	bool _submenu_init = false; //always reset when leaving submenu!
};

#endif
