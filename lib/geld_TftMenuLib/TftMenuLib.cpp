/*

  TFTMENULIB - Anzeigelibrary fuer good enough lighting device

*/

#include <Arduino.h>
#include <TftMenuLib.h>
#include <ILI9341_t3.h>
#include <Encoder.h>
#include <font_LiberationMono.h>


//numMenu noch in die library

TftMenuLib::TftMenuLib(char **menuItems, uint8_t numMenuItems, ILI9341_t3 * tft) {
  _tft = tft;
  _screenWidth = _tft->width();
  _screenHeight = _tft->height();
  _numItems = numMenuItems;
  _pMenuItems = menuItems;
}
TftMenuLib::TftMenuLib(char **menuItems, void (**menu_funcs)(), uint8_t numMenuItems, ILI9341_t3 * tft) {
  _tft = tft;
  _screenWidth = _tft->width();
  _screenHeight = _tft->height();
  _numItems = numMenuItems;
  _pMenuItems = menuItems;
 _pMenuFuncs = menu_funcs;

}
TftMenuLib::TftMenuLib(int x, int y, int w, int h, int cursX, int cursY, char **menuItems, void (**menu_funcs)(), uint8_t numMenuItems, ILI9341_t3 * tft) {
   _x = x; _y = y; _w = w; _h = h; _cursX = cursX; _cursY=cursY;
  _tft = tft;
  _screenWidth = _tft->width();
  _screenHeight = _tft->height();
  _numItems = numMenuItems;
  _pMenuItems = menuItems;
 _pMenuFuncs = menu_funcs;

}
TftMenuLib::TftMenuLib(int x, int y, int w, int h, int cursX, int cursY, char **menuItems, void (**menu_funcs)(int), uint8_t numMenuItems, ILI9341_t3 * tft) {
   _x = x; _y = y; _w = w; _h = h; _cursX = cursX; _cursY=cursY;
  _tft = tft;
  _screenWidth = _tft->width();
  _screenHeight = _tft->height();
  _numItems = numMenuItems;
  _pMenuItems = menuItems;
 // _pMenuFuncs = menu_funcs; // hab grade keine ahnung, wie man hier funktionen mit argumenten verdillert

}
void TftMenuLib::setSelectedChannel(uint16_t selectedChn) {
  selectedChannel = selectedChn;

}
void TftMenuLib::setNumMenu(uint8_t numMenu) {
  _numItems = numMenu;
}
void TftMenuLib::menuHeader(){
    _tft -> setCursor(HEADER_TXT_X, HEADER_TXT_Y);
    _tft -> setFont(HEADER_FONT);
    _tft -> setTextColor(MENU_HEADER_TXT_COLOR, MENU_BG_COLOR);
    _tft -> print(_pMenuItems[0]);
    _tft -> drawLine(0, 21, _tft -> width()-1, 21, GELD_GREEN);
    //for(unsigned int i = 0; i < strlen(_pMenuItems[0]); i++) _tft -> print(_pMenuItems[0] );
}

void TftMenuLib::menuListInit(){
      _tft -> fillScreen(GELD_BLACK);
      TftMenuLib::menuHeader();
      _tft -> setCursor(MENU_TXT_X, MENU_TOP);
      _tft -> setFont(MENU_FONT);
      _tft -> setTextColor(MENU_TXT_COLOR , MENU_BG_COLOR);

      _w = _tft -> width()/2+12;
      for ( uint8_t i = 1; i < _numItems ; i++) {      //HIER EIN HINWEIS AUF EVENTUELLEN FEHLER IN numItems?
        _tft -> setCursor (0, ((i - 1) * MENU_LINE_HEIGTH) + MENU_TOP);
        _tft -> print(_pMenuItems[i]);
      }
      //_tft -> fillRect (0, 228, 320, 12, ILI9341_WHITE );
}

void TftMenuLib::menuListInit(uint16_t cursorX, uint16_t cursorY, uint16_t lineHeight){
	_cursX = cursorX;
	_cursY = cursorY;
	_lineHeight = lineHeight;

	_tft -> fillScreen(GELD_BLACK);
	TftMenuLib::menuHeader();
	//_tft -> setCursor(_cursX, _cursY);
	_tft -> setFont(MENU_FONT);
	_tft -> setTextColor(MENU_TXT_COLOR , MENU_BG_COLOR);

	_w = _tft -> width()/2+12;
	for ( uint8_t i = 1; i < _numItems ; i++) {      //HIER EIN HINWEIS AUF EVENTUELLEN FEHLER IN numItems?
		_tft -> setCursor (_cursX, ((i - 1) * _lineHeight) + _cursY + MENU_TOP);
		_tft -> print(_pMenuItems[i]);
	}
}
void TftMenuLib::alertWindow(const char* text2Print, int x, int y, int w, int h, int cursorX, int cursorY) {

    //_x = x; _y = y; _w = w; _h = h; _cursX = cursorX; _cursY = cursorY;

    _tft -> fillRect(x, y, w, h, ALERT_BG_COLOR);
    _tft -> drawRect(x, y, w, h, ALERT_BORDER_COLOR);
    _tft -> setFont(MENU_FONT);
    _tft -> setTextColor(MENU_TXT_COLOR , ALERT_BG_COLOR);
    _tft -> setCursor(x + cursorX, y + cursorY);
    _tft -> print(text2Print);

}
void TftMenuLib::alertWindow(const char* text, const char* text2ndRow, int x, int y, int w, int h, int cursorX, int cursorY) {

      //_x = x; _y = y; _w = w; _h = h; _cursX = cursorX; _cursY = cursorY;

      //_tft -> print(text2ndRow);

      _tft -> fillRect(x, y, w, h, ALERT_BG_COLOR);
      _tft -> drawRect(x, y, w, h, ALERT_BORDER_COLOR);
      _tft -> setFont(MENU_FONT);
      _tft -> setTextColor(MENU_TXT_COLOR , MENU_BG_COLOR);
      _tft -> setCursor(x + cursorX, y +cursorY);
      _tft -> print(text);
      _tft -> setCursor(x + cursorX, y + cursorY + 22);
      _tft -> print(text2ndRow);

}

void TftMenuLib::comeBack() {
  menuListInit();
  menuListSelect();
}
void TftMenuLib::lastItemRect(uint8_t lastItem){
  signed char dirInt;
  _tft -> fillRect(_x + _cursX, _y + _cursY +((lastItem - 1 )*MENU_LINE_HEIGTH) + MENU_TOP - 3, _w - 20, 21, MENU_BG_COLOR);
  _tft -> setCursor(_x + _cursX, _y + _cursY + ((lastItem  - 1 )*MENU_LINE_HEIGTH )+ MENU_TOP);
  _tft -> setTextColor(MENU_TXT_COLOR, MENU_BG_COLOR);
  _tft -> println(_pMenuItems[lastItem]);

}
void TftMenuLib::selectedItemRect(uint8_t selectedMenuItem) {
  _tft -> fillRect(_x + _cursX, _y + _cursY + ((selectedMenuItem - 1)*MENU_LINE_HEIGTH)+MENU_TOP-3, _w -20, 21, MENU_HIGHLIGHT_COLOR);
  _tft -> setCursor(_x + _cursX, _y + _cursY +((selectedMenuItem - 1)*MENU_LINE_HEIGTH)+MENU_TOP);
  _tft -> setTextColor(MENU_TXT_COLOR, MENU_HIGHLIGHT_COLOR);
  _tft -> println(_pMenuItems[selectedMenuItem]);
}

void TftMenuLib::menuListSelect() {   //OVERLOADED Func if coming back from sub-routine to draw selection
    _tft -> setFont(MENU_FONT);
    selectedItemRect_window(menu_select);
}




// suffix "_window" means overworked functions for submenu-Windows, like in console or patchbay
// _window implements scrolling for more than 5 items


void TftMenuLib::reset_submenu(void) {
  _submenu_init = false;
}
void TftMenuLib::menuWindowInit() {

  //static booolean submenuInit = false;
      if (!_submenu_init) {
        _startItem = 1;
        _submenu_init = true;
      }
      setFontSize(12);
      int lines_to_draw;
      //_startItem = 1;
      _endItem = _startItem + maxLines_submenu;
      lines_to_draw = maxLines_submenu + 1;

      int hVar = _h + 12 + lines_to_draw * MENU_LINE_HEIGTH;
      _tft -> fillRect(_x, _y, _w, hVar, MENU_BG_COLOR);
      _tft -> drawRect(_x, _y, _w, hVar, MENU_BORDER_COLOR);
      _tft -> setCursor(_x + _cursX, _y + _cursY - 4);
      _tft -> setFont(HEADER_FONT);
      _tft -> setTextColor(MENU_HEADER_TXT_COLOR, MENU_BG_COLOR);
      _tft -> print(_pMenuItems[0]);
      if (selectedChannel != 0) {
        _tft -> printf("   %3d", selectedChannel );
      }
      int lineStartX =_x;
      int lineStartY =_y+ MENU_LINE_HEIGTH + 16;
      int lineEndX = lineStartX + _w;
      int lineEndY = lineStartY;

      _tft -> drawLine(lineStartX, lineStartY, lineEndX, lineEndY, MENU_BORDER_COLOR);
      drawList_window(_startItem);

}

void TftMenuLib::drawList_window(uint16_t startItem){
  uint16_t windowLength;
  if (_numItems > maxLines_submenu) windowLength = maxLines_submenu + 2;
  else windowLength = _numItems;   // beware to not write more than there is in menuitems_array!!

  for ( uint16_t i = _startItem  ; i < windowLength  ; i++) {      //HIER EIN HINWEIS AUF EVENTUELLEN FEHLER IN numItems?

    _tft -> setCursor (_x + _cursX, _y + ((i - 1) * MENU_LINE_HEIGTH) + MENU_TOP + _cursY);  // list gets printed, starting from startItem to liinestodraw
    _tft -> print(_pMenuItems[i]);
  }
}

void TftMenuLib::lastItemRect_window(uint8_t lastItem){
  signed char dirInt;
  _tft -> fillRect(_x + _cursX, _y + _cursY +((lastItem - 1 )*MENU_LINE_HEIGTH) + MENU_TOP - 3, _w - 30, 21, MENU_BG_COLOR);
  _tft -> setCursor(_x + _cursX, _y + _cursY + ((lastItem  - 1 )*MENU_LINE_HEIGTH )+ MENU_TOP);
  _tft -> setTextColor(MENU_TXT_COLOR, MENU_BG_COLOR);
  _tft -> println(_pMenuItems[lastItem]);
}

void TftMenuLib::selectedItemRect_window(uint8_t selectedMenuItem) {
  _tft -> fillRect(_x + _cursX, _y + _cursY + ((selectedMenuItem - 1)*MENU_LINE_HEIGTH)+MENU_TOP-3, _w -30, 21, MENU_HIGHLIGHT_COLOR);
  _tft -> setCursor(_x + _cursX, _y + _cursY +((selectedMenuItem - 1)*MENU_LINE_HEIGTH)+MENU_TOP);
  _tft -> setTextColor(MENU_TXT_COLOR, MENU_HIGHLIGHT_COLOR);
  _tft -> println(_pMenuItems[selectedMenuItem]);
}
void TftMenuLib::menuListSelect_window() {   //OVERLOADED Func if coming back from sub-routine to draw selection
    setFontSize(12);
    selectedItemRect_window(menu_select);
}

void TftMenuLib::menuListSelect_window(boolean direction) {
  setFontSize(12);
  uint8_t lastItem = menu_select;

  if (direction == UP) {
	  if (menu_select > _startItem) {
	    menu_select--;
	  }
	}
  if (menu_select != 0 ){    //menu init
	  if (!_menu_init) {
	    menu_select = 1;
	    _menu_init = true;
	   }
   }

	if (direction == DOWN) {
  if (menu_select <= _endItem) {
      menu_select++;
	  }
	}

	if (menu_select < 1 ) {
    menu_select = _startItem;
  }

  if (menu_select > _endItem){     // at end of window, insert followup window here!
    _startItem++;
    _endItem++;
    //menuWindowInit();
    //drawList_window(_startItem);
    menu_select = _endItem;
    }

  if (lastItem != menu_select)  {
    lastItemRect_window(lastItem);
    selectedItemRect_window(menu_select);
  }
}

void TftMenuLib::menuListSelect(boolean direction) {
  _tft -> setFont(MENU_FONT);
  uint8_t lastItem = menu_select;
	if (direction == UP) {
	  if (menu_select > 1) {
	    menu_select--;
	  }
	}

	if (direction == DOWN) {
	  if (menu_select <= _numItems) {
	    menu_select++;
	  }
	}
// static boolean menuEnd = 0;

	if (menu_select < 1 ) {
    menu_select = 1;
    //lastItemRect(lastItem);
    //selectedItemRect(menu_select);
  //  menuEnd = 1;

  }

  if (menu_select >= _numItems-1){
     menu_select = _numItems-1;
     //lastItemRect(lastItem);
     //selectedItemRect(menu_select);
  //   menuEnd = 1;

   }
  if (menu_select != 0 ){
	  if (!_menu_init) {
	    menu_select = 1;
	    _menu_init = 1;
	   }
   }
  if (lastItem != menu_select)  {
    lastItemRect(lastItem);
    selectedItemRect(menu_select);
  }
  //   menuEnd = 0;
    //  if (menu_select == 1 && direction == UP || menu_select ==_numItems -1 && direction == DOWN) {  // wenn am Ende oder anfang der liste bitte nicht auf encoderr reagieren.
    //  } else {
	  //      lastItemRect(lastItem);
	  //      selectedItemRect(menu_select);
    // //  }

}

void TftMenuLib::setFontSize(uint16_t fontsize){
  switch (fontsize){
  case(8):
  _tft -> setFont(FONT_8);
  break;
  case(9):
  _tft -> setFont(FONT_9);
  break;
  case(10):
  _tft -> setFont(FONT_10);
  break;
  case(12):
  _tft -> setFont(FONT_12);
  break;
  default:
  _tft -> setFont(FONT_10);
  break;
  }
}
