
/*
  Console.lib  Anzeigelibrary fuer G.E.L.D. / Hier sind die ganzen Conslenoberflaechen und so drinne
*/

#include <Arduino.h>
#include <consoleLib.h>
#include <ILI9341_t3.h>
#include <font_LiberationMono.h> // for better printTextAt()-function
#include <font_LiberationSans.h>

Console::Console(ILI9341_t3 *tft) {
  _tft = tft;
  _screenWidth = _tft->width();
  _screenHeight = _tft->height();
}

void Console::setScreenColor(uint16_t screenColor) {
  _screenColor = screenColor;
}
uint16_t Console::getScreenColor(void) {
  return _screenColor;
}

void Console::set_init_flag(bool flag) {
  _init = flag;
}

void Console::printHeader(const char* text) {
	//  - clears/resets screen
	//	- prints headline
	_tft -> fillScreen(ILI9341_BLACK);
	Console::printTextAt(0, 5, text, ILI9341_GREEN, ILI9341_BLACK, 10);
	_tft -> drawLine(0, 19, _tft -> width()-1, 19, ILI9341_GREEN);
//	_tft -> fillRect (0, 228, 320, 12, ILI9341_BLUE );
}

/*  ---------------------
	Slider-Bar handling:
	---------------------
	- pre-requisites:
		- in headerTypeDefinitions.h is type-definition for a SliderBar object, SliderBar_t, with attributes:
			char barTextAbove[7]; // text above bar
			char barTextBesides[12]; // text besides bar
			char barTextBelow[7]; // text below bar
			uint8_t barValue; // bar height [0..255]
		- in headers.h is global definition of an array of SliderBar_t objects, containing as many objects as there are faders:
			SliderBar_t gDisplaySliderBars[NUM_FADER];
	- config functions:
		* setBarColor(uint16_t foreColor, uint16_t backColor)
			- sets _foreColor and _backColor
		* setBarDimensions(uint16_t width, uint16_t height [, uint16_t radius])
			- sets _w, _h (and may be _radius)
		* setBarCoordinates(uint16_t x, uint16_t y)
			- sets x,y coordinates of first bar (= reference point for all bars)
	- display functions:
		* initAndDisplay_SliderBarGroup(uint8_t numSliderBars, char * header)
			- if called with header text: clears screen and displays header
			- calls refresh_SliderBarGroup() with number of bars to display [0..sizeof(gDisplaySliderBars)-1]
		* refresh_SliderBarGroup(uint8_t numSliderBars)
			- calls numSliderBars times refresh_SliderBar_inGroup()
		* refresh_SliderBar_inGroup(uint8_t sliderBarID)
			- draws/refreshes one bar out of gDisplaySliderBars[] (texts and bar-value)
		* sliderBar ( uint8_t value, uint16_t x, uint16_t y)
			- draws a slider bar at <x>, <y>, with fill state <value>
	- data functions:
		* reset_gDisplaySliderBars()
			- beats gDisplaySliderBars[] back to init-state
		* update_gDisplaySliderBars(uint16_t channelStartOffset, int itemSpecifier)
			- resets gDisplaySliderBars[]
			- calls update_singleBar_in_gDisplaySliderBars() for all elements of gDisplaySliderBars[]
		* update_singleBar_in_gDisplaySliderBars(uint16_t channelStartOffset, uint8_t sliderBarID, int itemSpecifier)
			- updates single element/sliderBar in array gDisplaySliderBars[], according to chosen view [global variable gThisView]
			- gThisView
				- must be set in calling skript (console.ino, patchBay.ino...)
				- value handling must be wrapped in an "if gThisView == XY" exception in this function. (see there for examples)
*/
// Slider-Bar handling, config functions:
void Console::setBarColor(uint16_t foreColor, uint16_t backColor) {
	_foreColor = foreColor;
	_backColor = backColor;
}
void Console::setBarDimensions(uint16_t width, uint16_t height) {
	_w = width;
	_h = height;
}
void Console::setBarDimensions(uint16_t width, uint16_t height, uint16_t radius) {
	_w = width;
	_h = height;
	_radius= radius;
}
void Console::setBarCoordinates(uint16_t x, uint16_t y){
  _barX = x;
  _barY = y;
}

uint16_t Console::getCoordinate_x(){
  return _barX;
}

uint16_t Console::getCoordinate_y(){
  return _barY;
}
uint16_t Console::getSliderWidth() {
  return _w;
}

uint16_t Console::getSliderHeight() {
  return _h;
}
// Slider-Bar handling, display functions:
void Console::initAndDisplay_SliderBarGroup(uint8_t numSliderBars, char * header) {
	// - if called with header text: clears screen and displays header
	// - calls refresh_SliderBarGroup() with number of bars to display [0..sizeof(gDisplaySliderBars)-1]
	if(header) Console::printHeader(header);
	Console::refresh_SliderBarGroup(numSliderBars);
}
void Console::refresh_SliderBarGroup(uint8_t numSliderBars) {
	// calls numSliderBars times refresh_SliderBar_inGroup()
	// numSliderBars: number of array elements to use (from gDisplaySliderBars[])
	for(int i = 0; i < NUM_FADER; i++){
		if(i>=numSliderBars) break; // stop if there are less than 8 bars to display
		if(!gDisplaySliderBars[i].barTextBesides) break; // stop if this (and following) channel is unused
		Console::refresh_SliderBar_inGroup(i);
	}
}

void Console::refresh_SliderBar_inGroup(uint8_t sliderBarID) {
	// draws/refreshes one bar out of gDisplaySliderBars[] (texts and bar-value)
	// sliderBarID: ID of array element to refresh
	uint16_t xValBase = sliderBarID * _channelFieldWidth;
	uint16_t yPosModifier = (gThisView == 'P' || sliderBarID%2 == 0) ? -15 : 0;

	Console::printTextAt(xValBase + 14, 38+10, gDisplaySliderBars[sliderBarID].barTextAbove, ILI9341_WHITE, ILI9341_BLACK, 8);
	Console::sliderBar(gDisplaySliderBars[sliderBarID].barValue, xValBase + _barX, _barY);
	Console::printTextAt(xValBase + 12, 200+yPosModifier, gDisplaySliderBars[sliderBarID].barTextBelow, ILI9341_WHITE, ILI9341_BLACK, 9);

	int oldRot = _tft -> getRotation();
	_tft -> setRotation(oldRot-1);
	Console::printTextAt(85, xValBase+7, gDisplaySliderBars[sliderBarID].barTextBesides, ILI9341_WHITE, ILI9341_BLACK, 8);
	_tft -> setRotation(oldRot);
}
void Console::sliderBar ( uint8_t value, uint16_t x, uint16_t y) {
	/* draws a slider bar at <x>, <y>, with fill state <value> */
	// 4: ecken-radius fuer breite=9, 5: diese "4" + 1 fuer smoothness beim zurueckziehen des faders
	// wegen "versuche, bei roundRect ungerade breite/höhe zu vermeiden", sollte _w auch im singlemode 9 statt 10 sein...
	uint16_t transformed_val = floor(((float)value/255.0)*((float)_h)); // mapps <value> [0..255] auf [0.._h]
	uint8_t corrector = (transformed_val > 8) ? 8 : 0; // avoid _backColor to "bleed" below the fader bar on low values
	// magic number, lets get it out

	_tft -> fillRoundRect(x, y,                    _w, _h-transformed_val+corrector, _radius, _backColor); // oben hintergrund
	_tft -> fillRoundRect(x, y+_h-transformed_val, _w, transformed_val,              _radius, _foreColor); // unten vordergrund
}

// Slider-Bar handling, data functions:
void Console::reset_gDisplaySliderBars(){
	// beats gDisplaySliderBars[] back to init-state
	for(int i=0; i<NUM_FADER; i++){
		gDisplaySliderBars[i] = {};
		gDisplaySliderBars[i].barValue = 0;
	}
}
void Console::update_gDisplaySliderBars(uint16_t channelStartOffset, int itemSpecifier){
	/*  - resets gDisplaySliderBars[]
		- updates all elements of gDisplaySliderBars[] according to chosen view
		- possible gThisView (must be set before calling!):
			S "singleChannel"  (console.ino)        Dmx_Channels[], starting with the <channelOffset>th channel
			D "deviceControl"  (deviceControl.ino)  connectedDevices[deviceNum]'s channel, starting with the <channelOffset>th channel
			P "patchBay" (patchBay.ino)
      B "Blockbuster" (blockbuster.ino)
		- itemSpecifier is for instance for "selectedDeviceID" in the deviceControl-case
		- channelStartOffset is the 0th element of 8 total (= which 8 physical sources are mapped to the 8 elements of gDisplaySliderBars[])
	*/
	Console::reset_gDisplaySliderBars();
	for(int i = 0; i < NUM_FADER; i++){
		Console::update_singleBar_in_gDisplaySliderBars(channelStartOffset, i, itemSpecifier);
	}
}
void Console::update_singleBar_in_gDisplaySliderBars(uint16_t channelStartOffset, uint8_t sliderBarID, int itemSpecifier){
	/*  updates single element/sliderBar in array gDisplaySliderBars[]
		- (see update_gDisplaySliderBars() above for parameters itemSpecifier + channelStartOffset)
		- sliderBarID: which element of gDisplaySliderBars[]
		- configures:
			- barTextAbove, barTextBesides, barTextBelow + barValue
			- from different physical sources, according to itemSpecifier
	*/
	uint16_t channelNumber;
	DMX_ChannelInfo_t channelInfo;
	if(gThisView == 'S') { // Single Channel
		channelNumber = channelStartOffset + sliderBarID;
		if( channelNumber + 1 > NUM_DMX_CHANNELS ) return; // filter out "free" channels
		channelInfo = Console::get_channelInfos( channelNumber );
	} else if(gThisView == 'D') { // Device Control
		channelNumber = connectedDevices[itemSpecifier].startChannel + channelStartOffset + sliderBarID;
		//if( (!channelInfo.displayText_Type) || (channelNumber + 1) > devicesAvailable[connectedDevices[itemSpecifier].deviceModelID].numChannels ) return; // filter out "free" channels
		if( (channelNumber + 1) > connectedDevices[itemSpecifier].startChannel + devicesAvailable[connectedDevices[itemSpecifier].deviceModelID].numChannels ) return; // filter out "free" channels
		channelInfo = Console::get_channelInfos( channelNumber );
	} else if(gThisView == 'P') { // Patch Bay
		channelNumber = channelStartOffset + sliderBarID;
		// build a fake channelInfo for patchBay-fader:
		strncpy(channelInfo.connectionState, "used", 4); channelInfo.connectionState[4] = '\0';
		channelInfo.channelNum = channelNumber;
		channelInfo.displayText_ChannelNum = channelNumber + 1;
    	channelInfo.displayText_Type = patchBays[channelNumber].name; // here goes the patchBay name
		channelInfo.displayText_Value = map(patchBays[channelNumber].faderValue, 0, 255, 0, 100);
		channelInfo.displayText_Unit = "% ";
		channelInfo.faderValue = patchBays[channelNumber].faderValue;
	} else if (gThisView == 'Z') {  // sceneControl

  }

	sprintf(gDisplaySliderBars[sliderBarID].barTextAbove, "%03d", channelNumber + 1);
	sprintf(gDisplaySliderBars[sliderBarID].barTextBesides, "%s", (strcmp(channelInfo.displayText_Type, "free") == 0) ? "" : channelInfo.displayText_Type);
	gDisplaySliderBars[sliderBarID].barValue = channelInfo.faderValue;
	//sprintf(gDisplaySliderBars[sliderBarID].barTextBelow, "%*d%s", (strcmp(channelInfo.displayText_Unit, "K") == 0) ? 5 : 3, channelInfo.displayText_Value, channelInfo.displayText_Unit);
  //sprintf(gDisplaySliderBars[sliderBarID].barTextBelow, "%*d%s", (strcmp(channelInfo.displayText_Unit, "K") == 0) ? 5 : 3, channelInfo.displayText_Value, channelInfo.displayText_Unit);
  if (strcmp(channelInfo.displayText_Unit, "K") == 0) {
    sprintf(gDisplaySliderBars[sliderBarID].barTextBelow, "%*d%s",5 , channelInfo.displayText_Value, channelInfo.displayText_Unit);
  } else {
  sprintf(gDisplaySliderBars[sliderBarID].barTextBelow, "%*d%s",3 , channelInfo.displayText_Value, channelInfo.displayText_Unit);
  }
}
/*  ------------------------
	END Slider-Bar handling
	------------------------
*/


/*  ------------------------------------------------
	text output wrapper (position, color, font...)
	------------------------------------------------
*/
void Console::printTextAt(uint16_t xPos, uint16_t yPos, const char * text) {
	_tft -> setCursor(xPos, yPos);
    _tft -> print (text);
}
void Console::printTextAt(uint16_t xPos, uint16_t yPos, char * text) {
	_tft -> setCursor(xPos, yPos);
    _tft -> print (text);
}
void Console::printTextAt(uint16_t xPos, uint16_t yPos, const char * text, uint16_t textColor, uint16_t bgColor) {
    _tft -> setTextColor(textColor, bgColor);
	Console::printTextAt(xPos, yPos, text);
}
void Console::printTextAt(uint16_t xPos, uint16_t yPos, char * text, uint16_t textColor, uint16_t bgColor) {
    _tft -> setTextColor(textColor, bgColor);
	Console::printTextAt(xPos, yPos, text);
}
void Console::printTextAt(uint16_t xPos, uint16_t yPos, const char * text, uint16_t textColor, uint16_t bgColor, int fontSize) {
    _tft -> setFont(fonts[fontSize]);
	Console::printTextAt(xPos, yPos, text, textColor, bgColor);
}
void Console::printTextAt(uint16_t xPos, uint16_t yPos, char * text, uint16_t textColor, uint16_t bgColor, int fontSize) {
	_tft -> setFont(fonts[fontSize]);
	Console::printTextAt(xPos, yPos, text, textColor, bgColor);
}
/*  -----------------------
	END text output wrapper
	-----------------------
*/


/*  ----------------------------------------------
	functions for acquiring infos about channels:
	----------------------------------------------
*/
Device_Channel_t * Console::get_connectedDeviceChannel(uint16_t channelNum){
	// returns pointer zum dmx channel des mit <channelNum> verbundenen geraete-channel
	// bsp: (falls ARRI SP60 Mode#1 ab kanal 0 angeschlossen ist)
	//	Serial.printf("Channel-Typ: %s, Channel-Unit: %s", get_connectedDeviceChannel(0)->type, get_connectedDeviceChannel(0)->unit);
	//	--> Channel-Typ: DIMM, Channel-Unit: %
	//Serial.printf("\nget_connectedDeviceChannel(channelNum = %d) --> type: %s\n", channelNum, Dmx_Channels[channelNum].connectedDevice->deviceModel->deviceChannels[ Dmx_Channels[channelNum].deviceChannelID ].type );
	return &( devicesAvailable[ connectedDevices[Dmx_Channels[channelNum].connectedDeviceID].deviceModelID].deviceChannels[ Dmx_Channels[channelNum].deviceChannelID ] );
}
DMX_ChannelInfo_t Console::get_channelInfos(uint16_t channelNum){
	// because the needed data is dispersed through different object structures,
	// this function collects them in one return object. It's just for convenience...
	// connectionState:
	// 		used 		- device connected, channel used
	// 		reserved 	- device connected, channel not used
	// 		free		- no device connected

	DMX_ChannelInfo_t channelInfo;
	if(Dmx_Channels[channelNum].deviceConnected == true){
		// there is a DEVICE connected to this DMX channel
		Device_Channel_t *connectedDeviceChannel = Console::get_connectedDeviceChannel(channelNum);
		//Device_Model_t * connectedDeviceModel = get_connectedDeviceModel(channelNum);
		if(connectedDeviceChannel->type){
			// there is a USED device-channel connected to this DMX channel
			strncpy(channelInfo.connectionState, "used", 4); channelInfo.connectionState[4] = '\0';
			channelInfo.channelNum = channelNum;
			channelInfo.displayText_ChannelNum = channelNum + 1;
			channelInfo.displayText_Type = connectedDeviceChannel->type;
			channelInfo.displayText_Value = map(Dmx_Channels[channelNum].faderValue, 0, 255, connectedDeviceChannel->zeroVal, connectedDeviceChannel->maxVal);
			channelInfo.displayText_Unit = connectedDeviceChannel->unit;
			channelInfo.faderValue = Dmx_Channels[channelNum].faderValue;
			// don't know, if we need the device meta data (may be for grouping, headline etc),
			// so for now just a pointer to the connected device:
			channelInfo.connectedDeviceID = Dmx_Channels[channelNum].connectedDeviceID;
		} else {
			// there is a UN-USED device-channel connected to this DMX channel
			strncpy(channelInfo.connectionState, "rsvd", 4); channelInfo.connectionState[4] = '\0';
			channelInfo.channelNum = channelNum;
			channelInfo.displayText_ChannelNum = channelNum + 1;
			channelInfo.displayText_Type = "rsvd";
			channelInfo.displayText_Value = map(Dmx_Channels[channelNum].faderValue, 0, 255, 0, 100);
			channelInfo.displayText_Unit = " ";
			channelInfo.faderValue = Dmx_Channels[channelNum].faderValue;
			channelInfo.connectedDeviceID = Dmx_Channels[channelNum].connectedDeviceID;
		}
	} else {
		// there is NO DEVICE connected to this DMX channel
		//channelInfo = {}; // seems to NOT work (emphasis on "seems")
		strncpy(channelInfo.connectionState, "free", 4); channelInfo.connectionState[4] = '\0';
		channelInfo.channelNum = channelNum;
		channelInfo.displayText_ChannelNum = channelNum + 1;
		channelInfo.displayText_Type = "free";
		channelInfo.displayText_Value = map(Dmx_Channels[channelNum].faderValue, 0, 255, 0, 100);
		channelInfo.displayText_Unit = "% ";
		channelInfo.faderValue = Dmx_Channels[channelNum].faderValue;
		channelInfo.connectedDeviceID = -1;
	}
	return channelInfo;
}
/*  ----------------------------------------------
	END functions for acquiring infos about channels
	----------------------------------------------
*/
