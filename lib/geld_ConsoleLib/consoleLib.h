/*

ConsoleLib - Anzeige library fuer good enough lighting device

*/

#include "Arduino.h"
#include <ILI9341_t3.h>

#ifndef FADERCOUNT
#define FADERCOUNT 8
#endif

#ifndef Console_h
#define Console_h

// with this we get the global typedefs, parameters and constants:
#include "../../src/headerTypeDefinitions.h"



#define TFT_DC  9
#define TFT_CS  10



  //ILI9341_t3 tft = ILI9341_t3(TFT_CS, TFT_DC);

class Console {

public:
	Console(ILI9341_t3 *tft);
	ILI9341_t3 *getTft();

	void setScreenColor(uint16_t screenColor);
	uint16_t getScreenColor(void);
	void set_init_flag(bool flag);
	void printHeader(const char* text2Print);

	// sliderBar functions:
	void setBarColor(uint16_t foreColor, uint16_t backColor);
	void setBarDimensions(uint16_t width, uint16_t height);
	void setBarDimensions(uint16_t width, uint16_t height, uint16_t radius);
	void setBarCoordinates(uint16_t x, uint16_t y);
	uint16_t getCoordinate_x();
	uint16_t getCoordinate_y();
	uint16_t getSliderWidth();
	uint16_t getSliderHeight();
	void initAndDisplay_SliderBarGroup(uint8_t numSliderBars, char * header);
	void refresh_SliderBarGroup(uint8_t numSliderBars);
	void refresh_SliderBar_inGroup(uint8_t sliderBarID);
	void sliderBar(uint8_t value, uint16_t x, uint16_t y);
	static void reset_gDisplaySliderBars();
	void update_gDisplaySliderBars(uint16_t channelStartOffset, int itemSpecifier = -1);
	static void update_singleBar_in_gDisplaySliderBars(uint16_t channelStartOffset, uint8_t sliderBarID, int itemSpecifier = -1);

	// text display functions:
	void printTextAt(uint16_t xPos, uint16_t yPos, const char * text);
	void printTextAt(uint16_t xPos, uint16_t yPos, char * text);
	void printTextAt(uint16_t xPos, uint16_t yPos, const char * text, uint16_t textColor, uint16_t bgColor);
	void printTextAt(uint16_t xPos, uint16_t yPos, char * text, uint16_t textColor, uint16_t bgColor);
	void printTextAt(uint16_t xPos, uint16_t yPos, const char * text, uint16_t textColor, uint16_t bgColor, int fontSize);
	void printTextAt(uint16_t xPos, uint16_t yPos, char * text, uint16_t textColor, uint16_t bgColor, int fontSize);

	// channel info functions:
	static Device_Channel_t * get_connectedDeviceChannel(uint16_t channelNum);
	static DMX_ChannelInfo_t get_channelInfos(uint16_t channelNum);

private:

  ILI9341_t3  *_tft;
  unsigned int _old_displayVal[8];
  unsigned int _dsplStartAdress;
  unsigned int _screenWidth;
  unsigned int _screenHeight;
  unsigned int _backColor;
  unsigned int _foreColor;

  uint16_t _screenColor;
	uint16_t _barX;
	uint16_t _barY;
  uint16_t _w;
  uint16_t _h;
  uint8_t _radius = 0;
  uint16_t _valueMax;
  uint16_t _valueMin;
  uint16_t _channelFieldWidth = 38;
  int16_t _startAdress = 1;
  bool _init = 0;
  //uint16_t * _valArray;
  uint16_t _valueMax_display;
  uint16_t _valueMin_display;

  uint8_t _displayVal[FADERCOUNT];
  uint16_t _barHeightMap;

  void fillCircleBckgrndHelper(int16_t x0, int16_t y0, int16_t r,
      uint8_t cornername, int16_t delta, uint16_t color);
};

#endif
