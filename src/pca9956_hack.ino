

/** Name of the PCA9956A registers (for direct register access) */
enum command_reg {
		MODE1,         /**< MODE1 register      */
		MODE2,         /**< MODE2 register      */
		LEDOUT0,       /**< LEDOUT0 register    */
		LEDOUT1,       /**< LEDOUT1 register    */
		LEDOUT2,       /**< LEDOUT2 register    */
		LEDOUT3,       /**< LEDOUT3 register    */
		LEDOUT4,       /**< LEDOUT4 register    */
		LEDOUT5,       /**< LEDOUT5 register    */
		GRPPWM,        /**< GRPPWM register     */
		GRPFREQ,       /**< GRPFREQ register    */
		PWM0,          /**< PWM0 register       */
		PWM1,          /**< PWM1 register       */
		PWM2,          /**< PWM2 register       */
		PWM3,          /**< PWM3 register       */
		PWM4,          /**< PWM4 register       */
		PWM5,          /**< PWM5 register       */
		PWM6,          /**< PWM6 register       */
		PWM7,          /**< PWM7 register       */
		PWM8,          /**< PWM8 register       */
		PWM9,          /**< PWM9 register       */
		PWM10,         /**< PWM10 register      */
		PWM11,         /**< PWM11 register      */
		PWM12,         /**< PWM12 register      */
		PWM13,         /**< PWM13 register      */
		PWM14,         /**< PWM14 register      */
		PWM15,         /**< PWM15 register      */
		PWM16,         /**< PWM16 register      */
		PWM17,         /**< PWM17 register      */
		PWM18,         /**< PWM18 register      */
		PWM19,         /**< PWM19 register      */
		PWM20,         /**< PWM20 register      */
		PWM21,         /**< PWM21 register      */
		PWM22,         /**< PWM22 register      */
		PWM23,         /**< PWM23 register      */
		IREF0,         /**< IREF0 register      */
		IREF1,         /**< IREF1 register      */
		IREF2,         /**< IREF2 register      */
		IREF3,         /**< IREF3 register      */
		IREF4,         /**< IREF4 register      */
		IREF5,         /**< IREF5 register      */
		IREF6,         /**< IREF6 register      */
		IREF7,         /**< IREF7 register      */
		IREF8,         /**< IREF8 register      */
		IREF9,         /**< IREF9 register      */
		IREF10,        /**< IREF10 register     */
		IREF11,        /**< IREF11 register     */
		IREF12,        /**< IREF12 register     */
		IREF13,        /**< IREF13 register     */
		IREF14,        /**< IREF14 register     */
		IREF15,        /**< IREF15 register     */
		IREF16,        /**< IREF16 register     */
		IREF17,        /**< IREF17 register     */
		IREF18,        /**< IREF18 register     */
		IREF19,        /**< IREF19 register     */
		IREF20,        /**< IREF20 register     */
		IREF21,        /**< IREF21 register     */
		IREF22,        /**< IREF22 register     */
		IREF23,        /**< IREF23 register     */
		OFFSET = 0x3A, /**< OFFSET register     */
		SUBADR1,       /**< SUBADR1 register    */
		SUBADR2,       /**< SUBADR2 register    */
		SUBADR3,       /**< SUBADR3 register    */
		ALLCALLADR,    /**< ALLCALLADR register */
		PWMALL,        /**< PWMALL register     */
		IREFALL,       /**< IREFALL register    */
		EFLAG0,        /**< EFLAG0 register     */
		EFLAG1,        /**< EFLAG1 register     */
		EFLAG2,        /**< EFLAG2 register     */
		EFLAG3,        /**< EFLAG3 register     */
		EFLAG4,        /**< EFLAG4 register     */
		EFLAG5,        /**< EFLAG5 register     */

		REGISTER_START = MODE1,
		LEDOUT_REGISTER_START = LEDOUT0,
		PWM_REGISTER_START = PWM0,
		IREF_REGISTER_START = IREF0,
};


#define   ALLPORTS      		 0xFF
#define 	DEFAULT_I2C_ADDR   0xC0
#define 	AUTO_INCREMENT     0x80
#define   REGISTER_START     MODE1

uint16_t address = 0x01;		//0x01 is the chipadress as defined by A1-A4 pins
uint16_t _i2caddr = 0x01;   //adress, target and _i2caddr mean all the same and can be cleaned up
	uint8_t target = 0x01;

 void init_pca9956() {

	char init_array[] = {
		AUTO_INCREMENT | REGISTER_START, //  Command
			0x00,
			0x00, //  MODE1, MODE2
			0xAA,
			0xAA,
			0xAA,
			0xAA,
			0xAA,
			0xAA, //  LEDOUT[5:0]
			0x80,
			0x00, //  GRPPWM, GRPFREQ
		};

	  Wire.beginTransmission(_i2caddr);       // slave addr
	  Wire.write(init_array, sizeof(init_array));
	  Wire.endTransmission();
		for(int i=0; i< 8; i++){  //initialize potentiometer buttons
			pwm(i, 5);
			current(i, 30);
		}
		for(int i=8; i< 16; i++){  //initialize rest of the buttons
			pwm(i, 5);
			current(i, 10);
		}
		// 
		// pwm(21, 5);
		// current(21, 10);
		// pwm(22, 5);
		// current(22, 10);
}



uint8_t read8(uint8_t addr) {
  Wire.beginTransmission(_i2caddr);
  Wire.write(addr);
  Wire.endTransmission();

  Wire.requestFrom((uint8_t)_i2caddr, (uint8_t)1);
  return Wire.read();
}



void write8(uint8_t addr, uint8_t d) {
  Wire.beginTransmission(_i2caddr);
  Wire.write(addr);
  Wire.write(d);
  Wire.endTransmission();
}
void pwm( int port, uint8_t v )
{
    char    reg_addr;
    reg_addr    = pwm_register_access( port );
    write8( reg_addr, v );
}

uint8_t n_of_ports = 24;

char pwm_register_access(int port)
{
    if (port < n_of_ports)
        return (PWM_REGISTER_START + port);
    return (PWMALL);
}

void current( uint8_t port, uint8_t v )
{
    char    reg_addr;
    reg_addr    = current_register_access( port );
    write8( reg_addr, v );
}

char current_register_access(int port)
{
    if (port < n_of_ports)
        return (IREF_REGISTER_START + port);
    return (IREFALL);
}

void pwm( float *vp )
{
    // int     n_of_ports  = number_of_ports();
    // char    *data = new char[ n_of_ports + 1 ];
		//
    // *data    = pwm_register_access( 0 );
		//
    // for ( int i = 1; i <= n_of_ports; i++ )
    //     data[ i ]   = (char)(*vp++ * 255.0);
		//
    // write( data, n_of_ports+1);
    // delete data;
}

// void tft_debug_PCA9956(struct i2cStruct* i2c, uint8_t target, uint8_t& found, uint8_t all)
// {
// 		tft.setCursor(0,0);
//
//     switch(i2c->currentStatus)
//     {
//     case I2C_WAITING:  tft.printf("Addr: 0x%02X ACK\n",target); found=1; break;
//     case I2C_ADDR_NAK: if(all) { tft.printf("Addr: 0x%02X\n",target); } break;
//     case I2C_TIMEOUT:  if(all) { tft.printf("Addr: 0x%02X Timeout\n",target); } break;
//     default: break;
//     }
// }
