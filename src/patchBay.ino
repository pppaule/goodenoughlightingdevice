#include <Arduino.h>

#define TOP_OFFSET 10
#define PTCHB_FONT FONT_9

int gGridCols = 4; int gGridRows = 8; // dimensions of channel grid. please keep at 4x8.
ChannelGridPage_t  gChannelGrid[30];
int gChannelGrid_numPages = 0; // array-size of gChannelGrid[]
uint8_t localUsedPatchChannels[MAXCHANNEL+1]; // bool = uint8_t = 1byte ;-)

// gSelected_DmxChannel, gCurrentPage + gPositionOnCurrentPage are globally defined and initially set to 0


/* Patchbay - hier werden eintelne Kanaele, und perspektivisch auch Szenen, Effekte oder
 * Aufzeichnungen zusammgefasst und durch konfigurierte Fader geregelt. Mithilfe der
 * Selector Funktion lassen sich in Form eines Submenus die Kanaele auswaehlen
 */

void patchBay() {

 	// main function:
 	//		- shows realtime fader-bars and -values
 	//		- tracks fader-changes
 	//		- overrides main_dmx_val[]    <-- change that!
	gThisView = 'P'; // P: patchBay
	gDoRefresh = true;
	uint16_t channelStartOffset = 0;
	sprintf(gTitle, "%s", " Patched DMX Channels ");
 	patchBay_initScreen(channelStartOffset);
	build_ChannelGrid();
	led_faderBar_reset();
	while (true) {
		backgroundFuncs();
		//flashButton_readAndProcess_patchBay(channelStartOffset);
		//sceneChnButton();
		if (encoderChange(DOWN)) {
			led_faderBar_reset();
			channelStartOffset = (channelStartOffset + NUM_FADER >= NUM_PATCHBAYS) ? 0 : channelStartOffset + NUM_FADER;
			gDoRefresh = true;
			reset_potiVals(channelStartOffset);
			console.printHeader(gTitle);
		}

		if (encoderChange(UP)) {
			led_faderBar_reset();
			channelStartOffset = (channelStartOffset - NUM_FADER < 0) ? NUM_PATCHBAYS - NUM_FADER : channelStartOffset - NUM_FADER;
			gDoRefresh = true;
			reset_potiVals(channelStartOffset);
			console.printHeader(gTitle);
		}

/* future feature: targetValue einstellung via shift button (encoderbutton)
		pushing channelButton switches channeloutput to targetValue (currently
		hardwired to 255). pushing the shift button and moving a fader to current
		targetValue (potCatch) enables adjustmentmode. a red arrow next to the fader
		indicates current targetValue.

		SHIFT button works.
		targetValue already implemented

		TODO:
		- check how faderbar-struct is saved in eeprom. SEEMS OK
		- add targetvalue parameter in faderbar struct, and make sure eeprom doesn't
		get fucked up by more data, eg if struct is saved as raw values. DONE
		- get fader coordinates from consoleLib for indication-arrow (x - 20,y or
		so), implement console.getFaderX() + console.getFaderY()
		- while buttonPressed(SHIFT) light up ALL targetValueIndicators.
			after only light up < 255
		- check proper initialization after startup. IS INITIALIZED IN TYPEDEFINITION

*/
 		readAndUpdate_PatchBay_PotiVals_mit_flashbutton(channelStartOffset);

		if(gDoRefresh){
			// tft.setFont(PTCHB_FONT);
			console.update_gDisplaySliderBars(channelStartOffset);
			console.refresh_SliderBarGroup(NUM_FADER);
			patchedChannelsShortList(channelStartOffset);
			draw_targetValues_indicators(channelStartOffset);
			gDoRefresh = false;
		}

 		// das hier brauchen wir "eigentlich" nicht mehr, wenn wir die Unterscheidung,
 		//   was genau gesendet werden soll
 		//        patchBays[i].faderValue
 		//        vs.
 		//        Dmx_Channels[ patchBays[i].patchedChannels[j] ].faderValue
 		//   anderweitig, zb in dmx_send/backgroundfunc abfackeln... --> fine-tuning, scene-behaviour, ...
 		for (int i = 0 ; i < NUM_FADER; i++) {
 			// "old"/master-override behaviour (ignore single-value, override with patchBay-value)
 			for (int j = 0; j < patchBays[i + channelStartOffset].numChannels; j++) {
				if(Dmx_Channels[ patchBays[i + channelStartOffset].patchedChannels[j] -1 ].faderValue != patchBays[i + channelStartOffset].faderValue){
					Dmx_Channels[ patchBays[i + channelStartOffset].patchedChannels[j] -1 ].faderValue = patchBays[i + channelStartOffset].faderValue;
				}
 			}
 		}

 		if (buttonPressed(SELECT)) { //SELECT fuehrt ins Kanal-Modifikationsmenue
			int8_t selectedFaderButton = faderSelectPopup("Select Fader!");
		 	if(selectedFaderButton == -1) continue; // fader-select exited without select
			uint8_t selectedPatchBay = selectedFaderButton + channelStartOffset;     //initialisierende anzeige des menues

			// now that we have declared them in this file, we can link the actual functions:
			subMenu_patchBayFunc[1] = (MenuFuncPtr_withIntArg)patchBay_channelSelect_initScreen; // " Patch Channels",
			subMenu_patchBayFunc[2] = (MenuFuncPtr_withIntArg)showPatchedChannels; // " List Connections",
			subMenu_patchBayFunc[3] = (MenuFuncPtr_withIntArg)renamePatchBay; // " (Re-)Name",
			subMenu_patchBayFunc[4] = (MenuFuncPtr_withIntArg)movePatchBay; // " Move",
			subMenu_patchBayFunc[5] = (MenuFuncPtr_withIntArg)clearPatchBay; // " Clear"
			subMenu_patchBayFunc[6] = (MenuFuncPtr_withIntArg)saveSceneToRam; //save scene 1-8

		 	TftMenuLib subMenu_patchbay(20, 40, 260, 40, 12,20, subMenu_patchBayItems, subMenu_patchBayFunc, numSubPatchBay,&tft );
		 	subMenu_patchbay.selectedChannel = selectedPatchBay + 1; // here we use the 1..512 notation. why?
			subMenu_patchbay.menuWindowInit();
			subMenu_patchbay.menuListSelect();

			// offer options for single patchfader via submenu:
			while (true) {
				backgroundFuncs();
				if (encoderChange(UP)) {
					subMenu_patchbay.menuListSelect(UP);
				}
				if (encoderChange(DOWN)) {          //encoder reading downward
					subMenu_patchbay.menuListSelect(DOWN);
				}
				if (buttonPressed(SELECT)) {
					// execute selected function
					subMenu_patchBayFunc[subMenu_patchbay.menu_select](selectedPatchBay);
					break;
				}
				if (buttonPressed(EXIT))  {
					console.set_init_flag(0); // TODO: @PAULE: wozu ist set_init_flag?
					break;         // Console beenden!
				}
			}
			patchBay_initScreen(channelStartOffset); // restore main view
 		}

 		if (buttonPressed(EXIT))  { // EXIT beendet patchBay-Mode und geht zurueck ins <LightControl-Menu>
			led_faderBar_reset();
			for (int i = 0; i < NUM_FADER; i++) poti_catch_firstCall[i] = true;
 			menuLightControl.menuListInit();
 			menuLightControl.menuListSelect();
 			break;            // Console beenden!
 		}
 	}
}

// display + interaction functions:
void patchBay_initScreen(int channelStartOffset){
	console.printHeader(gTitle);
	// display patchBays fader-bars and -values:
	console.setScreenColor(ILI9341_BLACK);
	console.setBarColor(ILI9341_CYAN, GELD_GREY1);
	console.setBarDimensions(10, 105, 4);        // Dimensionen der FaderBars x, y, width
	console.setBarCoordinates(20,68);
	console.update_gDisplaySliderBars(channelStartOffset);
	console.refresh_SliderBarGroup(NUM_FADER);
	patchedChannelsShortList(channelStartOffset);
}

void patchedChannelsShortList(int channelStartOffset){
	// print patched channels short-list:
	for (int i = 0; i < NUM_FADER; i++) {
		for (int j = 0 ; j < 2 && patchBays[i + channelStartOffset].numChannels >= j+1; j++) {
			char text[] = "...";
			if(j<2) sprintf(text, "%03d", patchBays[i + channelStartOffset].patchedChannels[j]);
			console.printTextAt((i * 38) + 14  , (j * 12)+200, text, ILI9341_WHITE, GELD_BLACK, 8);
		}
	}
}

void patchBay_channelSelect_initScreen(byte selectedPatchBay) {
	// displays the channel grid for bulk selection. color-code:
	//		BLACK: available
	//		CYAN: used by <selectedPatchBay>
	//		DARKCYAN: used by other fader
	char text[36]; sprintf(text, " PatchBay Channel Select: Fader %d", selectedPatchBay + 1);
	console.printHeader(text);
	display_ChannelGrid(selectedPatchBay);
	patchBay_channelSelector(selectedPatchBay);
}

void display_ChannelGrid(byte selectedPatchBay){
	// displays grid of Dmx_Channels[]
	//
	// localUsedPatchChannels: array of channel usage:
	//         0=used_by_none, 1=used_by_other, 2=used_by_me
	//     actually made it file-global just now ;-)

	memcpy(localUsedPatchChannels, usedPatchChannels, sizeof(usedPatchChannels));
	for (int i = 0; i < patchBays[selectedPatchBay].numChannels; i++) {
		localUsedPatchChannels [ patchBays[selectedPatchBay].patchedChannels[i] ] = 2;
	}

	int labelWidth = 288/gGridCols; int labelHeigth = 20;

	char text[36]; sprintf(text, " PatchBay Channel Select: Fader %d", selectedPatchBay + 1);
	console.printHeader(text);

	display_ChannelGridPage(labelWidth, labelHeigth);
}

void display_ChannelGridPage(int labelWidth, int labelHeigth) {
	// displays the grid-page gChannelGrid[gCurrentPage]
	bool doSidelines = false;
	bool belongsToConnectedDevice = false;
	for(int e = 0; e < gChannelGrid[gCurrentPage].itemCount; e++){
		uint8_t row = gChannelGrid[gCurrentPage].gridPageItems[e].row;
		uint8_t col = gChannelGrid[gCurrentPage].gridPageItems[e].col;
		if(gChannelGrid[gCurrentPage].gridPageItems[e].type == 'H'){
			// do a header
			uint16_t channelNum = (e < gChannelGrid[gCurrentPage].itemCount - 1)
							? gChannelGrid[gCurrentPage].gridPageItems[e+1].objectID
							: gChannelGrid[gCurrentPage+1].gridPageItems[0].objectID;
			DMX_ChannelInfo_t channelInfo = console.get_channelInfos(channelNum);
			tft.drawLine(5, row * (labelHeigth+5) + 34, 315, row * (labelHeigth+5) + 34, ILI9341_CYAN); // side line left
			tft.drawLine(5, row * (labelHeigth+5) + 34, 5, row * (labelHeigth+5) + 55, ILI9341_CYAN); // side line right
			tft.drawLine(315, row * (labelHeigth+5) + 34, 315, row * (labelHeigth+5) + 55, ILI9341_CYAN); // top line
			char text[61]; // make it big enough to hold long model names (up to 50 chars) without leaking memory
			sprintf(text, "%s %s", connectedDevices[channelInfo.connectedDeviceID].name, devicesAvailable[connectedDevices[channelInfo.connectedDeviceID].deviceModelID].name);
			text[35] = '\0'; // force hard string end because line-limit
			console.printTextAt(30, row * (labelHeigth+5) + 42, text, ILI9341_WHITE, ILI9341_BLACK, 10);
			doSidelines = true;
		} else if(gChannelGrid[gCurrentPage].gridPageItems[e].type == 'F'){
			// do a footer
			tft.drawLine(5, row * (labelHeigth+5) + 28, 315, row * (labelHeigth+5) + 28, ILI9341_CYAN);
			doSidelines = false;
		} else if(gChannelGrid[gCurrentPage].gridPageItems[e].type == 'C'){
			// do a channel
			uint16_t labelColor, textFgColor, textBgColor, channelNum;
			channelNum = gChannelGrid[gCurrentPage].gridPageItems[e].objectID;

			DMX_ChannelInfo_t channelInfo = console.get_channelInfos(channelNum);
			belongsToConnectedDevice = (strcmp(channelInfo.connectionState, "used") == 0 || strcmp(channelInfo.connectionState, "rsvd") == 0);

			if (localUsedPatchChannels[channelNum + 1]==1) {
				// used_by_other
				labelColor = ILI9341_DARKCYAN; textFgColor = GELD_GREY2; textBgColor = ILI9341_DARKCYAN;
			} else if (localUsedPatchChannels[channelNum + 1]==2) {
				// used_by_me
				labelColor = ILI9341_CYAN; textFgColor = ILI9341_BLACK; textBgColor = ILI9341_CYAN;
			} else {
				// used_by_none
				labelColor = ILI9341_BLACK; textFgColor = ILI9341_WHITE; textBgColor = ILI9341_BLACK;
			}

			tft.fillRoundRect(col * (labelWidth+6) + 9, row * (labelHeigth+5) + 33, labelWidth-3, labelHeigth-2, 3, labelColor);

			char text[9]; sprintf(text, "%03d", channelNum + 1);
			if(belongsToConnectedDevice){
				// only channel type, if defined
				sprintf(text, "%s", channelInfo.displayText_Type);
			}
			console.printTextAt(col * (labelWidth+6) + 16, row * (labelHeigth+5) + 37, text, textFgColor, textBgColor, 8);

			if(doSidelines){ // side lines for device block, until footer is done
				tft.drawLine(5, row * (labelHeigth+5) + 28, 5, row * (labelHeigth+5) + 52, ILI9341_CYAN);
				tft.drawLine(315, row * (labelHeigth+5) + 28, 315, row * (labelHeigth+5) + 52, ILI9341_CYAN);
			}
		}
	}
}

void patchBay_channelSelector(byte selectedPatchBay){
	// tracks + handles changes in the channel-grid sub-view
	int labelWidth = 288/gGridCols; int labelHeigth = 20;
	if(gPositionOnCurrentPage == 0) lookAheadOnPage(selectedPatchBay); // check if first entry is really a channel
	while (true) {
		backgroundFuncs();

		if (encoderChange(DOWN)) {
			uint8_t row = gChannelGrid[gCurrentPage].gridPageItems[gPositionOnCurrentPage].row;
			uint8_t col = gChannelGrid[gCurrentPage].gridPageItems[gPositionOnCurrentPage].col;
			tft.drawRoundRect((col) * (labelWidth+6) + 7, (row) *(labelHeigth+5) + 31, labelWidth+1, labelHeigth+2, 3,ILI9341_BLACK);

			gSelected_DmxChannel++;
			if (gSelected_DmxChannel > NUM_DMX_CHANNELS-1){ // go to first page
				gSelected_DmxChannel = 0;
				gCurrentPage = 0;
				gPositionOnCurrentPage = 0;
				display_ChannelGrid(selectedPatchBay);
			} else {
				gPositionOnCurrentPage++;
			}
			lookAheadOnPage(selectedPatchBay);
		}

		if (encoderChange(UP)) {
			uint8_t row = gChannelGrid[gCurrentPage].gridPageItems[gPositionOnCurrentPage].row;
			uint8_t col = gChannelGrid[gCurrentPage].gridPageItems[gPositionOnCurrentPage].col;
			tft.drawRoundRect((col) * (labelWidth+6) + 7, (row) *(labelHeigth+5) + 31, labelWidth+1, labelHeigth+2, 3,ILI9341_BLACK);

			gSelected_DmxChannel--;
			if (gSelected_DmxChannel < 0 ){
				gSelected_DmxChannel = NUM_DMX_CHANNELS-1;
				gCurrentPage = gChannelGrid_numPages-1;
				gPositionOnCurrentPage = gChannelGrid[gCurrentPage].itemCount-1;
				display_ChannelGrid(selectedPatchBay);
			} else {
				gPositionOnCurrentPage--;
			}
			lookBehindOnPage(selectedPatchBay);
		}

		uint8_t row = gChannelGrid[gCurrentPage].gridPageItems[gPositionOnCurrentPage].row;
		uint8_t col = gChannelGrid[gCurrentPage].gridPageItems[gPositionOnCurrentPage].col;
		tft.drawRoundRect((col) * (labelWidth+6) + 7, (row) *(labelHeigth+5) + 31, labelWidth+1, labelHeigth+2, 3,GELD_RED);

		if (buttonPressed(SELECT)) {
			patchBay_handle_gSelected_DmxChannel(selectedPatchBay);
			display_ChannelGrid(selectedPatchBay);
		}

		if (buttonPressed(EXIT))  {
			break; // Console beenden!
		}
	}
}

void lookAheadOnPage(byte selectedPatchBay){
	while(true){ // set gPositionOnCurrentPage on the first actual channel
		if(gChannelGrid[gCurrentPage].gridPageItems[gPositionOnCurrentPage].type != 'C') gPositionOnCurrentPage++; // may be it starts with header or footer?
		if(gPositionOnCurrentPage >= gChannelGrid[gCurrentPage].itemCount){
			gCurrentPage++;
			if(gCurrentPage >= gChannelGrid_numPages) gCurrentPage = 0;
			gPositionOnCurrentPage = 0;
			display_ChannelGrid(selectedPatchBay);
		}
		if(gChannelGrid[gCurrentPage].gridPageItems[gPositionOnCurrentPage].type == 'C') break;
	}
}
void lookBehindOnPage(byte selectedPatchBay){
	while(true){ // set gPositionOnCurrentPage on the last actual channel
		if(gChannelGrid[gCurrentPage].gridPageItems[gPositionOnCurrentPage].type != 'C') gPositionOnCurrentPage--; // may be it ends with header or footer?
		if(gPositionOnCurrentPage < 0){
			gCurrentPage--;
			if(gCurrentPage < 0) gCurrentPage = gChannelGrid_numPages-1;
			gPositionOnCurrentPage = gChannelGrid[gCurrentPage].itemCount-1;
			display_ChannelGrid(selectedPatchBay);
		}
		if(gChannelGrid[gCurrentPage].gridPageItems[gPositionOnCurrentPage].type == 'C') break;
	}
}

void build_ChannelGrid() {
	// parses Dmx_Channels[] and builds a map of all possible pages of the ChannelGrid (uses addidional 5.8k of SRAM!)
	int displayGrid_startChannelID = 0; // which ID in Dmx_Channels[] is first in display grid
	int displayGrid_endChannelID = 0;   // which ID in Dmx_Channels[] is last in display grid

	int p = 0; // page-counter
	bool belongsToConnectedDevice = false;
	bool connectedDeviceDoHeader= false;
	bool connectedDeviceDoFooter= false;
	int8_t connectedDeviceID = -1; // ID of element of connectedDevices[]
	int channelNum = displayGrid_startChannelID;

	while(true){
		ChannelGridPage_t channelGridPage;
		channelGridPage.firstItemID = displayGrid_startChannelID;
		channelGridPage.itemCount = 0;
		int c = 0;

		for(int row = 0; row < gGridRows; row++){
			for(int col = 0; col < gGridCols; col++){
				if(channelNum >= NUM_DMX_CHANNELS) break;
				DMX_ChannelInfo_t channelInfo = console.get_channelInfos(channelNum);
				belongsToConnectedDevice = (strcmp(channelInfo.connectionState, "used") == 0 || strcmp(channelInfo.connectionState, "rsvd") == 0);
				if(row == 0 && col == 0 && belongsToConnectedDevice) connectedDeviceDoHeader= true; // we may have to repeat header from previous page
				if( ( (!belongsToConnectedDevice) || connectedDeviceID != channelInfo.connectedDeviceID) && connectedDeviceDoFooter){
					// new or no device detected. do we have to do a footer for previous device?
					if(col > 0) row++; // do LF, if not already on a new line
					channelGridPage.gridPageItems[c].type = 'F';
					channelGridPage.gridPageItems[c].row = row;
					channelGridPage.gridPageItems[c].col = 0;
					c++;
					connectedDeviceDoFooter = false;
				}
				if( belongsToConnectedDevice && connectedDeviceID != channelInfo.connectedDeviceID ){
					// new device detected. we have to do a header.
					connectedDeviceID = channelInfo.connectedDeviceID;
					connectedDeviceDoHeader= true;
				}
				if(connectedDeviceDoHeader){ // print device header
					if(col > 0){
						row++; // do LF, if not already on first col of a new line
					}
					channelGridPage.gridPageItems[c].type = 'H';
					channelGridPage.gridPageItems[c].row = row;
					channelGridPage.gridPageItems[c].col = 0;
					//channelGridPage.gridPageItems[c].objectID = channelNum; // geht nicht, da id in connectedDevices[] nicht ermittelbar
					c++;
					row++; // do LF
					connectedDeviceDoHeader = false;
					connectedDeviceDoFooter = true; // we did a header, so we need to do a footer later (and sidelines, if more than 1 row of channels)
				}

				if(row >= gGridRows){
					break;
				}
				channelGridPage.gridPageItems[c].type = 'C';
				channelGridPage.gridPageItems[c].row = row;
				channelGridPage.gridPageItems[c].col = col;
				channelGridPage.gridPageItems[c].objectID = channelNum;
				c++;

				channelNum++;
			}
		}
		displayGrid_endChannelID = channelNum-1;

		channelGridPage.itemCount = c;
		gChannelGrid[p] = channelGridPage;

		// for debug only: (internal channelGridPage layout)
		// Serial.printf("=== page %2d === %d elements, start-chn: %d\n", p, channelGridPage.itemCount, channelGridPage.firstItemID);
		// for(int i = 0; i < channelGridPage.itemCount; i++){
		// 	Serial.printf("[%d] %c row %d, col %d, chn %d\n",
		// 		i,
		// 		channelGridPage.gridPageItems[i].type,
		// 		channelGridPage.gridPageItems[i].row,
		// 		channelGridPage.gridPageItems[i].col,
		// 		(channelGridPage.gridPageItems[i].type == 'C') ? channelGridPage.gridPageItems[i].objectID : -1);
		// }
		// ENDE debug only

		if(channelNum >= 511){
			break;
		} else {
			displayGrid_startChannelID = displayGrid_endChannelID + 1;
			p++; // next page
		}
	}
	// reset global grid tracking vars:
	displayGrid_startChannelID = 0;
	displayGrid_endChannelID = 0;
	gChannelGrid_numPages = p+1;
}
// ENDE display + interaction functions

void showPatchedChannels(byte selectedPatchBay){
	// displays list of selectedPatchBay's connected channels
	char text[33]; sprintf(text," Patched DMX Channels for Bay %d", selectedPatchBay + 1);
	console.printHeader(text);
	int page = 0; // 14 lines fit on one page with font_10
	int maxPage = ceil(patchBays[selectedPatchBay].numChannels / 12);
	if(patchBays[selectedPatchBay].numChannels < 1) {
		console.printTextAt(60, 100, "No channels connected.", ILI9341_YELLOW, ILI9341_BLACK, 10);
	} else {
		_showPatchedChannels_page(selectedPatchBay, page, maxPage);
	}
	while(true){
		backgroundFuncs();
		if (patchBays[selectedPatchBay].numChannels > 0 && maxPage > 0 && encoderChange(DOWN)) {
			page++;
			if(page > maxPage) page = 0;
			_showPatchedChannels_page(selectedPatchBay, page, maxPage);
		}
		if (patchBays[selectedPatchBay].numChannels > 0 && maxPage > 0 && encoderChange(UP)) {
			page--;
			if(page < 0) page = maxPage ;
			_showPatchedChannels_page(selectedPatchBay, page, maxPage);
		}
		if (buttonPressed(EXIT))  {
 			break;
 		}
	}
}
void _showPatchedChannels_page(byte selectedPatchBay, int page, int maxPage){
	// displays one page of list of selectedPatchBay's connected channels
	//    14 lines fit on one page with font_10, so minus intro- & outro-dots 12 channels
	int x = 5; int y = 30;
	char text[33];
	sprintf(text," Patched DMX Channels for Bay %d  (%d/%d)", selectedPatchBay + 1, page+1, maxPage+1);
	console.printHeader(text);

	int start = 12 * page; int end = start + 11;
	if(page > 0) console.printTextAt(x, y, "   ...", ILI9341_YELLOW, ILI9341_BLACK, 8);
	y += 15;
	if(end > patchBays[selectedPatchBay].numChannels - 1) end = patchBays[selectedPatchBay].numChannels - 1;
	for(int i = start; i <= end; i++){
		DMX_ChannelInfo_t channelInfo = console.get_channelInfos( patchBays[selectedPatchBay].patchedChannels[i] -1 );
		tft.setCursor(x,y);
		if(strcmp(channelInfo.connectionState, "used") == 0 || strcmp(channelInfo.connectionState, "rsvd") == 0 ) {
			// device connected
			sprintf(text, "%d %03d (%s -> %s)\n", i, patchBays[selectedPatchBay].patchedChannels[i], connectedDevices[channelInfo.connectedDeviceID].name, channelInfo.displayText_Type);
		} else {
			// no device connected
			sprintf(text, "%d %03d (no device)\n", i, patchBays[selectedPatchBay].patchedChannels[i]);
		}
		console.printTextAt(x, y, text, ILI9341_YELLOW, ILI9341_BLACK, 8);
		y += 15;
	}
	if(start + 11 < patchBays[selectedPatchBay].numChannels - 1) console.printTextAt(x, y, "   ...", ILI9341_YELLOW, ILI9341_BLACK, 8);
}

void renamePatchBay(byte selectedPatchBay){
	// assign name to patchBay
	char name[11];
	sprintf(name, "%s", patchBays[selectedPatchBay].name);
	alphaNumericInput(name, "Enter Name:");
	sprintf(patchBays[selectedPatchBay].name, "%s", name);
}

void movePatchBay(byte selectedPatchBay){
	// - asks for new position in patchBays[],
	// - moves selectedPatchBay there and
	// - shifts other patch bays accordingly
	byte newPosition = numberSelectPopup("New position:", selectedPatchBay + 1, 1, NUM_PATCHBAYS);
	newPosition--; // convert 1..NUM to 0..NUM-1
	Patch_Bay_t temp_patchBay = patchBays[selectedPatchBay];
	if(newPosition == selectedPatchBay) {
		return;
	} else if(newPosition > selectedPatchBay) {
		for(int j = selectedPatchBay; j < newPosition; j++){
			_clearPatchBay(j); // make room
			sprintf(patchBays[j].name, "%s", patchBays[j+1].name); // move name
			for(int i=patchBays[j+1].numChannels-1; i>=0; i--){ // move channels
				int temp_channel = patchBays[j+1].patchedChannels[i];
				removeChannelFromPatchbay(j+1, patchBays[j+1].patchedChannels[i]);
				insertChannelIntoPatchbay(j, temp_channel);
			}
			patchBays[j].faderValue = patchBays[j+1].faderValue; // move fader value
		}
	} else if(newPosition < selectedPatchBay) {
		for(int j = selectedPatchBay; j > newPosition; j--){
			_clearPatchBay(j); // make room
			sprintf(patchBays[j].name, "%s", patchBays[j-1].name); // move name
			for(int i=patchBays[j-1].numChannels-1; i>=0; i--){ // move channels
				int temp_channel = patchBays[j-1].patchedChannels[i];
				removeChannelFromPatchbay(j-1, patchBays[j-1].patchedChannels[i]);
				insertChannelIntoPatchbay(j, temp_channel);
			}
			patchBays[j].faderValue = patchBays[j-1].faderValue; // move fader value
		}
	}
	sprintf(patchBays[newPosition].name, "%s", temp_patchBay.name); // move name
	for(int i=temp_patchBay.numChannels-1; i>=0; i--){ // move channels
		insertChannelIntoPatchbay(newPosition, temp_patchBay.patchedChannels[i]);
	}
	patchBays[newPosition].faderValue = temp_patchBay.faderValue; // move fader value

}

void clearPatchBay(byte selectedPatchBay){
	// remove name and channels from patchBay
	if(confirmPopup("Are you sure?")) _clearPatchBay(selectedPatchBay);
}
void _clearPatchBay(byte selectedPatchBay){
	// remove name and channels from patchBay
	sprintf(patchBays[selectedPatchBay].name, "%s", "");
	for(int i=patchBays[selectedPatchBay].numChannels-1; i>=0; i--){
		removeChannelFromPatchbay(selectedPatchBay, patchBays[selectedPatchBay].patchedChannels[i]);
	}
}

void readAndUpdate_PatchBay_PotiVals(uint16_t channelStartOffset){
	// reads fader-positions and updates patchBays[].faderValue
	for (int i = 0; i < NUM_FADER ; i++) {
		// potiVal[i]: 0..1024  potiRead[i]: 0..255
	//	potiVal[i] = poti_smooth(potiVal[i], i);
		potiRead[i] = poti_catch_val(poti_map_8bit(potiVal[i]), patchBays[channelStartOffset + i].faderValue, poti_catch_firstCall[i], i); // wenn helligkeit
		poti_catch_firstCall[i] = false;

		if (potiRead[i] <= 0) potiRead[i] = 0 ;		// lower limit Korrektur.

		if (patchBays[channelStartOffset + i].faderValue != potiRead[i]) {	// erfasse potiaenderung nur wenn neuer wert vorliegt.
			patchBays[channelStartOffset + i].faderValue = potiRead[i]; // store global fader values:
			console.update_singleBar_in_gDisplaySliderBars(channelStartOffset, i);
			console.refresh_SliderBar_inGroup(i);
			// resetScreensaveTimer();
		}
	}
}

// void adjustTargetvalue_for_patchBay() {
// 	while (userVal[SHIFT]) {
// 		backgroundFuncs();
// 		hallo();
// 		for (int i = 0; i < 8; i++){
// 			tft.setCursor((i * console.getCoordinate_x())+15, console.getCoordinate_y());
// 			tft.print(patchBays[1/*channelStartOffset + i*/].targetValue);
// 			}
// 	}
// }

void readAndUpdate_PatchBay_PotiVals_mit_flashbutton(uint16_t channelStartOffset){
	// reads fader-positions and updates patchBays[].faderValue
	// static uint8_t storedValue[NUM_FADER];	//uebernehme mal die flashbutton funktion mit in die generalUpdate Funktion
	// static uint8_t targetValue = 255;
	static uint8_t storedValue[NUM_PATCHBAYS];
	//uint8_t targetValue = 255;
	static bool pressed[8];
	adjustTargetvalue_for_patchBay(channelStartOffset); //locks into a while-loop
	for (int i = 0; i < NUM_FADER ; i++) {
		// potiVal[i]: 0..1024  potiRead[i]: 0..255
		potiVal[i] = poti_smooth(potiVal[i], i);  // wird immer ausgeführt, muss auf bedarfsfall geändert werden.
		potiRead[i] = poti_catch_val(poti_map_8bit(potiVal[i]), patchBays[channelStartOffset + i].faderValue, poti_catch_firstCall[i], i); // wenn helligkeit
		poti_catch_firstCall[i] = false;

		if (potiRead[i] <= 0) potiRead[i] = 0 ;		// lower limit Korrektur.

		if (buttonChnPressed(i)) {
			pressed[i] = true;

			storedValue[channelStartOffset + i] = patchBays[channelStartOffset + i].faderValue;
			//potiVal[i] = targetValue[i];
			patchBays[channelStartOffset + i].faderValue = patchBays[channelStartOffset + i].targetValue; // store global fader values:
			console.update_singleBar_in_gDisplaySliderBars(channelStartOffset, i);
			console.refresh_SliderBar_inGroup(i);
			//poti_catch_firstCall[i] = false;
			}
			//}
		if (pressed[i]){
			}

		if (buttonChnReleased(i) ) {
			pressed[i] = false;
			patchBays[channelStartOffset + i].faderValue = storedValue[channelStartOffset + i];
			console.update_singleBar_in_gDisplaySliderBars(channelStartOffset, i);
			console.refresh_SliderBar_inGroup(i);
			}

		if (patchBays[channelStartOffset + i].faderValue != potiRead[i] && !pressed[i]) {	// erfasse potiaenderung nur wenn neuer wert vorliegt.
			//hallo();
			patchBays[channelStartOffset + i].faderValue = poti_catch_val(poti_map_8bit(potiVal[i]), storedValue[i], poti_catch_firstCall[i], i); // store global fader values:
			console.update_singleBar_in_gDisplaySliderBars(channelStartOffset, i);
			console.refresh_SliderBar_inGroup(i);
			resetScreensaveTimer();
		}

	}
}

void patchBay_handle_gSelected_DmxChannel(int selectedPatchBay){
	// adds or removes <selectedChannel> to/from patchBays[selectedPatchBay]

	if(inArray(patchBays [selectedPatchBay].patchedChannels, patchBays [selectedPatchBay].numChannels, gSelected_DmxChannel + 1, true)){
		// Channel abwaehlen
		removeChannelFromPatchbay(selectedPatchBay, gSelected_DmxChannel+1);
	} else if (!usedPatchChannels[gSelected_DmxChannel + 1]){	//wenn in der patchTabelle platz ist && kanal nicht in usedPatchChannels
		// Channel auswaehlen
		insertChannelIntoPatchbay(selectedPatchBay, gSelected_DmxChannel+1);
	}

}

bool patchBay_memAvailable(){
	// checks if there is still room in patchBays[].patchedChannels[]
	int sum = 0;
	for(int i=0;i<NUM_PATCHBAYS;i++){
		sum += patchBays[i].numChannels;
	}
	return sum < NUM_DMX_CHANNELS;
}

void insertChannelIntoPatchbay(int selectedPatchBay, int channel){
	// fuegt Channel# in patchBays[selectedPatchBay].patchedChannels[] ein (sortiert!)
	// <channel> is 1..512 !!
	if(patchBay_memAvailable()){
		// there is still room in patchBay_UsedChannels
		int positionInThisPatchBay = 0;
		if (patchBays [selectedPatchBay].numChannels > 0){
			// there are already channels in this patchBay
			if(patchBays [selectedPatchBay].patchedChannels [patchBays [selectedPatchBay].numChannels - 1] < channel){
				// wenn letzter benutzter channel kleiner als aktueller channel -> einfach anhaengen
				positionInThisPatchBay = patchBays [selectedPatchBay].numChannels;
			}else{
				// an richtiger Stelle Platz machen, wenn schon channels enthalten sind
				for(int i=0;i<patchBays [selectedPatchBay].numChannels;i++){
					if(patchBays [selectedPatchBay].patchedChannels [i] < channel) continue;
					positionInThisPatchBay = i;
					break;
				}
				shiftArray(patchBays [selectedPatchBay].patchedChannels, positionInThisPatchBay, patchBays [selectedPatchBay].numChannels-positionInThisPatchBay, '>');
			}
		}
		patchBays [selectedPatchBay].patchedChannels [positionInThisPatchBay] = channel;
		Dmx_Channels[channel-1].patchBayID = selectedPatchBay;
		patchBays [selectedPatchBay].numChannels++;
		usedPatchChannels[channel] = 1; //schreibe kanal in usedPatchChannels
	} else {
		// patchBays[].patchedChannels[] is FULL!! (512 Channels vergeben)
	}
}

void removeChannelFromPatchbay(int selectedPatchBay, int channel){
	// entfernt Channel# aus patchBays[selectedPatchBay].patchedChannels[] (und behaelt Sortierung bei)
	// <channel> is 1..512 !!
	int positionInThisPatchBay = 0;
	for(int i=0; i<patchBays [selectedPatchBay].numChannels; i++){
		if(patchBays [selectedPatchBay].patchedChannels [i] != channel) continue;
		positionInThisPatchBay = i;
		break;
	}
	shiftArray(patchBays [selectedPatchBay].patchedChannels, positionInThisPatchBay, patchBays [selectedPatchBay].numChannels-positionInThisPatchBay, '<');
	Dmx_Channels[channel-1].patchBayID = -1;
	patchBays [selectedPatchBay].numChannels--;
	usedPatchChannels[channel] = 0; // ARRAY zur Verhinderung von Doppelbelegungen
}

void adjustTargetvalue_for_patchBay(uint8_t channel_offset) {

	if(buttonPressed(SHIFT)) {	// if buttonPressed once
			draw_targetValues_indicators(channel_offset);
	}

	while (userVal[SHIFT]) {		// while buttonPressed
		backgroundFuncs();

//		static uint8_t oldVals[8] = {255,255,255,255, 255, 255, 255, 255};
		for (int i =0; i < 8; i++){
			if (buttonChnPressed(i)){

		//		uint8_t currentVal = poti_map_8bit(potiVal[i]);
				patchBays[i+channel_offset].targetValue = poti_map_8bit(potiVal[i]);
				draw_targetValues_indicators(channel_offset);
			//poti_catch_firstCall[i] = false;

			}
		}
		if (buttonReleased(SHIFT)) reset_targetValue_indicators(channel_offset);
	}
}
//
// void draw_single_tV_indicator(uint8_t i){ // move into console library
// 	uint16_t h = console.getSliderHeight();
// 	uint16_t w = console.getSliderWidth();
// 	uint16_t x = (i * (console.getCoordinate_x()  +18)+32);
// 	uint16_t y = console.getCoordinate_y() + map(patchBays[i].targetValue,
// 																									255, 0,
// 																									0,  h);
// 	//tft.drawLine(x-1,y,x+6,y, ILI9341_BLACK);
// 	tft.drawLine(x-2,y,x+6,y, ILI9341_YELLOW);
// 	//tft.drawLine(x-1,y,x+6,y, ILI9341_BLACK);
// }

void draw_targetValues_indicators(uint8_t channel_offset){
	tft.setTextColor(ILI9341_YELLOW, ILI9341_BLACK);
	uint16_t h = console.getSliderHeight();

	for (int i = 0; i < 8 ; i++){
		if (patchBays[i+channel_offset].targetValue != 255){
			uint16_t x = (i * (console.getCoordinate_x()  +18)+32);
			uint16_t y = console.getCoordinate_y() + map(patchBays[i+channel_offset].targetValue,
																										255, 0,
																										0,  h);
			tft.drawLine(x-2,y,x+6,y, ILI9341_YELLOW);
			}
		}
}

void reset_targetValue_indicators(uint8_t channel_offset){		//draws a black rectangle next to the fader
	uint16_t h = console.getSliderHeight();
	uint16_t w = console.getSliderWidth();
	for (int i = 0; i < 8; i++){
		if (patchBays[i+channel_offset].targetValue == 255) {
			uint16_t x = (i * (console.getCoordinate_x() +18 )+31);
			uint16_t y = console.getCoordinate_y() ;
			tft.fillRect(x, y, w, h, ILI9341_BLACK);
		}
	}
}

void draw_all_tVal_indicators(){

}
