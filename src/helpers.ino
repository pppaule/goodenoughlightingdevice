
void print_frames_per_(uint16_t interval) { //

  static unsigned long frames_timer = millis();
  static int frames_counter = 0;
  static int frames_per_interval = 0;
  frames_counter++;

  if (millis() - frames_timer > interval) {
    frames_timer = millis();
    frames_per_interval = frames_counter;
    tft.setTextColor(ILI9341_BLACK, ILI9341_WHITE);
    tft.setFont(FONT_8);
    tft.setCursor(250, 230);
    tft.printf("fps: %4d  ", frames_per_interval);
    frames_counter = 0;
  }
}
void print_freeRam() {
  int32_t free_ram = FreeRam();
  tft.setCursor(115, 230);
  tft.printf("FreeMemory: %02d.%03d ", free_ram / 1000,
             free_ram - free_ram / 1000 * 1000);
}
uint32_t FreeRam() { // for Teensy 3.0
  uint32_t stackTop;
  uint32_t heapTop;

  // current position of the stack.
  stackTop = (uint32_t)&stackTop;

  // current position of heap.
  void *hTop = malloc(1);
  heapTop = (uint32_t)hTop;
  free(hTop);

  // The difference is the free, available ram.
  return stackTop - heapTop;
}

void calculate_antilogTable() {

  float _scalingFactor[256] = {
      0.0000, 0.0522, 0.0738, 0.0905, 0.1046, 0.1170, 0.1282, 0.1386, 0.1483,
      0.1574, 0.1660, 0.1742, 0.1821, 0.1897, 0.1969, 0.2040, 0.2108, 0.2175,
      0.2239, 0.2302, 0.2364, 0.2424, 0.2482, 0.2540, 0.2596, 0.2652, 0.2706,
      0.2760, 0.2812, 0.2864, 0.2915, 0.2965, 0.3015, 0.3063, 0.3112, 0.3159,
      0.3206, 0.3253, 0.3299, 0.3344, 0.3389, 0.3434, 0.3478, 0.3521, 0.3564,
      0.3607, 0.3650, 0.3692, 0.3733, 0.3775, 0.3815, 0.3856, 0.3896, 0.3936,
      0.3976, 0.4016, 0.4055, 0.4094, 0.4132, 0.4171, 0.4209, 0.4247, 0.4284,
      0.4322, 0.4359, 0.4396, 0.4433, 0.4469, 0.4506, 0.4542, 0.4578, 0.4614,
      0.4649, 0.4685, 0.4720, 0.4755, 0.4790, 0.4825, 0.4860, 0.4894, 0.4929,
      0.4963, 0.4997, 0.5031, 0.5065, 0.5098, 0.5132, 0.5165, 0.5198, 0.5232,
      0.5265, 0.5298, 0.5330, 0.5363, 0.5396, 0.5428, 0.5460, 0.5493, 0.5525,
      0.5557, 0.5589, 0.5621, 0.5653, 0.5684, 0.5716, 0.5747, 0.5779, 0.5810,
      0.5841, 0.5872, 0.5904, 0.5935, 0.5965, 0.5996, 0.6027, 0.6058, 0.6088,
      0.6119, 0.6149, 0.6180, 0.6210, 0.6241, 0.6271, 0.6301, 0.6331, 0.6361,
      0.6391, 0.6421, 0.6451, 0.6481, 0.6510, 0.6540, 0.6570, 0.6599, 0.6629,
      0.6658, 0.6688, 0.6717, 0.6747, 0.6776, 0.6805, 0.6834, 0.6863, 0.6893,
      0.6922, 0.6951, 0.6980, 0.7009, 0.7037, 0.7066, 0.7095, 0.7124, 0.7153,
      0.7181, 0.7210, 0.7239, 0.7267, 0.7296, 0.7324, 0.7353, 0.7381, 0.7410,
      0.7438, 0.7466, 0.7495, 0.7523, 0.7551, 0.7580, 0.7608, 0.7636, 0.7664,
      0.7692, 0.7720, 0.7749, 0.7777, 0.7805, 0.7833, 0.7861, 0.7889, 0.7917,
      0.7944, 0.7972, 0.8000, 0.8028, 0.8056, 0.8084, 0.8111, 0.8139, 0.8167,
      0.8195, 0.8222, 0.8250, 0.8278, 0.8305, 0.8333, 0.8361, 0.8388, 0.8416,
      0.8444, 0.8471, 0.8499, 0.8526, 0.8554, 0.8581, 0.8609, 0.8636, 0.8664,
      0.8691, 0.8719, 0.8746, 0.8773, 0.8801, 0.8828, 0.8856, 0.8883, 0.8910,
      0.8938, 0.8965, 0.8992, 0.9020, 0.9047, 0.9074, 0.9102, 0.9129, 0.9156,
      0.9184, 0.9211, 0.9238, 0.9265, 0.9293, 0.9320, 0.9347, 0.9374, 0.9402,
      0.9429, 0.9456, 0.9483, 0.9511, 0.9538, 0.9565, 0.9592, 0.9619, 0.9647,
      0.9674, 0.9701, 0.9728, 0.9755, 0.9783, 0.9810, 0.9837, 0.9864, 0.9891,
      0.9918, 0.9946, 0.9973, 1.0000};

  Serial.print("antilogTable[256] = \n\t");
  for (int i = 0; i < 256; i++) {
    int value = 255 * _scalingFactor[i];
    static int count = 0;
    count++;
    if (i == 255) {
      Serial.printf("%3d ", value);
    } else {
      Serial.printf("%3d, ", value);
    }
    if (count == 16) {
      Serial.println();
      Serial.print("\t");
      count = 0;
    }
  }
  Serial.println("};");
}

void reboot_device() { // rebootet den Teensy zum update
  // menu_alert_rect();
  if (!confirmPopup("Are You Sure?")) {
    backgroundFuncs(); // immerwiederholte routinen fur dmx, crmx, shiftregister
                       // etc, immer innerhalb von while schleifen!
    menuMain.comeBack();
    // mainMenu();
  } else {
    _reboot_Teensyduino_();
  }
}

bool confirmPopup(const char *text) {
  // popup for choosing to either accept or reject an action
  int16_t x = 90;
  int16_t y = 85; // 90,85
  int16_t w = 140;
  int16_t h = 48; // 140 x 48;
  //uint16_t *pcolors;
  // pcolors = (uint16_t*)malloc( sizeof(uint16_t) * w * h );

  // tft.readRect(x, y, w ,h, pcolors);

  tft.fillRect(x, y, w, h, ILI9341_BLACK);
  tft.drawRect(x + 1, y + 1, w - 2, h - 2, ILI9341_RED);
  console.printTextAt(x + 13, y + 10, text, ILI9341_YELLOW, ILI9341_BLACK, 10);
  console.printTextAt(x + 5, y + 30, "SELECT:Y  EXIT:N", ILI9341_YELLOW,
                      ILI9341_BLACK, 10);
  while (true) {
    backgroundFuncs();
    if (buttonPressed(SELECT)) {
      //	tft.writeRect(x,y,w,h,pcolors);
      //	free(pcolors);
      return (true);
    }
    if (buttonPressed(EXIT)) {
      //	tft.writeRect(x,y,w,h,pcolors);
      //	free(pcolors);
      return (false);
    }
  }
}

void infoPopup(const char *text) {
  // displays a popup with <text> for 0.75 sec
  int16_t x = 90;
  int16_t y = 85; // 90,85
  int16_t w = 150;
  int16_t h = 48; // 150 x 48;
  int timerStart = millis();
  tft.fillRect(x, y, w, h, ILI9341_BLACK);
  tft.drawRect(x + 1, y + 1, w - 2, h - 2, ILI9341_RED);
  console.printTextAt(x + 13, y + 10, text, ILI9341_YELLOW, ILI9341_BLACK, 10);
  while (true) {
    if (millis() - timerStart > 750)
      break;
  }
  return;
}

int8_t faderSelectPopup(const char *text) {
  // popup for selecting a fader by pressing the according fader-button
  //    returns 0..NUM_FADER-1 on fader-button-press, or -1 on EXIT-press
  int16_t x = 90;
  int16_t y = 85; // 90,85
  int16_t w = 145;
  int16_t h = 48; // 140 x 48;
  // uint16_t *pcolors;
  // pcolors = (uint16_t*)malloc( sizeof(uint16_t) * w * h );

  // tft.readRect(x, y, w ,h, pcolors);

  tft.fillRect(x, y, w, h, ILI9341_BLACK);
  tft.drawRect(x + 1, y + 1, w - 2, h - 2, ILI9341_RED);
  tft.setCursor(x + 13, y + 10);
  tft.setTextColor(ILI9341_YELLOW, ILI9341_BLACK);
  tft.setFont(FONT_10);
  tft.println(text);
  tft.setCursor(x + 5, y + 30);
  tft.println("Pos. 1..8 or EXIT");
  while (true) {
    backgroundFuncs();
    for (int i = 0; i < NUM_FADER; i++) {
      if (buttonChnPressed(i)) {
        // tft.writeRect(x,y,w,h,pcolors);
        // free(pcolors);
        return (i);
      }
    }
    if (buttonPressed(EXIT)) {
      // tft.writeRect(x,y,w,h,pcolors);
      // free(pcolors);
      return (-1);
    }
  }
}

int numberSelectPopup(const char *text, int initialVal, int minVal,
                      int maxVal) {
  // popup for selecting a number between minVal and maxVal
  //    (at this point the formatting is ok for numbers 0..999)
  int16_t x = 90;
  int16_t y = 85; // 90,85
  int16_t w = 145;
  int16_t h = 48; // 140 x 48;
  // uint16_t *pcolors;
  // pcolors = (uint16_t*)malloc( sizeof(uint16_t) * w * h );

  // tft.readRect(x, y, w ,h, pcolors);

  tft.fillRect(x, y, w, h, ILI9341_BLACK);
  tft.drawRect(x + 1, y + 1, w - 2, h - 2, ILI9341_RED);
  console.printTextAt(x + 13, y + 10, text, ILI9341_YELLOW, ILI9341_BLACK, 10);
  console.printTextAt(x + 30, y + 30, "<-", ILI9341_YELLOW, ILI9341_BLACK, 10);
  console.printTextAt(x + 90, y + 30, "->", ILI9341_YELLOW, ILI9341_BLACK, 10);
  int newVal = initialVal;
  char numberText[14];
  sprintf(numberText, "%3d", newVal); // tft.setFont(FONT_10);
  console.printTextAt(x + 60, y + 30, numberText, ILI9341_YELLOW, ILI9341_BLACK,
                      10);
  while (true) {
    backgroundFuncs();
    if (encoderChange(DOWN)) {
      newVal++;
      if (newVal > maxVal)
        newVal = minVal;
      sprintf(numberText, "%3d", newVal); // tft.setFont(FONT_10);
      tft.fillRect(x + 60, y + 30, 30, 10, ILI9341_BLACK);
      console.printTextAt(x + 60, y + 30, numberText,
                          (newVal == initialVal) ? ILI9341_YELLOW : ILI9341_RED,
                          ILI9341_BLACK, 10);
    }
    if (encoderChange(UP)) {
      newVal--;
      if (newVal < minVal)
        newVal = maxVal;
      sprintf(numberText, "%3d", newVal); // tft.setFont(FONT_10);
      tft.fillRect(x + 60, y + 30, 30, 10, ILI9341_BLACK);
      console.printTextAt(x + 60, y + 30, numberText,
                          (newVal == initialVal) ? ILI9341_YELLOW : ILI9341_RED,
                          ILI9341_BLACK, 10);
    }
    if (buttonPressed(SELECT)) {
      // tft.writeRect(x,y,w,h,pcolors);
      // free(pcolors);
      return (newVal);
    }
    if (buttonPressed(EXIT)) {
      // tft.writeRect(x,y,w,h,pcolors);
      // free(pcolors);
      return (initialVal);
    }
  }
}
int numberSelectPopup_forScenes() {
  // popup for selecting a scene number to store in
  int16_t x = 90;
  int16_t y = 85; // 90,85
  int16_t w = 145;
  int16_t h = 48; // 140 x 48;

  tft.fillRect(x, y, w, h, ILI9341_BLACK);
  tft.drawRect(x + 1, y + 1, w - 2, h - 2, ILI9341_RED);
  console.printTextAt(x + 13, y + 10, "Save scene number:", ILI9341_YELLOW,
                      ILI9341_BLACK, 10);
  console.printTextAt(x + 30, y + 30, "<-", ILI9341_YELLOW, ILI9341_BLACK, 10);
  console.printTextAt(x + 90, y + 30, "->", ILI9341_YELLOW, ILI9341_BLACK, 10);
  int newVal = 0;
  char numberText[14];
  sprintf(numberText, "%3d", newVal + 1); // tft.setFont(FONT_10);
  console.printTextAt(x + 60, y + 30, numberText,
                      (DMX_Scenes[newVal].isEmpty == true) ? ILI9341_YELLOW
                                                           : ILI9341_RED,
                      ILI9341_BLACK, 10);
  while (true) {
    backgroundFuncs();
    if (encoderChange(DOWN)) {
      newVal++;
      if (newVal == NUM_SCENES)
        newVal = 0;
      sprintf(numberText, "%3d", newVal + 1); // tft.setFont(FONT_10);
      tft.fillRect(x + 60, y + 30, 30, 10, ILI9341_BLACK);
      console.printTextAt(x + 60, y + 30, numberText,
                          (DMX_Scenes[newVal].isEmpty == true) ? ILI9341_YELLOW
                                                               : ILI9341_RED,
                          ILI9341_BLACK, 10);
    }
    if (encoderChange(UP)) {
      newVal--;
      if (newVal < 0)
        newVal = NUM_SCENES - 1;
      sprintf(numberText, "%3d", newVal + 1); // tft.setFont(FONT_10);
      tft.fillRect(x + 60, y + 30, 30, 10, ILI9341_BLACK);
      console.printTextAt(x + 60, y + 30, numberText,
                          (DMX_Scenes[newVal].isEmpty == true) ? ILI9341_YELLOW
                                                               : ILI9341_RED,
                          ILI9341_BLACK, 10);
    }
    if (buttonPressed(SELECT)) {
      // tft.writeRect(x,y,w,h,pcolors);
      // free(pcolors);
      if (DMX_Scenes[newVal].isEmpty == true ||
          confirmPopup("Overwrite scene?"))
        return (newVal);
      return (-1);
    }
    if (buttonPressed(EXIT)) {
      // tft.writeRect(x,y,w,h,pcolors);
      // free(pcolors);
      return (-1);
    }
  }
}

void alphaNumericInput(char *returnBuffer, const char *titletext) {
  /* enter string by encoder-select chars/nums out of a grid
          max length implemented by _displayGrid_withInput(): 10 char !!!
          call like this:
                  char name[10];
                  alphaNumericInput(name, "Enter Name:");
                  print(name);
  */
  tft.fillScreen(ILI9341_BLACK);
  console.printTextAt(0, 4, titletext, ILI9341_GREEN, ILI9341_BLACK, 10);
  tft.drawLine(0, 19, tft.width() - 1, 19, ILI9341_GREEN);

  int rows = 4;
  int cols = 10;
  int sizeOf_elementArray = 37; // alphaNum has 37 elements

  _displayGrid_withInput(rows, cols, sizeOf_elementArray, alphaNum,
                         returnBuffer);
}
void _displayGrid_withInput(int rows, int cols, int sizeOf_elementArray,
                            char *elementArray, char *returnBuffer) {
  /*
          displays cols x rows grid of encoder-selectable elements of
     elementArray[] plus DELETE + ACCEPT buttons waits for user interaction:
                - encoder-positioning in grid
                - SELECT:
                       - encoder@element: add selected element to buffer
                       - encoder@field "DELETE" -> remove last element from
     buffer
                       - encoder@field "ACCEPT" -> terminate buffer and return
     to previous view
                - EXIT: same like SELECT field "ACCEPT"
          max. length of returnBuffer: 10 char

          @PAULE: die koennte evtl auch global fuer andere grid-anzeigen genutzt
     werden (zb channel) dazu muessten wir dann die derzeitige begrenzung von
     elementArray[] auf char[] aufbohren, so daß auch int[] o.ä. akzeptiert
     wird...
  */
  uint8_t counter = 0;
  int xPos, yPos;
  int width = 310 / cols - 5;
  int height = 20;
  for (int row = 0; row < rows; row++) {
    for (int col = 0; col < cols; col++) {
      if (counter >= sizeOf_elementArray)
        break;
      xPos = col * 310 / cols + 5;
      yPos = row * 200 / (rows + 1) + 40;

      tft.fillRoundRect(xPos, yPos, width, height, 3, ILI9341_DARKCYAN);
      char text[2] = {elementArray[counter],
                      '\0'}; // have to convert from single char to char* !!
      console.printTextAt(xPos + 8, yPos + 5, text, ILI9341_BLACK,
                          ILI9341_DARKCYAN, 10);
      counter++;
    }
  }
  int long_width = 70;
  yPos = 200;

  xPos = 60;
  tft.fillRoundRect(xPos, yPos, long_width, height, 3, ILI9341_DARKCYAN);
  console.printTextAt(xPos + 9, yPos + 5, "DELETE", ILI9341_BLACK,
                      ILI9341_DARKCYAN, 10);

  xPos = 190;
  tft.fillRoundRect(xPos, yPos, long_width, height, 3, ILI9341_DARKCYAN);
  console.printTextAt(xPos + 9, yPos + 5, "ACCEPT", ILI9341_BLACK,
                      ILI9341_DARKCYAN, 10);

  int bufferPosition = strlen(returnBuffer);
  int encoderPosition = 0;

  tft.setFont(FONT_8);
  console.printTextAt(180, 5, returnBuffer, ILI9341_WHITE, ILI9341_BLACK, 10);
  tft.drawRoundRect((encoderPosition % cols) * (width + 5) + 3,
                    (encoderPosition / cols) * (200 / (rows + 1)) + 38, 30, 24,
                    3, RUSSENROT);
  while (true) {
    // handle encoder and buttons
    backgroundFuncs();
    if (encoderChange(DOWN)) {
      // clear old posFrame
      if (encoderPosition == sizeOf_elementArray) { // DELETE button
        tft.drawRoundRect(58, 198, 74, 24, 3, ILI9341_BLACK);
      } else if (encoderPosition == sizeOf_elementArray + 1) { // ACCEPT button
        tft.drawRoundRect(188, 198, 74, 24, 3, ILI9341_BLACK);
      } else {
        tft.drawRoundRect((encoderPosition % cols) * (width + 5) + 3,
                          (encoderPosition / cols) * (200 / (rows + 1)) + 38,
                          30, 24, 3, ILI9341_BLACK);
      }

      // mark new posFrame
      encoderPosition =
          (encoderPosition < sizeOf_elementArray + 1) ? encoderPosition + 1 : 0;
      if (encoderPosition == sizeOf_elementArray) { // DELETE button
        tft.drawRoundRect(58, 198, 74, 24, 3, RUSSENROT);
      } else if (encoderPosition == sizeOf_elementArray + 1) { // ACCEPT button
        tft.drawRoundRect(188, 198, 74, 24, 3, RUSSENROT);
      } else {
        tft.drawRoundRect((encoderPosition % cols) * (width + 5) + 3,
                          (encoderPosition / cols) * (200 / (rows + 1)) + 38,
                          30, 24, 3, RUSSENROT);
      }
    }

    if (encoderChange(UP)) {
      // clear old posFrame
      if (encoderPosition == sizeOf_elementArray) { // DELETE button
        tft.drawRoundRect(58, 198, 74, 24, 3, ILI9341_BLACK);
      } else if (encoderPosition == sizeOf_elementArray + 1) { // ACCEPT button
        tft.drawRoundRect(188, 198, 74, 24, 3, ILI9341_BLACK);
      } else {
        tft.drawRoundRect((encoderPosition % cols) * (width + 5) + 3,
                          (encoderPosition / cols) * (200 / (rows + 1)) + 38,
                          30, 24, 3, ILI9341_BLACK);
      }

      // mark new posFrame
      encoderPosition =
          (encoderPosition > 0) ? encoderPosition - 1 : sizeOf_elementArray + 1;
      if (encoderPosition == sizeOf_elementArray) { // DELETE button
        tft.drawRoundRect(58, 198, 74, 24, 3, RUSSENROT);
      } else if (encoderPosition == sizeOf_elementArray + 1) { // ACCEPT button
        tft.drawRoundRect(188, 198, 74, 24, 3, RUSSENROT);
      } else {
        tft.drawRoundRect((encoderPosition % cols) * (width + 5) + 3,
                          (encoderPosition / cols) * (200 / (rows + 1)) + 38,
                          30, 24, 3, RUSSENROT);
      }
    }

    if (buttonPressed(SELECT)) {
      if (encoderPosition == sizeOf_elementArray) { // DELETE button
        returnBuffer[bufferPosition] = '\0';
        if (bufferPosition > 0)
          bufferPosition--;
        returnBuffer[bufferPosition] = '\0';
      } else if (encoderPosition == sizeOf_elementArray + 1) { // ACCEPT button
        break;                                                 // end loop
      } else { // add element to buffer
        returnBuffer[bufferPosition] = elementArray[encoderPosition];
        returnBuffer[bufferPosition + 1] = '\0';
        if (bufferPosition < 9)
          bufferPosition++; // max 9 chars
      }
      tft.fillRect(180, 5, 150, 14, ILI9341_BLACK);
      console.printTextAt(180, 5, returnBuffer, ILI9341_WHITE, ILI9341_BLACK,
                          10);
    }

    if (buttonPressed(EXIT)) {
      break; // Loop beenden!
    }
  }
}

void shiftArray(uint16_t *array, int startPos, int length, char direction) {
  if (direction == '>') {
    for (int i = startPos + length; i > startPos; i--) {
      array[i] = array[i - 1];
    }
  } else if (direction == '<') {
    for (int i = startPos; i < startPos + length; i++) {
      array[i] = array[i + 1];
    }
  }
}
bool inArray(uint16_t *array, int array_size, int value, bool stopOnZero) {
  // returns true, if [value] is in [array]
  // [stopOnZero == true] ends seek at first zero-element
  for (int i = 0; i < array_size; i++) {
    if (array[i] == value)
      return true;
    if (stopOnZero && array[i] == 0)
      break;
  }
  return false;
}

void dumpArray(uint16_t array[], char *name, int arraySize) {
  Serial.print(name);
  Serial.print(" [0..");
  Serial.print(arraySize - 1);
  Serial.println("]: ");
  for (int i = 0; i < arraySize; i++) {
    Serial.print(array[i]);
    Serial.print(" ");
  }
  Serial.println(" ");
}
void dumpArray(uint8_t array[], char *name, int arraySize) {
  Serial.print(name);
  Serial.print(" [0..");
  Serial.print(arraySize - 1);
  Serial.println("]: ");
  for (int i = 0; i < arraySize; i++) {
    Serial.print(array[i]);
    Serial.print(" ");
  }
  Serial.println(" ");
}
void dumpArray(bool array[], char *name, int arraySize) {
  Serial.print(name);
  Serial.print(" [0..");
  Serial.print(arraySize - 1);
  Serial.println("]: ");
  for (int i = 0; i < arraySize; i++) {
    Serial.print(array[i]);
    Serial.print(" ");
  }
  Serial.println(" ");
}

// ist hasChanged ein fall fuer ein template?
boolean hasChanged(BYTES_VAL_T x) {
  static BYTES_VAL_T old_value = x;
  if (x != old_value) {
    old_value = x;
    return (1);
  }
  return (0);
}
boolean hasChanged(int x) {
  static int old_value = x;
  if (x != old_value) {
    old_value = x;
    return (1);
  }
  return (0);
}
boolean hasChanged(uint32_t x) {
  static uint32_t old_value = x;
  if (x != old_value) {
    old_value = x;
    return (1);
  }
  return (0);
}
boolean hasChanged(int x, int channel) {
  static int old_value[8] = {0, 0, 0, 0, 0, 0, 0, 0};
  if (x != old_value[channel]) {
    old_value[channel] = x;
    return (1);
  }
  return (0);
}

boolean potiHasChanged() {
  static int old_value[8] = {0, 0, 0, 0, 0, 0, 0, 0};
  static boolean letsGo = false;
  for (uint8_t i = 0; i < 8; i++) {
    if (adc_poti_reading[i] != old_value[i]) {
      old_value[i] = adc_poti_reading[i];
      letsGo = true;
      break;
    }
    if (letsGo) {
      letsGo = false;
      return true;
    }
  //  return (0);
  }
  return (0);

}

void checkAndSwitchToScreenSaver() {
  //
  if (millis() - gNoInteractionSince > gScreensaverDelay &&
      !gScreenSaverState) {
    resetScreensaveTimer();
    gScreenSaverState = true;
    switchScreenOff();
    while (true) {
      backgroundFuncs();

//      static unsigned long backlightFlashDelay = millis();
      // if (millis() - backlightFlashDelay > 10000) {
      // 	backlightFlashDelay = millis();
      // 	switchScreenOn();
      // 	delay(2);
      // 	switchScreenOff();
      // }
      // ledChaser_for_screensaver();
      // led_highlight_fader(120, 0);
      if (buttonPressed(EXIT)) {
        resetScreensaveTimer();
        gScreenSaverState = 0;
        //	led_faderBar_reset();		// eine menucomeback funktion
        // einbauen um hier den screensaver wieder zu aktivieren ledchaser taken
        // out due to buggy ledUpdate..system freeze

        switchScreenOn();
        return;
      }
    }
  }
}

void screensaver_init() {
  gNoInteractionSince = millis(); // timer für den screensaver
}

void resetScreensaveTimer() { gNoInteractionSince = millis(); }

void switchScreenOff() { digitalWrite(TFT_BACKLIGHT, LOW); }
void switchScreenOn() { digitalWrite(TFT_BACKLIGHT, HIGH); }

// void checkForScreenSaverTimeout() {
// 	if (millis() - gNoInteractionSince > SCREENSAVER_DELAY ){
// //		screenSaver();
// 	 }
// }

/* TODO:
@PAULE: - screensaver so stricken, dass die erste Interaktion im SleepMode
                        nur das geraet aufweckt aber nicht als userinteraktion
gewertet wird.
                        - den wakeUp fuer die buttons auf die gesamt pinValues
bauen und nicht in der button funktion!
                        - led blinkblink adden (aliveness anzeige)
                        - potis mit firstTouch funktion adden
*/

// boolean screenSaverTimeout() {
// 	static unsigned long noInputSince = millis();
// 	if (anyInput()){
// 		noInputSince= millis();
// 		return(0);
// 	}
//
// 	if (millis() - noInputSince > SCREENSAVER_DELAY )	return(1);
// }

void hallo() {
  static uint8_t halloCount = 0;
  tft.setFont(FONT_8);
  tft.fillRect(0, 0, 60, 10, ILI9341_BLACK);
  tft.setCursor(0, 0);
  tft.printf("Hallo %03d", halloCount);
  halloCount++;
}

void autosaveCounter() {
  static uint8_t counter = 1;
  tft.setCursor(230, 0);
  tft.printf("AUTOSAVE %03d", counter);
  counter++;
}

void schuessi() {
  static uint8_t schuessiCount = 0;
  tft.setFont(FONT_8);
  tft.fillRect(40, 0, 60, 10, ILI9341_BLACK);
  tft.setCursor(40, 0);
  tft.printf("schuessi %03d", schuessiCount);
  schuessiCount++;
}

void ledInit() {
//  int ledHighVal = 30;
//  int ledLowVal = 3;

#ifdef LED_CHIP
  // pullup muss bei loetbarkeit auf EXT umgestellt werden
  //
  // leds.begin();
  // for (int i = 8; i >= 0; i--) {
  //   leds.set_brightness(ledPin[i], 250);
  //}

#endif
}

void led_highlight_fader(int faderNum) {
  if (!ledState[faderNum]) {
#ifdef LED_CHIP
//    leds.set_brightness(ledPin[faderNum], 200);
//			led_highlight_fader[ledPin[faderNum]];
#endif

#ifdef PCA9956
    pwm(faderNum, 255);
#endif

    ledState[faderNum] = 1;
  }
}

void led_highlight_fader(uint16_t brightness, uint16_t faderNum) {
  if (!ledState[faderNum]) {
#ifdef LED_CHIP // GELD 2018
    leds.set_brightness(ledPin[faderNum], brightness);
#endif

#ifdef PCA9956 // GELD 2019!!
    pwm(faderNum, brightness);
#endif

    ledState[faderNum] = 1;
  }
}

void led_faderBar_reset() {
#ifdef LED_CHIP
  for (int i = 0; i < NUM_FADER; i++) {
    leds.set_brightness(i, 5);
    ledState[i] = 0;
  }
#endif

#ifdef PCA9956
  for (int i = 0; i < NUM_FADER; i++) {
    pwm(i, 5);
    ledState[i] = 0;
  }
#endif
}

// helper functions for cross-module access + interaction
void reset_patchBayPositionalVars() {
  // resets global positional variables for patchBay display
  //    (used f.i. in device setup to reset patchbay display positions)
  gSelected_DmxChannel = 0;   // ID in Dmx_Channels[]
  gCurrentPage = 0;           //  tracks ID of page in gChannelGrid[]
  gPositionOnCurrentPage = 0; // tracks ID of gSelected_DmxChannel in
                              // gChannelGrid[gCurrentPage].gridPageItems[]
}
// END helper functions for cross-module access + interaction

// math helper functions
unsigned int reduceIntByIntPercent(unsigned int targetFader_Raw_Val,
                                   unsigned int prozFader_Raw_Val,
                                   unsigned int prozFader_Raw_MaxVal) {
  // integer based percent calculation
  //    - targetFader_Raw_Val is integer value 0..255
  //    - prozFader_Raw_Val is an integer value 0..prozFader_Raw_MaxVal
  //    - function returns prozFader_Raw_Val-reduced integer value of
  //    targetFader_Raw_Val
  //
  //    - uses only int arithmetic
  //    - does rounding
  return ((((targetFader_Raw_Val * 100) * (prozFader_Raw_Val * 100)) /
           (prozFader_Raw_MaxVal * 100)) +
          50) /
         100;
}
// END math helper functions
