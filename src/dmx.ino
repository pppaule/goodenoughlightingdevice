#include <Arduino.h>
#define DMX_UPDATE_INTERVAL 10
#define CRMX_UPDATE_INTERVAL 3
// one full (512chns) dmx frame is 22676 us long, hence an update
// interval of 20 millis is more than enough (500% boost with wireless!!)

int dmxValues[512];

void dmx_init() { Dmx.setMode(TeensyDmx::DMX_OUT); }

void dmxHandler() {
  DMXsendValue[0] = 0;
  static unsigned long dmxUpdateTimer = millis();

  if (millis() - dmxUpdateTimer > DMX_UPDATE_INTERVAL) {
    dmxUpdateTimer = millis();
    for (int i = 0; i < MAXCHANNEL; i++) {

      // simple master fade on 8th fader
      // int reducedInt = reduceIntByIntPercent(255, adc_poti_reading[7], 1023);
      // tft.setCursor(0,0);
      // tft.print(reducedInt);
      //  following line makes fader 8 an masterfader, just for
      //		DMXsendValue[i] =
      //reduceIntByIntPercent(Dmx_Channels[i].faderValue, adc_poti_reading[7],
      //1023);

      DMXsendValue[i] = Dmx_Channels[i].faderValue;
      Dmx.setChannel(i, Dmx_Channels[i].faderValue);

    }
  }

  //	}
} // end of dmxUpdateTimer function
