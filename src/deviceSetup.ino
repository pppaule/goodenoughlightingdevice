
/*  deviceSetup
 *  Setting up connected DMX Devices for GELD
 *
 */

//#include <typeinfo> //excluded this, cuz it made trouble at bootTime..?!

// Console deviceSetup_console = Console(&tft);
int gSelected_connectedDevice = 0;     // the ID in connectedDevices[] of the device in question
int gSelected_DeviceModelID = -1;      // the ID in devicesAvailable[] (a.k.a. device model id). In "device setup" we need the "-1" as "bullshit-detector"
char gSelected_DeviceModelName[11];    // buffer for device name input
int gSelected_DeviceStartChannel = -1; //
int gDisplayGrid_startDeviceID = 0;    // which ID in connectedDevices[] is first in display grid

int gDisplayPositions_deviceSetup[4][3] = {  // x, y, width. for detail screen elements (colored fields). text- & frame-values are derived from this.
											{   5,  55, 300}, // device model
			 								{   5, 115, 140}, // device identifier / name
											{ 165, 115, 140}, // startchannel
											{ 100, 180, 100}  // DELETE button
};

void reset_gSelected_DeviceModelName(){
	for(int i=0;i<11;i++) gSelected_DeviceModelName[i] = '\0';
}

void deviceSetup() {
	// build_autoNamingTable_DO_NOT_USE_ME();

	deviceSetup_initScreen();
	// track + handle changes in the device-grid
	while (true){
		backgroundFuncs();

		connectedDevice_Selector(); // ausgelagerte encoder abfrage

		if (buttonPressed(SELECT)) {
			handle_gSelected_connectedDevice();
			deviceSetup_initScreen();
		}

		if (buttonPressed(EXIT)) {
			//consoleInit = 0;
			menuLightControl.menuListInit();
			menuLightControl.menuListSelect();
			break; // Loop beenden!
		}
	}
}


void handle_gSelected_connectedDevice(){
	// open popup to add/remove a device @ connectedDevices[]

	getDeviceSpecs(); // get user input for device model + device identifier/name
	// connect if device model is selected
	if(gSelected_DeviceStartChannel == -1) gSelected_DeviceStartChannel = next_free_DmxChannel(NUM_FADER);
	if(gSelected_DeviceModelID != -1) { // we actually did something in the setup screen
		if(connectedDevices[gSelected_connectedDevice].deviceModelID == -1){
			// previously there was no device, so connect a new one:
			connect_newDmxDevice (gSelected_connectedDevice, gSelected_DeviceModelName, gSelected_DeviceModelID, gSelected_DeviceStartChannel );
		} else {
			// there was already a device connected, so just rename it:
			sprintf(connectedDevices[gSelected_connectedDevice].name, gSelected_DeviceModelName);
		}
	}
	gSelected_DeviceModelID = -1;
	reset_gSelected_DeviceModelName();
	gSelected_DeviceStartChannel = -1;
}

void getDeviceSpecs() {
	// make popup to enter
	//    - a device model out of devicesAvailable[]
	//    - assign a name to the device
	//    - assign start channel
	reset_gSelected_DeviceModelName();
	deviceSetup_detailScreen();

	int maxEncoderPosition = (connectedDevices[gSelected_connectedDevice].deviceModelID == -1) ? 2 : 3; // DELETE only when device present
	int encoderPosition = (connectedDevices[gSelected_connectedDevice].deviceModelID == -1) ? 0 : 1; // preselect name when device present
	if(connectedDevices[gSelected_connectedDevice].deviceModelID == -1){
		tft.drawRoundRect(gDisplayPositions_deviceSetup[encoderPosition][0]-2, gDisplayPositions_deviceSetup[encoderPosition][1]-2, gDisplayPositions_deviceSetup[encoderPosition][2] + 4, 24, 3, GELD_RED);
		deviceModel_Selector();
		// have to do it again to refresh auto-name ;-)
		deviceSetup_detailScreen();
	}
	tft.drawRoundRect(gDisplayPositions_deviceSetup[encoderPosition][0]-2, gDisplayPositions_deviceSetup[encoderPosition][1]-2, gDisplayPositions_deviceSetup[encoderPosition][2] + 4, 24, 3, GELD_RED);


	while(true){
		// handle encoder and buttons
		tft.drawRoundRect(gDisplayPositions_deviceSetup[encoderPosition][0]-2, gDisplayPositions_deviceSetup[encoderPosition][1]-2, gDisplayPositions_deviceSetup[encoderPosition][2] + 4, 24, 3, GELD_RED);
		backgroundFuncs();
		if(encoderChange(DOWN)) {
			// clear old posFrame
			tft.drawRoundRect(gDisplayPositions_deviceSetup[encoderPosition][0]-2, gDisplayPositions_deviceSetup[encoderPosition][1]-2, gDisplayPositions_deviceSetup[encoderPosition][2] + 4, 24, 3, ILI9341_BLACK);

			// mark new posFrame
			if(connectedDevices[gSelected_connectedDevice].deviceModelID == -1){
				encoderPosition = (encoderPosition < maxEncoderPosition) ? encoderPosition+1 : 0;
			} else {
				encoderPosition = (encoderPosition == maxEncoderPosition) ? 1 : maxEncoderPosition; // works only if we stick to "only name + DELETE" for present devices
			}
			tft.drawRoundRect(gDisplayPositions_deviceSetup[encoderPosition][0]-2, gDisplayPositions_deviceSetup[encoderPosition][1]-2, gDisplayPositions_deviceSetup[encoderPosition][2] + 4, 24, 3, GELD_RED);
		}

		if(encoderChange(UP)) {
			// clear old posFrame
			tft.drawRoundRect(gDisplayPositions_deviceSetup[encoderPosition][0]-2, gDisplayPositions_deviceSetup[encoderPosition][1]-2, gDisplayPositions_deviceSetup[encoderPosition][2] + 4, 24, 3, ILI9341_BLACK);

			// mark new posFrame
			if(connectedDevices[gSelected_connectedDevice].deviceModelID == -1){
				encoderPosition = (encoderPosition == 0) ? maxEncoderPosition : encoderPosition-1;
			} else {
				encoderPosition = (encoderPosition == maxEncoderPosition) ? 1 : maxEncoderPosition; // works only if we stick to "only name + DELETE" for present devices
			}
			tft.drawRoundRect(gDisplayPositions_deviceSetup[encoderPosition][0]-2, gDisplayPositions_deviceSetup[encoderPosition][1]-2, gDisplayPositions_deviceSetup[encoderPosition][2] + 4, 24, 3, GELD_RED);
		}

		if (buttonPressed(SELECT)) {
			if (encoderPosition == 0){ // Model Select
				deviceModel_Selector();
				deviceSetup_detailScreen();
			} else if(encoderPosition == 1){ // Name input
				alphaNumericInput(gSelected_DeviceModelName, " Enter Device Name: ");
				deviceSetup_detailScreen();
			} else if(encoderPosition == 2) { // Start Channel Select
				startChannel_Selector();
				deviceSetup_detailScreen();
			} else { // DELETE
				if(!confirmPopup("Are you sure?")){
					// deviceSetup_detailScreen();
				} else {
					remove_connectedDmxDevice(gSelected_connectedDevice);
					gSelected_DeviceModelID = -1;
					break;
				}
			}
		}

		if (buttonPressed(EXIT)) {
			break; // Loop beenden!
		}
	}
}

void deviceModel_Selector(){
	// encoder-scrolls through devicesAvailable[] and selects gSelected_DeviceModelID
	// model selection may force a re-select of start channel, if previously selected channel doesn't fit the devices channel-needs anymore
	console.printTextAt(10, 60, "scroll to select model..", ILI9341_WHITE, ILI9341_DARKCYAN, 10);
	int sizeOf_elementArray = sizeof(devicesAvailable) / sizeof(devicesAvailable[0]);
	int encoderPosition = -1;
	while(true){
		// handle encoder and buttons
		backgroundFuncs();
		if(encoderChange(DOWN)) {
			encoderPosition = (encoderPosition < sizeOf_elementArray-1) ? encoderPosition + 1 : 0;
			tft.fillRoundRect(5, 55, 300, 20, 3, ILI9341_DARKCYAN);
			console.printTextAt(10, 60, devicesAvailable[encoderPosition].name, ILI9341_WHITE, ILI9341_DARKCYAN, 10);
		}

		if(encoderChange(UP)) {
			encoderPosition = (encoderPosition > 0) ? encoderPosition - 1 : sizeOf_elementArray-1;
			tft.fillRoundRect(5, 55, 300, 20, 3, ILI9341_DARKCYAN);
			console.printTextAt(10, 60, devicesAvailable[encoderPosition].name, ILI9341_WHITE, ILI9341_DARKCYAN, 10);
		}

		if (buttonPressed(SELECT)) {
			if(encoderPosition != -1){
				gSelected_DeviceModelID = encoderPosition;
				// the selected start channel may be too high after selecting a device with more channels:
				if(gSelected_DeviceStartChannel > 511 - devicesAvailable[gSelected_DeviceModelID].numChannels) gSelected_DeviceStartChannel = -1;
				sprintf(gSelected_DeviceModelName, "%s", devicesAvailable[gSelected_DeviceModelID].shortName); // preset name with deviceModel shortName
				break; // Loop beenden!
			}
		}

		if (buttonPressed(EXIT)) {
			break; // Loop beenden!
		}
	}
}

void startChannel_Selector(){
	// encoder-scrolls through 1..512 and selects gSelected_DeviceStartChannel
	// if device model is selected, only allows values from 1..(512-device.numChannels)
	// TODO: @Andreas: wollen wir belegte DMX-bereiche überspringen??? --> dann implementieren...
	console.printTextAt(gDisplayPositions_deviceSetup[2][0] + 5, gDisplayPositions_deviceSetup[2][1] + 5, "select channel..", ILI9341_WHITE, ILI9341_DARKCYAN, 10);
	int sizeOf_elementArray = (gSelected_DeviceModelID == -1) ? 512 : 512 - devicesAvailable[gSelected_DeviceModelID].numChannels; // this leaves the last channel unavailable (which would trigger a display bug)
	int encoderPosition = -1;
	while(true){
		// handle encoder and buttons
		backgroundFuncs();
		if(encoderChange(DOWN)) {
			encoderPosition = (encoderPosition < sizeOf_elementArray-1) ? encoderPosition + 1 : 0;
			tft.fillRoundRect(gDisplayPositions_deviceSetup[2][0], gDisplayPositions_deviceSetup[2][1], gDisplayPositions_deviceSetup[2][2], 20, 3, ILI9341_DARKCYAN);
			char channelText[4] = {'\0'}; sprintf(channelText, "%03d", encoderPosition+1);
			console.printTextAt(gDisplayPositions_deviceSetup[2][0] + 5, gDisplayPositions_deviceSetup[2][1] + 5, channelText, ILI9341_WHITE, ILI9341_DARKCYAN, 10);
		}

		if(encoderChange(UP)) {
			encoderPosition = (encoderPosition > 0) ? encoderPosition - 1 : sizeOf_elementArray-1;
			tft.fillRoundRect(gDisplayPositions_deviceSetup[2][0], gDisplayPositions_deviceSetup[2][1], gDisplayPositions_deviceSetup[2][2], 20, 3, ILI9341_DARKCYAN);
			char channelText[4] = {'\0'}; sprintf(channelText, "%03d", encoderPosition+1);
			console.printTextAt(gDisplayPositions_deviceSetup[2][0] + 5, gDisplayPositions_deviceSetup[2][1] + 5, channelText, ILI9341_WHITE, ILI9341_DARKCYAN, 10);
		}

		if (buttonPressed(SELECT)) {
			gSelected_DeviceStartChannel = encoderPosition;
			break; // Loop beenden!
		}

		if (buttonPressed(EXIT)) {
			break; // Loop beenden!
		}
	}
}


// display + interaction functions:
void deviceSetup_initScreen(){
	// main screen for connectedDevices[] select
	// tft.setTextColor(ILI9341_WHITE, ILI9341_BLACK);
	// tft.setFont(FONT_10);
	console.printHeader(" Device Setup Menu ");
	display_DeviceGrid();
}

void display_DeviceGrid(){
	// display grid of connectedDevices[]:
	// TODO: @paule:
	// add int Console.gridSelector(rows, columns) function to consoleLib
	// add int Console.gridItem(const char* name, int bg_color, int border_color, int selct_color)
	// add struct gridItem[MAX_CONNECTED_DEVICES]
	int gridCols = 2; int gridRows = 8; // gridCols that work out-of-the-box: 1,2,3,4
	int labelWidth = 288/gridCols; int labelHeigth = 20;
	// tft.setFont(FONT_10);
	display_DeviceGridPage(gridCols, gridRows, labelWidth, labelHeigth);
}

void display_DeviceGridPage(int gridCols, int gridRows, int labelWidth, int labelHeigth) {
	for(int row = 0; row < gridRows; row++){
		for(int col = 0; col < gridCols; col++){
			// display grid element:
			int deviceNum = gDisplayGrid_startDeviceID + (row * gridCols) + col;
			char text[8]; //sprintf(text, "");
			if (connectedDevices[deviceNum].deviceModelID != -1) {
				// device connected
				sprintf(text, "%03d-%03d", connectedDevices[deviceNum].startChannel + 1, connectedDevices[deviceNum].startChannel + devicesAvailable[connectedDevices[deviceNum].deviceModelID].numChannels);
				tft.fillRoundRect(col * (labelWidth+6) + 5, row * (labelHeigth+5) + 32, labelWidth, labelHeigth, 3, ILI9341_CYAN);
				console.printTextAt(col * (labelWidth+6) + 9, row * (labelHeigth+5) + 39, text, GELD_GREY2, ILI9341_CYAN, 8);
				console.printTextAt(col * (labelWidth+6) + 50, row * (labelHeigth+5) + 37, connectedDevices[deviceNum].name, GELD_GREY2, ILI9341_CYAN, 10);
			} else {
				// no device connected
				tft.fillRoundRect(col * (labelWidth+6) + 5, row * (labelHeigth+5) + 32, labelWidth, labelHeigth, 3, GELD_GREY2);
				console.printTextAt(col * (labelWidth+6) + 9, row * (labelHeigth+5) + 39, text, ILI9341_BLACK, GELD_GREY2, 8);
				console.printTextAt(col * (labelWidth+6) + 30, row * (labelHeigth+5) + 37, "[EMPTY]", ILI9341_BLACK, GELD_GREY2, 10);
			}
			// tft.setCursor(col * (labelWidth+6) + 9, row * (labelHeigth+5) + 37);
			// tft.printf("%02d", deviceNum + 1);
		}
	}
}

void connectedDevice_Selector() {
	// select a deviceNum from displayed connectedDevice-grid and store in gSelected_connectedDevice

	// make that vars global? --> also used in display_DeviceGrid()...
	int gridCols = 2; int gridRows = 8; // gridCols that work out-of-the-box: 1,2,3,4
	int labelWidth = 288/gridCols; int labelHeigth = 20;

	int thisPage_selectedDeviceID = gSelected_connectedDevice - gDisplayGrid_startDeviceID; // a.k.a. position on this page of grid
	tft.drawRoundRect((thisPage_selectedDeviceID % gridCols) * (labelWidth+6) + 4, ( thisPage_selectedDeviceID / gridCols) * (labelHeigth+5) + 31, labelWidth+2, labelHeigth+2,3, GELD_RED);

	if(encoderChange(DOWN)) {
		tft.drawRoundRect((thisPage_selectedDeviceID % gridCols) * (labelWidth+6) + 4, ( thisPage_selectedDeviceID / gridCols) * (labelHeigth+5) + 31, labelWidth+2, labelHeigth+2,3, GELD_BLACK);
	 	gSelected_connectedDevice++;
	 	if(gSelected_connectedDevice > 31){
			gSelected_connectedDevice = 0; // close ID-circle
			gDisplayGrid_startDeviceID = 0; // close page-circle
			display_DeviceGridPage(gridCols, gridRows, labelWidth, labelHeigth);
		}
		if (gSelected_connectedDevice - gDisplayGrid_startDeviceID >= (gridCols * gridRows) ) {
			// next page
			gDisplayGrid_startDeviceID += (gridCols * gridRows);
			display_DeviceGridPage(gridCols, gridRows, labelWidth, labelHeigth);
		}
		thisPage_selectedDeviceID = gSelected_connectedDevice - gDisplayGrid_startDeviceID; // recalc position on this page of grid
		tft.drawRoundRect((thisPage_selectedDeviceID % gridCols) * (labelWidth+6) + 4, ( thisPage_selectedDeviceID / gridCols) * (labelHeigth+5) + 31, labelWidth+2, labelHeigth+2,3, GELD_RED);
	}

	if(encoderChange(UP)) {
		tft.drawRoundRect((thisPage_selectedDeviceID % gridCols) * (labelWidth+6) + 4, ( thisPage_selectedDeviceID / gridCols) * (labelHeigth+5) + 31, labelWidth+2, labelHeigth+2,3, GELD_BLACK);
	 	gSelected_connectedDevice--;
	 	if (gSelected_connectedDevice < 0) {
			gSelected_connectedDevice = 31; // close ID-circle
			gDisplayGrid_startDeviceID = 32 - (gridCols * gridRows); // close page-circle
			display_DeviceGridPage(gridCols, gridRows, labelWidth, labelHeigth);
		}
		if (gSelected_connectedDevice - gDisplayGrid_startDeviceID < 0 ) {
			// previous page
			gDisplayGrid_startDeviceID -= (gridCols * gridRows);
			display_DeviceGridPage(gridCols, gridRows, labelWidth, labelHeigth);
		}
		thisPage_selectedDeviceID = gSelected_connectedDevice - gDisplayGrid_startDeviceID; // recalc position on this page of grid
		tft.drawRoundRect((thisPage_selectedDeviceID % gridCols) * (labelWidth+6) + 4, ( thisPage_selectedDeviceID / gridCols) * (labelHeigth+5) + 31, labelWidth+2, labelHeigth+2,3, GELD_RED);
	}
}

void deviceSetup_detailScreen(){
	// detail screen for device@connectedDevices[]
	if(connectedDevices[gSelected_connectedDevice].deviceModelID != -1) {
		// when re-entering setup screen for already connected device -> preset values with devices values
		gSelected_DeviceModelID = connectedDevices[gSelected_connectedDevice].deviceModelID;
		if(strlen(gSelected_DeviceModelName) < 2) sprintf(gSelected_DeviceModelName, connectedDevices[gSelected_connectedDevice].name);
		gSelected_DeviceStartChannel = connectedDevices[gSelected_connectedDevice].startChannel;
		console.printHeader(" Device Setup - Edit Connected Device ");
	} else {
		console.printHeader(" Device Setup - Define Connected Device ");
	}
	int height = 20;

	// device model:
	console.printTextAt(gDisplayPositions_deviceSetup[0][0], gDisplayPositions_deviceSetup[0][1] - 15, "connected Device:", ILI9341_WHITE, ILI9341_BLACK, 10);
	if(connectedDevices[gSelected_connectedDevice].deviceModelID == -1) tft.fillRoundRect(gDisplayPositions_deviceSetup[0][0], gDisplayPositions_deviceSetup[0][1], gDisplayPositions_deviceSetup[0][2], height, 3, ILI9341_DARKCYAN);
	if(connectedDevices[gSelected_connectedDevice].deviceModelID == -1 || gSelected_DeviceModelID == -1){
		console.printTextAt(gDisplayPositions_deviceSetup[0][0] + 5, gDisplayPositions_deviceSetup[0][1] + 5, (gSelected_DeviceModelID == -1) ? "<Select Model>" : devicesAvailable[gSelected_DeviceModelID].name, ILI9341_WHITE, ILI9341_DARKCYAN, 10);
	} else {
		console.printTextAt(gDisplayPositions_deviceSetup[0][0] + 5, gDisplayPositions_deviceSetup[0][1] + 5, devicesAvailable[gSelected_DeviceModelID].name, ILI9341_WHITE, ILI9341_BLACK, 10);
	}

	// device name:
	console.printTextAt(gDisplayPositions_deviceSetup[1][0], gDisplayPositions_deviceSetup[1][1] - 15, "Device Identifier:", ILI9341_WHITE, ILI9341_BLACK, 10);
	tft.fillRoundRect(gDisplayPositions_deviceSetup[1][0], gDisplayPositions_deviceSetup[1][1], gDisplayPositions_deviceSetup[1][2], height, 3, ILI9341_DARKCYAN);
	if(connectedDevices[gSelected_connectedDevice].deviceModelID == -1){
		console.printTextAt(gDisplayPositions_deviceSetup[1][0] + 5, gDisplayPositions_deviceSetup[1][1] + 5, ((gSelected_DeviceModelName[0] != '\0') ? gSelected_DeviceModelName : "<Name Device>"), ILI9341_WHITE, ILI9341_DARKCYAN, 10);
	} else {
		console.printTextAt(gDisplayPositions_deviceSetup[1][0] + 5, gDisplayPositions_deviceSetup[1][1] + 5, gSelected_DeviceModelName, ILI9341_WHITE, ILI9341_DARKCYAN, 10);
	}

	// device start channel:
	console.printTextAt(gDisplayPositions_deviceSetup[2][0], gDisplayPositions_deviceSetup[2][1] - 15, "Start Channel:", ILI9341_WHITE, ILI9341_BLACK, 10);
	if(connectedDevices[gSelected_connectedDevice].deviceModelID == -1) tft.fillRoundRect(gDisplayPositions_deviceSetup[2][0], gDisplayPositions_deviceSetup[2][1], gDisplayPositions_deviceSetup[2][2], height, 3, ILI9341_DARKCYAN);
	char channelTextAuto[4] = {'\0'}; sprintf(channelTextAuto, "%03d", next_free_DmxChannel(NUM_FADER)+1);
	char channelText[4] = {'\0'}; sprintf(channelText, "%03d", gSelected_DeviceStartChannel+1);
	if(connectedDevices[gSelected_connectedDevice].deviceModelID == -1 || gSelected_DeviceStartChannel == -1){
		console.printTextAt(gDisplayPositions_deviceSetup[2][0] + 5, gDisplayPositions_deviceSetup[2][1] + 5, (gSelected_DeviceStartChannel == -1) ? channelTextAuto : channelText, ILI9341_WHITE, ILI9341_DARKCYAN, 10);
	} else {
		console.printTextAt(gDisplayPositions_deviceSetup[2][0] + 5, gDisplayPositions_deviceSetup[2][1] + 5, channelText, ILI9341_WHITE, ILI9341_BLACK, 10);
	}

	if(connectedDevices[gSelected_connectedDevice].deviceModelID != -1) {
		// tft.setFont(FONT_10);
		tft.fillRoundRect(gDisplayPositions_deviceSetup[3][0], gDisplayPositions_deviceSetup[3][1], gDisplayPositions_deviceSetup[3][2], height, 3, ILI9341_DARKCYAN);
		console.printTextAt(gDisplayPositions_deviceSetup[3][0] + 25, gDisplayPositions_deviceSetup[3][1] + 5, "DELETE", ILI9341_WHITE, ILI9341_DARKCYAN, 10);
		// tft.setFont(FONT_8);
	}
}


// ENDE display + interaction functions:

// add-/remove-functions for device-list connectedDevices[]
uint16_t next_free_DmxChannel(int rowLength){
	// returnes the next startChannel that starts a new row of <rowLength>
	//         (channels start at 0, presently <rowLength> would be 8)
	//         bsp: dev#1 start:0, numChannels:12 --> uses Dmx_Channels[[0..11]
	//              next_free_DmxChannel(8) --> 16
	// beware: always gives address after highest used channel of all devices.

	uint16_t nextFreeAddress = 0;

	for(int i=0; i<MAX_CONNECTED_DEVICES; i++){
		if(connectedDevices[i].deviceModelID != -1){
			uint16_t lastUsedAddress = connectedDevices[i].startChannel + devicesAvailable[connectedDevices[i].deviceModelID].numChannels;
			uint16_t wouldBe_nextFreeAddress = floor( (lastUsedAddress + rowLength) / rowLength ) * rowLength;
			if(nextFreeAddress <= wouldBe_nextFreeAddress) nextFreeAddress = wouldBe_nextFreeAddress;
		}
	}
	if(nextFreeAddress < NUM_DMX_CHANNELS) {
		return(nextFreeAddress);
	} else {
		return(901); // 901: no more address-space behind last device
	}
}

void connect_newDmxDevice (uint8_t deviceNum, char * name, int deviceModelID, uint16_t startAddress) {
	// Adds a new device to connectedDevices[] (and assignes channels starting with <startAddress>)
	//		Only works, if there are no conflicting connected devices, otherwise doesn't do anything.
	// <deviceNum> 0..31 (a.k.a. MAX_CONNECTED_DEVICES)
	// bsp:
	//      connect_newDmxDevice (0, "my_SP60_01", &(devicesAvailable[0]), 16);

	// check for occupied channels:
	bool conflict = false;
	for(int i = 0; i < devicesAvailable[deviceModelID].numChannels; i++){
		if(Dmx_Channels[startAddress+i].deviceConnected) { // if i use deviceConnected instead, code crashes
			conflict = true;
			break;
		}
	}
	if(!conflict) {
		strncpy(connectedDevices[deviceNum].name, name, strlen(name)); // avoid obscure "i lost my name along the way"-bug 1/2
		connectedDevices[deviceNum].name[sizeof(connectedDevices[deviceNum].name) - 1] = '\0'; // avoid obscure "i lost my name along the way"-bug 2/2
		connectedDevices[deviceNum].deviceModelID 	= deviceModelID;
		connectedDevices[deviceNum].startChannel 	= startAddress;
		for(int i = 0; i < devicesAvailable[deviceModelID].numChannels; i++){
			Dmx_Channels[startAddress+i].deviceConnected = true;
			Dmx_Channels[startAddress+i].connectedDeviceID = deviceNum;
			Dmx_Channels[startAddress+i].deviceChannelID = i;
		}
		reset_patchBayPositionalVars();
	} else {
		// error("This channel area is already in use by a device!")
	}
}

void remove_connectedDmxDevice(uint8_t deviceNum){
	// removes connectedDevices[deviceNum] and frees used channels in Dmx_Channels[]
	int startChannel = connectedDevices[deviceNum].startChannel;
	int endChannel = connectedDevices[deviceNum].startChannel + devicesAvailable[connectedDevices[deviceNum].deviceModelID].numChannels;
	for(int i = startChannel; i < endChannel; i++){
		Dmx_Channels[i].deviceConnected = false;
		Dmx_Channels[i].connectedDeviceID = -1;
		Dmx_Channels[i].deviceChannelID = {};
	}
	connectedDevices[deviceNum] = {};
	connectedDevices[deviceNum].deviceModelID 	= -1;
	reset_patchBayPositionalVars();
}
// ENDE add-/remove-functions for device-list connectedDevices[]
