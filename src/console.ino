
//Console
// single chnnel dmx controller


uint16_t console_ChannelStartOffset = 0; // separately, to remember selected page between view-switches

void consoleDisplay() {
	gThisView = 'S'; // S: singleChannel
	console.setBarColor(GELD_GREEN, GELD_GREY1);     //definition der formatierung
	console.setBarDimensions(20, 105, 6);
	// console.setBarCoordinates(20,68);
	console.setBarCoordinates(12,68);

	sprintf(gTitle, "%s", " DMX Single Channel Control ");

	reset_potiVals(console_ChannelStartOffset);
	console.update_gDisplaySliderBars(console_ChannelStartOffset);
	console.initAndDisplay_SliderBarGroup(NUM_FADER, gTitle);
	gDoRefresh = true;

	while (true) {
		backgroundFuncs();
		flashButton_readAndProcess(console_ChannelStartOffset);

		// check encoder
		if (encoderChange(DOWN)) {
			console_ChannelStartOffset = (console_ChannelStartOffset + NUM_FADER >= NUM_DMX_CHANNELS) ? 0 : console_ChannelStartOffset + NUM_FADER;
			gDoRefresh = true;
			reset_potiVals(console_ChannelStartOffset);
			console.printHeader(gTitle);
		}

		if (encoderChange(UP)) {
			console_ChannelStartOffset = (console_ChannelStartOffset - NUM_FADER < 0) ? NUM_DMX_CHANNELS-NUM_FADER : console_ChannelStartOffset - NUM_FADER;
			gDoRefresh = true;
			reset_potiVals(console_ChannelStartOffset);
			console.printHeader(gTitle);
		}


		readAndUpdate_SingleChannel_PotiVals(console_ChannelStartOffset);
		if(gDoRefresh){
			console.update_gDisplaySliderBars(console_ChannelStartOffset);
			console.refresh_SliderBarGroup(NUM_FADER);
			led_faderBar_reset();
			for(int c = 0; c<NUM_FADER; c++){

				if(Dmx_Channels[console_ChannelStartOffset + c].deviceConnected && Dmx_Channels[console_ChannelStartOffset + c].deviceChannelID == 0){ // && Dmx_Channels[console_ChannelStartOffset + c].deviceChannelID != 0){
					// print device names. this works only if connectedDevice->name is REALLY max 10 char long
					// 		and devices right beside each other have AT LEAST 2 channels:
					char text[10]; sprintf(text, "%s", connectedDevices[Dmx_Channels[console_ChannelStartOffset + c].connectedDeviceID].name);
					console.printTextAt((c *  38) + 10, 28, text, ILI9341_WHITE, ILI9341_BLACK,9);  // DEVICE NAME:
				}
			}
			gDoRefresh = false;
		}

		if (buttonPressed(SELECT)) {    //SELECT fuehrt ins Kanal-Modifikationsmenue
			consoleSubMenu( console_ChannelStartOffset);
			console.initAndDisplay_SliderBarGroup(NUM_FADER, gTitle);
		}

		if (buttonPressed(EXIT))  {
			console.set_init_flag(0);
			led_faderBar_reset();
			for (int i = 0; i < NUM_FADER; i++) poti_catch_firstCall[i] = true;

			menuLightControl.menuListInit();
			menuLightControl.menuListSelect();
			break;            // Console beenden!
		}
	}   //end of lipstickParty
}     //end of consoleDisplay

static boolean ledState[8] = {0};

void readAndUpdate_SingleChannel_PotiVals(uint16_t console_ChannelStartOffset){
	// reads fader-positions of Dmx_Channels[], +channelOffset
	//       and updates relevant Dmx_Channels[].faderValue

	for (int i = 0; i < NUM_FADER ; i++) {
		// potiVal[i]: 0..1024  potiRead[i]: 0..255
		potiVal[i] = poti_smooth(potiVal[i], i);
		if (hasChanged(potiVal[i],i)) {

			potiRead[i] = poti_catch_val(poti_map_8bit(potiVal[i]), Dmx_Channels[console_ChannelStartOffset + i].faderValue, poti_catch_firstCall[i], i); // wenn helligkeit
			poti_catch_firstCall[i] = false;

			if (potiRead[i] <= 0) potiRead[i] = 0 ;		// lower limit Korrektur.

			if (Dmx_Channels[console_ChannelStartOffset + i].faderValue != potiRead[i]) {	// erfasse potiaenderung nur wenn neuer wert vorliegt.
				Dmx_Channels[console_ChannelStartOffset + i].faderValue = potiRead[i]; // store global fader values:
				console.update_singleBar_in_gDisplaySliderBars(console_ChannelStartOffset, i);
				console.refresh_SliderBar_inGroup(i);
				resetScreensaveTimer();
			}
		}
	}
}


void flashButton_readAndProcess(uint16_t console_ChannelStartOffset) {
	static uint8_t storedValue[NUM_FADER];
	uint8_t targetValue = 255;
	static bool pressed[8] = {0,0,0,0,0,0,0,0};

	for (int i = 0; i < NUM_FADER ; i++) {
		if (buttonChnPressed(i)) {
			pressed[i] = true;
			Serial.println(i);
			storedValue[i] = Dmx_Channels[console_ChannelStartOffset + i].faderValue;
			Dmx_Channels[console_ChannelStartOffset + i].faderValue = targetValue;
			console.update_singleBar_in_gDisplaySliderBars(console_ChannelStartOffset, i);
			console.refresh_SliderBar_inGroup(i);
			}

		if (pressed[i]){
			Dmx_Channels[console_ChannelStartOffset + i].faderValue = targetValue;			// debounce function so
		}

		if (buttonChnReleased(i) ) {
			pressed[i] = false;
			Dmx_Channels[console_ChannelStartOffset + i].faderValue = storedValue[i];
			console.update_singleBar_in_gDisplaySliderBars(console_ChannelStartOffset, i);
			console.refresh_SliderBar_inGroup(i);
		}
 }
}


void consoleSubMenu(uint16_t console_ChannelStartOffset) {
	int8_t selectedFaderButton = faderSelectPopup("Select Channel!"); //("SELECT for global options, ");  TODO: May be do an "global options menu on 2nd SELECT click"??
																										//	--> for "reset all channels", "save scene"...
	if(selectedFaderButton == -1) return; // channel-select exited without select

	TftMenuLib subMenu_console(20, 40, 260, 40, 12,20, subMenu_consoleItems, subMenu_consoleFunc, numSubConsole,&tft );
	subMenu_console.selectedChannel = selectedFaderButton + console_ChannelStartOffset + 1; // here we use the 1..512 notation. why?

	subMenu_console.menuWindowInit();
	subMenu_console.menuListSelect();

	while (true) {
		backgroundFuncs();

		if (encoderChange(UP)) {
			subMenu_console.menuListSelect(UP);
		}

		if (encoderChange(DOWN)) {          //encoder reading downward
			subMenu_console.menuListSelect(DOWN);
		}

		if (buttonPressed(SELECT)) {
			subMenu_consoleFunc[subMenu_console.menu_select - 1](selectedFaderButton);
			// TODO: whatever should happen here...?
			//subMenu_consoleFunc[subMenu_console.menu_select]();
		}

		if (buttonPressed(EXIT))  {
			console.set_init_flag(0); // TODO: @PAULE: wozu ist set_init_flag?
															 // Keine Ahnung, aber hier kommt gleich nochmal ne Submenu Init flag für die scrollfunktion
			subMenu_console.reset_submenu();

			return;         // Console beenden!
		}
	} //end of pyjamaParty
}       // end of consoleSubMenu
