#include <Arduino.h>

#include "headers.h"
#include "menuItems.h" // hier befinden sich die ganzen menu-unterpunkte

//#include <chipDefines.h>

bool bootMenuPatch = 0; // parameter für die eepromBootOption ob direkt beim
                        // boot in die patchbay gegeangen werden soll.

void init_unused_cs_pins() {
  pinMode(CS_FLASH, OUTPUT);
  digitalWrite(CS_FLASH, HIGH);
  pinMode(SD_CS, OUTPUT);
  digitalWrite(SD_CS, HIGH);
  pinMode(CS_TOUCH, OUTPUT);
  digitalWrite(CS_TOUCH, HIGH);
  pinMode(timo_csPin, INPUT_PULLUP);
  digitalWrite(timo_csPin, HIGH);
  i2cInit();
}

void setup() {
  Serial.begin(115200);
  SPI.begin();
  init_unused_cs_pins();
  init_mcp23017();
  // batteryGaugePrintOut();
  adc_init();
  potiCatch_init();
  dmx_init();
  shift_regs_init();
  display_init();

  init_sd();
  copy_scenes_from_sd_to_ram();

  special_startup_sequence();
  screensaver_init();
  // tft.println("Hi Sucker");
  EEPROMrestoreFunction(); // disabled, function hangs when brand new teensy /
                           // clean eeprom.
  eeprom_initialize();

#ifdef PCA9956
  init_pca9956();
#endif
}

void loop() {
  // blockbuster();
  //  consoleDisplay();
  mainMenu();
}

void backgroundFuncs() {
  // BackgroundFunctions to be executed inside while-loops THIS RUNS ALWAYS!

  if (gScreenSaverEnabled)
    checkAndSwitchToScreenSaver(); // this locks into screensaver until
                                   // exit_button pressed
  newEncPosition = myEnc.read();   // uopdates encode
  buttonQuery();
  dmxHandler();

  if (!gScreenSaverState) {
    static unsigned long adcReadTimer = millis();

    if (millis() - adcReadTimer > ADC_READ_INTERVAL) {
      adcReadTimer = millis();
      mcp3008_channels_read();
    }
  }
  if (gAutosaveEnabled)
    autoSave_eeprom(gAutosaveDelay);
  bottomBar_monitor();
#ifdef SD_LOGGING
  battery_logger(2); // logging batt values to sd card. arg is interval in sec's
#endif
}
