#include <Arduino.h>





void display_init() {
  pinMode (TFT_BACKLIGHT, OUTPUT);
  digitalWrite(TFT_BACKLIGHT, HIGH);
  tft.begin();
  tft.fillScreen(ILI9341_BLACK);
  tft.setRotation(1);
  old_display_draw = display_draw;
}


void menu_alert_rect(){
  tft.fillRect(40, 50, 250,100, ILI9341_BLACK);
  tft.drawRect(42, 52, 246, 96, ILI9341_RED);
}
