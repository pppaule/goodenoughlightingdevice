

/*
  SD card test

 This example shows how use the utility libraries on which the'
 SD library is based in order to get info about your SD card.
 Very useful for testing a card when you're not sure whether its working or not.

 The circuit:
  * SD card attached to SPI bus as follows:
 ** MOSI - pin 11 on Arduino Uno/Duemilanove/Diecimila, pin 7 on Teensy with
 audio board
 ** MISO - pin 12 on Arduino Uno/Duemilanove/Diecimila
 ** CLK - pin 13 on Arduino Uno/Duemilanove/Diecimila, pin 14 on Teensy with
 audio board
 ** CS - depends on your SD card shield or module - pin 10 on Teensy with audio
 board Pin 4 used here for consistency with other Arduino examples


 created  28 Mar 2011
 by Limor Fried
 modified 9 Apr 2012
 by Tom Igoe
 */
// include the SD library:
// #include <SD.h>
// #include <SPI.h>
//
// // set up variables using the SD utility library functions:
// Sd2Card card;
// SdVolume volume;
// SdFile myFile;
// // SdFile myFile;
//
// const int chipSelect = 20;
//
// void check_sd_files() {
//   tft.print("Initializing SD card...");
//
//   if (!SD.begin(chipSelect)) {
//     tft.println("initialization failed!");
//     return;
//   }
//   tft.println("initialization done.");
//
//   if (SD.exists("example.txt")) {
//     tft.println("example.txt exists.");
//   } else {
//     tft.println("example.txt doesn't exist.");
//   }
//
//   // open a new file and immediately close it:
//   tft.println("Creating example.txt...");
//   myFile = SD.open("example.txt", FILE_WRITE);
//   myFile.close();
//
//   // Check to see if the file exists:
//   if (SD.exists("example.txt")) {
//     tft.println("example.txt exists.");
//   } else {
//     tft.println("example.txt doesn't exist.");
//   }
//
//   // delete the file:
//   tft.println("Removing example.txt...");
//   SD.remove("example.txt");
//
//   if (SD.exists("example.txt")) {
//     tft.println("example.txt exists.");
//   } else {
//     tft.println("example.txt doesn't exist.");
//   }
// }
//
// void check_sd_card() {
//   // UNCOMMENT THESE TWO LINES FOR TEENSY AUDIO BOARD:
//   // SPI.setMOSI(7);  // Audio shield has MOSI on pin 7
//   // SPI.setSCK(14);  // Audio shield has SCK on pin 14
//
//   tft.print("\nInitializing SD card...");
//
//   //   // we'll use the initialization code from the utility libraries
//   //   // since we're just testing if the card is working!
//   if (!card.init(SPI_HALF_SPEED, chipSelect)) {
//     tft.println("initialization failed. Things to check:");
//     tft.println("* is a card inserted?");
//     tft.println("* is your wiring correct?");
//     tft.println(
//         "* did you change the chipSelect pin to match your shield or
//         module?");
//     return;
//   } else {
//     tft.println("Wiring is correct and a card is present.");
//   }
//
//   // print the type of card
//   tft.print("\nCard type: ");
//   switch (card.type()) {
//   case SD_CARD_TYPE_SD1:
//     tft.println("SD1");
//     break;
//   case SD_CARD_TYPE_SD2:
//     tft.println("SD2");
//     break;
//   case SD_CARD_TYPE_SDHC:
//     tft.println("SDHC");
//     break;
//   default:
//     tft.println("Unknown");
//   }
//
//   //   // Now we will try to open the 'volume'/'partition' - it should be
//   FAT16
//   //   or
//   // FAT32
//   if (!volume.init(card)) {
//     tft.println("Could not find FAT16/FAT32 partition.\n");
//     return;
//   }
//
//   // print the type and size of the first FAT-type volume
//   uint32_t volumesize;
//   tft.print("\nVolume type is FAT");
//   tft.println(volume.fatType(), DEC);
//   tft.println();
//
//   volumesize = volume.blocksPerCluster(); // clusters are collections of
//   volumesize *= volume.clusterCount();    // we'll have a lot of
//   // clusters if (volumesize < 8388608ul) {
//   tft.print("Volume size (bytes): ");
//   tft.println(volumesize * 512); // SD card blocks are always 512 bytes
//
//   tft.print("Volume size (Kbytes): ");
//   volumesize /= 2;
//   tft.println(volumesize);
//   tft.print("Volume size (Mbytes): ");
//   volumesize /= 1024;
//   tft.println(volumesize);
//
//   tft.println("\nFiles found on the card (name, date and size in bytes):");
//   // if (!root.openRoot(volume))
//   //   tft.print("opening root failed");
//
//   // list all files in the card with date and size
//   // root.ls(LS_R | LS_DATE | LS_SIZE);
// }

//

void init_sd() {
  // not checky, whacky, whacky
  bool ok;
  const int chipSelect = SD_CS;
  SD.sdfs.begin(SdSpiConfig(chipSelect, SHARED_SPI, SD_SCK_MHZ(24)));
}

void print_sd_root_directory() {
  tft.println("Print directory using SD functions");
  File root = SD.open("/");
  while (true) {
    File entry = root.openNextFile();
    if (!entry)
      break; // no more files
    tft.print(entry.name());
    if (entry.isDirectory()) {
      tft.println("/");
    } else {
      printSpaces(40 - strlen(entry.name()));
      tft.print("  ");
      tft.println(entry.size(), DEC);
    }
    entry.close();
  }
}

//
// void printTime(const DateTimeFields tm) {
//   const char *months[12] = {"January",   "February", "March",    "April",
//                             "May",       "June",     "July",     "August",
//                             "September", "October",  "November", "December"};
//   if (tm.hour < 10)
//     tft.print('0');
//   tft.print(tm.hour);
//   tft.print(':');
//   if (tm.min < 10)
//     tft.print('0');
//   tft.print(tm.min);
//   tft.print("  ");
//   tft.print(tm.mon < 12 ? months[tm.mon] : "???");
//   tft.print(" ");
//   tft.print(tm.mday);
//   tft.print(", ");
//   tft.print(tm.year + 1900);
// }

void printSpaces(int num) {
  for (int i = 0; i < num; i++) {
    tft.print(" ");
  }
}
