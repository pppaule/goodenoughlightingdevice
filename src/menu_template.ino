//
// HOWTO: GELD MENU
//
// // initialize an array with the title text to be printed in the menu
// char *random_menu[] = {
//   (char *)"sometimes a header line ",
//   (char *)" function 1 ",
//   (char *)" function 2 ",
//   (char *)" ... ",
//   (char *)" ..."
// };
//
// // get the length of the array
// const uint8_t num_random_menu = (sizeof(chasers) / sizeof(char *));
//
// // initialize array of function pointers (functions have to be initialized in
// headers.h!) MenuFuncPtr *random_menu_func[] = {
//   ,//blank space for header line -> no function connected
//   function 1,
//   function 2,
//   ...,
//   ...
// };
//
// //getting length of function pointer array
// const uint8_t num_random_menu_func = (sizeof(chasers) / sizeof(char *));
//
// // if this gets generalized, please include testing num_random_menu against
// // function_pointer_array.improve naming.
//
// //initialize TftMenuLib Object
// TftMenuLib
//     menu_random(random_menu, random_menu_func, num_random_menu,
//              &tft);
//
// // put this in the loop
// void randomMenu() {
// randomMenu.menuListInit();
// randomMenu.menuListSelect();
// while(true){
// backgroundFuncs();
// if (encoderChange(UP)) {
//    randomMenu.menuListSelect(UP);
// }
// if (encoderChange(DOWN)) {
//  randomMenu.menuListSelect(DOWN);
// }
// if (buttonPressed(SELECT)) {
//      randomMenu_func[randomMenu.menu_select]();
//      return;
// }
// if (buttonPressed(EXIT)) {
//       return;
//     }
//   }
// }
//
// // finish functions in menu with randomMenuInit();
//  */


// void blockbuster(){
//   console_blckbstr.setBarColor(GELD_GREEN, GELD_GREY1);     //definition der formatierung
//   console_blckbstr.setBarDimensions(20, 105, 6);
//   // console_blckbstr.setBarCoordinates(20,68);
//   console_blckbstr.setBarCoordinates(12,68);
//
//   sprintf(gTitle, "%s", " GELD - Blockbuster ");
//
// while (1){
//   backgroundFuncs();
//   if (encoderChange(DOWN)) {
//   }
//
//   if (encoderChange(UP)) {
//   }
//   if (buttonPressed(SELECT){
//   }
//   if (buttonPressed(EXIT){
//   }
//   if (buttonPressed(SHIFT){
//   }
// }
// }



// char *experimental_menu[] = {(char *)" Experimental Functions ",
//                              (char *)" Battery Monitor ",
//                              (char *)" Blockbuster ",
//                              (char *)" FlickiFlocki ",
//                              (char *)" ClrSlctrExp ",
//
//                              (char *)" Bouncing Ball",
//                              (char *)" Knight Rider"};
// const uint8_t num_experimental_menu =
//     (sizeof(experimental_menu) / sizeof(char *));
//
// MenuFuncPtr *menu_experimental_func[] = {NULL,
//                                          batterylog_monitor_menu,
//                                          blockbuster,
//                                          draw_menu_efx,
//                                          colorSelectMenu,
//
//                                          bouncingBall,
//                                          knightrider};
//
// const uint8_t num_experimental_menu_func =
//     (sizeof(menu_experimental_func) / sizeof(char *));
//
// TftMenuLib experimental(
//     experimental_menu, menu_experimental_func, num_experimental_menu,
//     &tft); // globale Initialisierung des Hauptmenu als library instanz
//
// void menuExperimental() {
//   experimental.menuListInit();
//   experimental.menuListSelect();
//
//   while (true) {
//     backgroundFuncs();
//     if (encoderChange(UP)) {
//       experimental.menuListSelect(UP);
//
//     }
//     if (encoderChange(DOWN)) {
//       experimental.menuListSelect(DOWN);
//
//     }
//     if (buttonPressed(SELECT)) {
//       menu_experimental_func[experimental.menu_select]();
//
//     }
//     if (buttonPressed(EXIT)) {
//       return;
//     }
//   }
// }
