// WIP

// G. E. L. D. -> Scene Controller
//
// currently 8 scenes are supported
// Save Scene in patchBay or console subMenu
// restore in sceneControl with Channel Buttons
//
// trying to crossfade between scenes
// some bugs though:

// buttons start reacting after second button has been prossed
const int debuglevel = 4; // global debuglevel 0 - 4 (0 means no feedback, 4
                          // means everything printed to tft);
void restoreSceneFromRam(uint16_t);

// main Console for Scene Control

void sceneControl_console() {

  char buffer[50];
  // infoPopup
  copy_scenes_from_sd_to_ram();
  tft.fillScreen(ILI9341_BLACK);
  tft.setCursor(20, 20);
  tft.setFont(FONT_10);

  init_sd();

  static uint8_t lastScene;
  for (int i = 0; i < NUM_FADER; i++) {
    drawSceneButton(i, ILI9341_ORANGE, ILI9341_BLACK);
    printSceneButton(i, ILI9341_ORANGE, ILI9341_BLACK);
  }
  unsigned long sdcard_timer = millis();
  tft.setCursor(0, 80);
  // save_scene_to_sd(1);
  tft.setCursor(0, 0);

  //  SD.sdfs.ls(&tft);
  //  save_scene_to_sd(0);
  //  dump_scene_from_sdcard(0);
  // crossfade(0, 1, 30);
  // dump_scene_from_sdcard(0);

  // dump_scene_from_sdcard(1);
  while (true) {
    backgroundFuncs();
    // backgroundFuncs_without_DMX();
    //    scenecontrol_to_dmx();

    for (int i = 0; i < 8; i++) {

      if (buttonChnPressed(i)) {
        if (lastScene != i) { // routine to reset last Rectangle back to
          lastScene = i;
          tft.setFont(FONT_10);
          sprintf(gTitle, "Scene Number: %d", i + 1);
          console.printTextAt(10, 20, gTitle, ILI9341_ORANGE, ILI9341_BLACK);
          // tft.fillRect((i * 40) + 10, 40, 30, 30 , ILI9341_ORANGE);

          drawSceneButton(lastScene, ILI9341_ORANGE, ILI9341_ORANGE);
          printSceneButton(lastScene, ILI9341_ORANGE, ILI9341_ORANGE);

          printSceneButton(i, ILI9341_BLACK, ILI9341_ORANGE);

          for (int j = 0; j < MAXCHANNEL; j++) {
            Dmx_Channels[j].faderValue = DMX_Scenes[i].dmxChannelValues[j];
            //    dump_scene_from_sdcard(i);
          }
        }
      }
      if (buttonChnReleased(i)) {
        drawSceneButton(i, ILI9341_ORANGE, ILI9341_BLACK);
        printSceneButton(i, ILI9341_ORANGE, ILI9341_BLACK);
      }
    }
    if (buttonPressed(EXIT)) {
      menuLightControl.menuListInit();
      menuLightControl.menuListSelect();
      return;
    }
  }
}

// fader
// toggle
// xfade

void scenecontrol_to_dmx() {
  //   // test function for local dmx control etc.
  //
  //   DMXsendValue[0] = 0;
  //   static unsigned long dmxUpdateTimer = millis();
  //
  //   if (millis() - dmxUpdateTimer > DMX_UPDATE_INTERVAL) {
  //     dmxUpdateTimer = millis();
  //     uint16_t r, g, b, c;
  //
  //     for (int i = 0; i < MAXCHANNEL; i++) {
  //       if (i == 0) {
  //         DMXsendValue[i] = g;
  //         Dmx.setChannel(i, DMXsendValue[i]);
  //       } else if (i == 1) {
  //         DMXsendValue[i] = r;
  //         for (int j = 0; j < 255; j++) {
  //           Dmx.setChannel(i, j);
  //           delay(10);
  //         }
  //         for (int j = 255; j > 0; j--) {
  //           Dmx.setChannel(i, j);
  //           delay(10);
  //         }
  //
  //       } else if (i == 3) {
  //         DMXsendValue[i] = b;
  //         Dmx.setChannel(i, DMXsendValue[i]);
  //       } else {
  //         DMXsendValue[i] = Dmx_Channels[i].faderValue;
  //         Dmx.setChannel(i, DMXsendValue[i]);
  //       }
  //     }
  //   }
  // #ifdef TIMO_CRMX_CHIP
  //   for (int i = 0; i < 4; i++) {
  //     write_crmx_full_universe(
  //         DMXsendValue); // send 4 times 128 channels to timo chip
  //     delayMicroseconds(1750);
  //   }
  // #endif // TIMO_CRMX_CHIP
}

void drawSceneButton(uint16_t sceneNumber, uint16_t color, uint16_t bg_color) {
  tft.fillRect((sceneNumber * 40) + 10, 40, 30, 30, bg_color);
  tft.drawRect((sceneNumber * 40) + 10, 40, 30, 30, color);
}
void printSceneButton(uint16_t sceneNumber, uint16_t color, uint16_t bg_color) {
  sprintf(gTitle, "%d", sceneNumber + 1);
  //  tft.setTextColor(color, bg_color);
  tft.setFont(FONT_8);
  console.printTextAt(sceneNumber * 40 + 20, 50, gTitle, color, bg_color);
}

// File / Ram operations
void saveSceneToRam(uint16_t sceneNum) {
  // simply safe DMX_Values to Ram sceneDMX Array
  //   function param <sceneNum> is NOT used. just for convenience
  //   handling of menu item calls...
  // Serial.printf(" \n \n Saving Scene %d \n", sceneNumber + 1);

  int sceneNumber = numberSelectPopup_forScenes();

  if (sceneNumber == -1) {

    char text[20];
    sprintf(text, "Scene save aborted.");
    infoPopup(text);
    return;
  }

  char newFile[13]; // gsc = geldscene file format
  sprintf(newFile, "scene%02d.txt", sceneNumber);
  File myFile = SD.open(newFile, FILE_WRITE);
  myFile.truncate();

  if (myFile) {
    tft.printf("%s opened.", newFile);
  } else {
    tft.printf("%s not opened!", newFile);
  }

  tft.printf("...saving DMX-Scene to sd card...\n");

  for (int i = 0; i < MAXCHANNEL; i++) {
    DMX_Scenes[sceneNumber].dmxChannelValues[i] =
        Dmx_Channels[i].faderValue; // save Values from Dmx_Channels
                                    // Struct to scene-struc
    myFile.write(Dmx_Channels[i].faderValue);
  }
  myFile.write("\0");
  tft.printf("Filesize after saving: %d", myFile.size());
  myFile.close();
  //  save_scene_to_sd(sceneNum);
  // write eof

  DMX_Scenes[sceneNumber].isEmpty = false;
  char text[20];
  sprintf(text, "Scene %d saved.", sceneNumber + 1);
  infoPopup(text);
  //  printDMXscene(sceneNumber);
}

void restoreSceneFromRam(uint16_t sceneNumber) {
  if (DMX_Scenes[sceneNumber].isEmpty)
    return;
  for (int i = 0; i < MAXCHANNEL; i++) {
    Dmx_Channels[i].faderValue =
        DMX_Scenes[sceneNumber]
            .dmxChannelValues[i]; // dump DMX Values from scene-STRUCTURES
                                  // into DmxCh..faderValue
  }
  //  printDMXscene(sceneNumber);
}

// Serialprint a Table of alle DMX Values in Scene
// relates to MAXCHANNEL makro

void printDMXscene(uint16_t sceneNumber) {
  for (int chn = 0; chn < /*512*/ MAXCHANNEL; chn++) {
    if (chn == 0)
      tft.print(" ");
    if (chn % 14 == 0) {
      tft.printf("\n %03d", DMX_Scenes[sceneNumber].dmxChannelValues[chn]);
    } else if (chn >= MAXCHANNEL - 1) {
      tft.printf(" %03d \n", DMX_Scenes[sceneNumber].dmxChannelValues[chn]);
    } else
      tft.printf(" %03d", DMX_Scenes[sceneNumber].dmxChannelValues[chn]);
  }
}

void save_scene_to_sd(uint8_t sceneNr) {
  // saving one scene to sd card
  // static unsigned long sdcard_timer = millis();
  char buffer[50];
  char newFile[13]; // gsc = geldscene file format

  sprintf(newFile, "scene%02d.txt", sceneNr);
  File myFile = SD.open(newFile, FILE_WRITE);

  if (myFile) {
    sprintf(buffer, "%s opened.", newFile);
    tft.println(buffer);
  } else {
    sprintf(buffer, "%s not opened!", newFile);
    tft.println(buffer);
  }
  myFile.truncate();

  tft.printf("...saving DMX-Scene to sd card...\n");
  for (int i = 0; i < MAXCHANNEL; i++) {
    myFile.write(DMX_Scenes[sceneNr].dmxChannelValues[i]);
    if (i < 9)
      tft.printf(" %03d ", DMX_Scenes[sceneNr].dmxChannelValues[i]);
  }
  tft.printf("\n ...");

  myFile.write("\0");
  tft.printf("Filesize after saving: %d", myFile.size());
  myFile.close();
  // tft.setCursor(10, 50);
  // tft.printf("SD operations: %lu ms \n ", (millis() - sdcard_timer));
}

void copy_scenes_from_sd_to_ram() {
  static boolean init_scenes_flag = false;
  if (!init_scenes_flag) {
    init_scenes_flag = true;
    File f;

    char myFile[13]; // gsc = geldscene file format
    char dmxBuffer[MAXCHANNEL];

    for (int sceneNr = 0; sceneNr < 8; sceneNr++) {
      sprintf(myFile, "scene%02d.txt", sceneNr);

      tft.printf("checking %s : ", myFile);

      if (f = SD.open(myFile, FILE_READ)) {
        tft.print("all good. \n");
        DMX_Scenes[sceneNr].isEmpty = false;

        while (f.available()) {
          f.read(dmxBuffer, MAXCHANNEL);
          tft.printf("buffer filled.");
        }

      } else
        tft.print("problems with the file! \n");

      for (int dmxChan = 0; dmxChan < MAXCHANNEL; dmxChan++) {
        DMX_Scenes[sceneNr].dmxChannelValues[dmxChan] = dmxBuffer[dmxChan];
      }
      f.close();
      // printDMXscene(0);
    }
  }
}

void dump_scene_from_sdcard(uint8_t sceneNr) {
  File f;
  char buffer[50];

  char myFile[13]; // gsc = geldscene file format
  sprintf(myFile, "scene%02d.txt", sceneNr);
  tft.printf("now dumping %s :\n", myFile);
  if (f = SD.open(myFile, FILE_READ)) {
    tft.print("Alles ok. \n");
  } else
    tft.println("Probleme mit der Datei! \n");

  while (f.available()) {
    static int counter = 0;
    counter++;
    if (counter > 9) {
      tft.println();
      counter = 0;
    }

    tft.printf(" %03d", f.read());
  }
  f.close();
}
