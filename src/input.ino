
//  input.ino - geld.
// basically all i/o stuff 
//
#include <Arduino.h>



//
// ENCODER
//

boolean encoderChange(int direction) {

  if (direction == DOWN) {
    if (newEncPosition < (oldEncPosition - 1)) {
      oldEncPosition = newEncPosition;
      resetScreensaveTimer();
      return (1);
    }
  } // end direction
  if (direction == UP) {
    if (newEncPosition > (oldEncPosition + 1)) {
      oldEncPosition = newEncPosition;
      resetScreensaveTimer();
      return (1);
    }
  } //  end direction
  return (0);
}

//
// SHIFTREGISTER
//

void shift_regs_init() { // just ifndef'ed function
#ifndef GELD_2019
  pinMode(ploadPin, OUTPUT);
  pinMode(clockEnablePin, OUTPUT);
  pinMode(clockPin, OUTPUT);
  pinMode(dataPin, INPUT);
  digitalWrite(clockPin, LOW);
  digitalWrite(ploadPin, HIGH);
#endif
  pinValues = read_shift_regs();
  oldPinValues = pinValues;
}

BYTES_VAL_T read_shift_regs() {
  long bitVal;
  BYTES_VAL_T bytesVal = 0;

#ifndef GELD_2019 // this is for older boards
  digitalWrite(clockEnablePin, HIGH);
  digitalWrite(ploadPin, LOW);
  delayMicroseconds(PULSE_WIDTH_USEC);
  digitalWrite(ploadPin, HIGH);
  digitalWrite(clockEnablePin, LOW);
  for (int i = 0; i < DATA_WIDTH; i++) {
    bitVal = digitalRead(dataPin);
    bytesVal |= (bitVal << ((DATA_WIDTH - 1) - i));
    digitalWrite(clockPin, HIGH);
    delayMicroseconds(PULSE_WIDTH_USEC);
    digitalWrite(clockPin, LOW);
  }
#else // this is for version GELD_2019

  for (int i = 0; i < DATA_WIDTH; i++) {
    bitVal = mcp.digitalRead(i);
    bytesVal |= (bitVal << ((DATA_WIDTH - 1) - i));
  }
#endif
  return (bytesVal);
}
//
// BUTTONS
//

// static uint16_t old_buttonvals = pinValues;
//
// if (pinValues != old_buttonvals) {
// 	old_buttonvals = pinValues;

//	if (buttonPressed(7))		return (true);

//
// for (int i = 0; i < 8; i++) {
//  if (buttonChnPressed(i)) return(true);
// //if (buttonPressed(i)) return (true);
// if (userVal[i] && !old_userVal[i] && i < 3) return(true);
// //newEncPosition = myEnc.read();
// //if (encoderChange(UP)){ // || encoderChange(DOWN)) {
// //	oldEncPosition = newEncPosition;
// //	return(true);
// //	}
// }
// 		return(false);
// 	}
// @PAULE: wenn ich den encoder in der anyInput funktion auswerte, geht er zwar
// um aus dem screensaver rauszugehen, aber nicht mehr in den menufunktionen

bool buttonChnPressed(int button) {
  if (buttVal[button] && !old_buttVal[button]) {
    return (1);
    resetScreensaveTimer();
  }
  return (0);
}

bool buttonChnReleased(uint8_t button) {
  if (!buttVal[button] && old_buttVal[button]) {
    old_buttVal[button] = buttVal[button];
    return (1);
  }
  old_buttVal[button] = buttVal[button];
  return (0);
}

// void flashButton() {
// 	for (int i = 0; i < NUM_FADER ; i++) {
// 		if (buttonChnPressed(i)) {
// 		//	hallo();
// 			}
//
// 		if (buttonChnReleased(i) ) {
// 		//	schuessi();
// 			}
// 		}
//}

boolean buttonPressed(int button) {
  if (userVal[button] && !old_userVal[button]) {
    old_userVal[button] = userVal[button];
    resetScreensaveTimer();
    return (1);
  }
  old_userVal[button] = userVal[button];
  return (0);
} // end of buttonPressed

bool buttonReleased(uint8_t button) {
  if (!userVal[button] && old_userVal[button]) {
    old_userVal[button] = userVal[button];
    return (1);
  }
  old_userVal[button] = userVal[button];
  return (0);
}

void buttonQuery() {
  static unsigned long shiftRegTimer = millis();
  if (millis() - shiftRegTimer > SHIFT_REG_INTERVAL) {
    shiftRegTimer = millis();
    pinValues = read_shift_regs();
    if (hasChanged(pinValues)) {
      // hallo();
      for (int i = 0; i < 8; i++)
        buttVal[buttonBit[i]] = ~(pinValues >> i) & 1;
      for (int i = 8; i < DATA_WIDTH; i++)
        userVal[i - 8] = ~(pinValues >> buttonBit[i]) & 1;
    }
  }
}

//
// adc
//

void adc_init() {
  pinMode(CS_MCP3008, OUTPUT);
  digitalWrite(CS_MCP3008, LOW); // Cycle the ADC CS pin as per datasheet
  digitalWrite(CS_MCP3008, HIGH);
}

void adc_print_values() { // diagnostic function: printing out raw values
  mcp3008_channels_read();
  for (int i = 0; i < 8; i++) {
    Serial.printf(" Ch%d: %04d, ", i, adc_poti_reading[i]);
    if (i == 7)
      Serial.println();
  }
}
void mcp3008_channels_read() {
  int i, j;
#ifndef GELD_2019 // thats old potimapping (15-8)
  for (i = 0, j = 15; i < 8; i++, j--) {
#else // thats new potimapping since GELD_2019 (8-15)
  for (i = 0, j = 8; i < 8; i++, j++) {
#endif
    adc_poti_reading[i] = adc_single_channel_read(j);
  }
}

int adc_single_channel_read(byte readAddress) {

  byte dataMSB = 0;
  byte dataLSB = 0;
  byte JUNK = 0x00;

  SPI.beginTransaction(MCP3008);
  digitalWrite(CS_MCP3008, LOW);
  // Start Bit
  SPI.transfer(0x01);
  // Send readAddress and receive MSB data, masked to two bits
  dataMSB = SPI.transfer(readAddress << 4) & 0x03;
  // Push junk data and get LSB byte return
  dataLSB = SPI.transfer(JUNK);
  digitalWrite(CS_MCP3008, HIGH);
  SPI.endTransaction();

  return dataMSB << 8 | dataLSB;
}

//
// POTENTIOMETER
//

// potiprocessingshit() - glaettet die 8 potikanaele,
// rechnet die globalen channels (zb aus main_dmx_val) relativ zum startChannel
// auf die 8 displayChannels, resettet firstCall fuer die poti_catch funktion
// und ruft die display-funktionen nur im falle einer eraenderugng der potiWerte
// auf.

void poti_processing_shit(
    uint16_t *mainArray,
    int16_t startAdress) { // old collection of functions, not in use anymore
                           // but useful reference to when it worked..

  for (int i = 0; i < NUM_FADER; i++) { // Iteration der 8 Poti Kanaele
    potiVal[i] = poti_smooth(potiVal[i], i);
    if (hasChanged(potiVal[i])) {
      display_dmx_channel[i] =
          startAdress + i; // definiert das fenster von dmx-kanaelen, welches im
                           // aktuallen zusatnd veraendert wird
      potiRead[i] = poti_catch_val(
          poti_map_8bit(potiVal[i]), mainArray[display_dmx_channel[i]],
          poti_catch_firstCall[i], i); // wenn helligkeit
      poti_catch_firstCall[i] = false; // noch zu klaeren mit catchval
      if (potiRead[i] <= 0)
        potiRead[i] = 0;
      if (hasChanged(potiRead[i], i)) {
        mainArray[display_dmx_channel[i]] = potiRead[i];
      }
    }
  }
}

uint8_t antilogTable[256] = {
    //			0,  13,  18,  23,  26,  29,  32,  35,  37,  40,  42,  44,  46,
    //48,
    // 50,  52,

    0,   1,   3,   3,   6,   6,   6,   9,   9,   12,  16,  20,  24,  28,  35,
    39,  42,  45,  48,  52,  55,  58,  61,  64,  66,  67,  69,  70,  71,  73,
    74,  75,  76,  78,  79,  80,  81,  82,  84,  85,  86,  87,  88,  89,  90,
    91,  93,  94,  95,  96,  97,  98,  99,  100, 101, 102, 103, 104, 105, 106,
    107, 108, 109, 110, 111, 112, 113, 113, 114, 115, 116, 117, 118, 119, 120,
    121, 122, 123, 123, 124, 125, 126, 127, 128, 129, 129, 130, 131, 132, 133,
    134, 135, 135, 136, 137, 138, 139, 140, 140, 141, 142, 143, 144, 144, 145,
    146, 147, 148, 148, 149, 150, 151, 152, 152, 153, 154, 155, 156, 156, 157,
    158, 159, 159, 160, 161, 162, 162, 163, 164, 165, 166, 166, 167, 168, 169,
    169, 170, 171, 172, 172, 173, 174, 175, 175, 176, 177, 177, 178, 179, 180,
    180, 181, 182, 183, 183, 184, 185, 186, 186, 187, 188, 188, 189, 190, 191,
    191, 192, 193, 194, 194, 195, 196, 196, 197, 198, 199, 199, 200, 201, 201,
    202, 203, 204, 204, 205, 206, 206, 207, 208, 208, 209, 210, 211, 211, 212,
    213, 213, 214, 215, 216, 216, 217, 218, 218, 219, 220, 220, 221, 222, 223,
    223, 224, 225, 225, 226, 227, 227, 228, 229, 230, 230, 231, 232, 232, 233,
    234, 234, 235, 236, 236, 237, 238, 239, 239, 240, 241, 241, 242, 243, 243,
    244, 245, 245, 246, 247, 248, 248, 249, 250, 250, 251, 252, 252, 253, 254,
    255};

uint16_t poti_mapping(uint16_t potiVal, uint8_t *mapArray) {
  potiVal = mapArray[potiVal];
  return potiVal;
}

uint16_t poti_smooth(uint16_t potiVal, uint8_t channel) {

  potiVal = (0.4 * (potiVal) +
             0.6 * (adc_poti_reading[channel])); // smoothing poti-channel
  return potiVal;
}

uint16_t poti_map_8bit(uint16_t potiVal) {
  potiVal = map(potiVal, 0, 1023, 0, 255);
#ifdef LOG_FADERS
  potiVal = antilogTable[potiVal];
#endif
  return potiVal;
}

void console_poti_map(int channel,
                      char mapMode) { // glaettet die Potiwerte und mappt diese
                                      // auf die Hardware Potiports

  potiVal[channel] =
      (0.2 * potiVal[channel]) + (0.8 * (adc_poti_reading[channel]));
  if (mapMode == MAIN) {
    if (channel < 8)
      potiGlatt[channel] = map(potiVal[channel], 3, 1023, 0, 255);
  }
}

void potiCatch_init() {
  for (int i = 0; i < 8; i++) {
    poti_catch_firstCall[i] = true;
    poti_catch_gotIt[i] = false;
  }
}

int poti_catch_val(int pot_read, int stored_pot_val, boolean first_call,
                   int i) {
  if (poti_catch_firstCall[i]) { // das wird ausgefuehrt bevor gecatcht wurde
    last_pot_read[i] = stored_pot_val;
    //	led_highlight_fader[ledPin[i]];

    poti_catch_gotIt[i] = false;
  }
  if (!poti_catch_gotIt[i]) { // dies wird ausgefuehrt wenn gecatcht wurde
    if (((last_pot_read[i] > stored_pot_val) && (pot_read <= stored_pot_val)) ||
        ((last_pot_read[i] < stored_pot_val) && (pot_read >= stored_pot_val))) {
      led_highlight_fader(i);
      last_pot_read[i] = pot_read;
      poti_catch_gotIt[i] = true;
      poti_catch_firstCall[i] = false;
      return pot_read;
    } else { // dies wird wiederholt ausgefuehrt BEVOR gecatcht wurde
      last_pot_read[i] = pot_read;
      return stored_pot_val;
    }
  } else { // dies wird wiederholt ausgefuehrt NACHDEM gecatcht wurde
    last_pot_read[i] = pot_read;
    return pot_read;
  }
}

void reset_potiVals(int channelStartOffset) {
  // some function to re-initialize the poti-vals, after changing displayed page
  for (int i = 0; i < NUM_FADER; i++) {
    poti_catch_firstCall[i] = true;
  }
}

//
// BATTERIE
//

void bottomBar_monitor() {
  static unsigned long debugMonitorTimer = millis();
  if (millis() - debugMonitorTimer > 1000) {
    debugMonitorTimer = millis();
    tft.setFont(FONT_8);

//@ANDREAS: bitte mal #define DEBUGGER in deine chipDefines.h reintun..
#ifdef DEBUGGER
    print_freeRam();
#endif //DEBUGGER
  }
#ifdef DEBUGGER
  print_frames_per_(1000);
#endif //DEBUGGER
}

// TOUCHSCREEN - currently not implemented but CS has to be initialized!!

void touch_init() {
  pinMode(CS_TOUCH, OUTPUT);
  digitalWrite(CS_TOUCH, HIGH);
}
