#include <Arduino.h>
//MAIN MENU+
// basically all menus working with tftmenu lib

void mainMenu() {
  menuMain.menuListInit();
  menuMain.menuListSelect();

  while(true){
  backgroundFuncs();
  if (encoderChange(UP)) {
    menuMain.menuListSelect(UP);

  }
  if (encoderChange(DOWN)) {
    menuMain.menuListSelect(DOWN);
  }
  if (buttonPressed(SELECT)) {
      menuMain_func[menuMain.menu_select]();
      return;
  }
  if (buttonPressed(EXIT)) {
    #ifdef DEBUGGER
      reboot_device();
    #endif
    }
  }
} // end of mainMenu()

void lightControlMenu(void) {
  menuLightControl.menuListInit();
  menuLightControl.menuListSelect();
  while(true) {
    backgroundFuncs();
    if (encoderChange(UP)) {
      menuLightControl.menuListSelect(UP);
      }
    if (encoderChange(DOWN)) {
      menuLightControl.menuListSelect(DOWN);
      }
    if (buttonPressed(SELECT)) {
      func_lightControl[menuLightControl.menu_select]();

      }
    if (buttonPressed(EXIT)) {
    //  mainMenu();
    //  hallo();

        // habe hier mal den erneuten Funktionsaufruf der übergeordneten (menu-) funktion
        // rausgeworfen um die funktion nun sauber zu beenden.
      return;
      }
    }   // end of consoleState
  }


void eepromMenu() {
  menuEeprom.menuListInit();
  menuEeprom.menuListSelect();
  while(true) {
    backgroundFuncs();
    ttransfer.transfer();  // while filemnu is opened , teensytransfer is running!

    if (encoderChange(UP)) {
      menuEeprom.menuListSelect(UP);
      }
    if (encoderChange(DOWN)) {
      menuEeprom.menuListSelect(DOWN);
      }
    if (buttonPressed(SELECT)) {
      func_eeprom[menuEeprom.menu_select]();
      }
    if (buttonPressed(EXIT)) {
    return;
    }
  }   // end of consoleState
} //end of crmxMenu



void special_startup_sequence() {
  tft.setFont(FONT_10);
  tft.println("GOOD ENOUGH LIGHTING DEVICE");
  tft.println("");
  tft.println("pre 1.0 beta");
  tft.println();


}
