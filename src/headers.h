// aus: http://forum.arduino.cc/index.php?topic=165658.0
#ifndef HEADER_GLOBAL
#define HEADER_GLOBAL
// #include <unordered_map> // for associative array stuff. does not work yet -
// "arduino vs. unordered_map" problem
#include "headerTypeDefinitions.h"

#include <EEPROM.h>
#include <Encoder.h>
#include <MCP3008.h>
#include <SPI.h>
#include <TeensyDmx.h>
#include <TftMenuLib.h> // eigene library fuer mein ultra-mikromenu
#include <font_LiberationMono.h>
//#include <geldEfxLib.h> // eigene library fuer die Effektberechnung
//#include "efx.h"

#include "chipDefines.h"
#include <SD.h>
#include <consoleLib.h> // eigene library fuer die ganzen steueroberflaechen
#include <i2c_t3.h>

#ifdef GELD_2019
#include "pca9956_t3.h"
#include "pca9956_t3.h"
#else
#include "pca9634.h"
#endif

#include "Adafruit_MCP23017.h"
#include "MAX17043.h"
#include <TeensyTransfer.h>
MAX17043 batteryMonitor;

// this are the timeout-values for screensaver and autosave functions
boolean gRedraw = true;
uint32_t gScreensaverDelay =
    150000; // screensaver after n seconds of no interaction
uint32_t gAutosaveDelay = 15000; // autosave after n seconds without interaction
boolean gScreenSaverEnabled = true;
boolean gAutosaveEnabled = true;

// deklarierte funktionnen.

// void menu_main(void);
void TIMO_menu_status(void);
void TIMO_menu_config(void);
void TIMO_link(void);
void TIMO_unlink(void);
void TIMO_txPower();
void TIMO_wireless_enable(void);
void TIMO_interslotAdjust(void);
void TIMO_refreshPeriodAdjust(void);
void TIMO_wireless_enable(void);
void TIMO_blocked_channels_adjust(void);
void consoleDisplay(void);
void EEPROMsave(void);
void EEPROMclear(void);
void EEPROMrestore(void);
void fixtureControl(void);
void fixtureSetup(void);
void sceneControl_console(void);
void chaserMenu(void);
void draw_menu_efx(void );
void menuExperimental(void);
void  batterylog_monitor_menu(void);
void blockbuster(void);
/*void subMenu_FXset(void);
void subMenu_mute(void);
void subMenu_solo(void);
void subMenu_reset(void);*/
void colorSelectMenu(void);

void reboot_device(void);
void patchBay(void);
void studioBay_main(void);
void lightControlMenu(void);
void crmxMenu(void);
void eepromMenu(void);
void rgbSensor(void);
void special_startup_sequence(void);
void assignedFixtureConsole(void);
void deviceControl(void);
void deviceSetup(void);
void saveSceneToRam(uint16_t);
void factoryReset(void);
void talkto_pca9956();
void blockbuster();

#define COLOR_TXT_MENU_HEADER GREEN
#define COLOR_BG_MENU_HEADER BLACK
#define COLOR_TXT_MENU_ITEM WHITE
#define COLOR_BG_MENU_ITEM BLACK

#define COLOR_TXT_PATCHBAY WHITE
#define COLOR_BG_PATCHBAY BLACK
#define COLOR_FADER_PATCHBAY CYAN
#define COLOR_FADER_BG_PATCHBAY GELD_GREY1

#define COLOR_TXT_DMXCONSOLE WHITE
#define COLOR_BG_DMXCONSOLE BLACK
#define COLOR_FADER_DMXCONSOLE GREEN
#define COLOR_FADER_BG_DMXCONSOLE GELD_GREY1

#define CPU_RESTART_ADDR (uint32_t *)0xE000ED0C
#define CPU_RESTART_VAL 0x5FA0004
#define CPU_RESTART (*CPU_RESTART_ADDR = CPU_RESTART_VAL);

// PORT DEFINE
/*
//das sind die ports vom geld 1_0

#define ENC_A  32
#define ENC_B  33
#define BATTERY  A14
#define SD_CS   25
#define timo_csPin  19
#define timo_irqPin  18
#define TiMo_irq  19
#define cs_TiMo  18
*/

// dies sind die ports fuer heartattack trigger

//#define ENC_A  14 MAX
//#define ENC_B  15
#define BATTERY 23
#define SD_CS 20
#define timo_csPin 22
#define timo_irqPin 21
#define TiMo_irq 21
#define cs_TiMo 22
#define TFT_BACKLIGHT 6

// #ifdef GELD_2019
// #define CS_TOUCH 8
// #elif GELD_2020
#ifdef GELD_2020
#define CS_TOUCH 3
#define CS_FLASH 16
#endif

#define MAXEFX_PARAM 160
#define MAX_EFX 6
#define MAXPATCH 8
#define MAXPATCH_CHANNELS 32

#define MAXFIXTURECHANNELS 32
#define MAXDMXCHANNELS 512
// const int potiPin[] = { 28, 27, 26, A12, 97, 16, 98, 99};
const int ledPin[8] = {4, 5, 6, 7, 3, 2, 1, 0};
const int buttonBit[] = {0, 1, 2, 3, 4, 5, 6, 7, 10, 9, 8, 11, 12, 13, 14, 15};

#define DMX_REDE 2
TeensyDmx Dmx(Serial1, DMX_REDE);
Adafruit_MCP23017 mcp;
#ifdef LED_CHIP
//		pca9634 leds( 0x00 );
Pca9956_t3 leds;
#endif

// MENU NAMES

#define MAIN 0
#define SUB_CHN 1
#define SUB_EFX 2
#define SUB_TIMO 3

byte menuLayer = MAIN;
// INITS

// SHIFT REGISTER INIT
#define DATA_WIDTH                                                             \
  16 // Width of pulse to trigger the shift register to read and latch.
#define PULSE_WIDTH_USEC 0 //  5 Optional delay between shift register reads.
#define POLL_DELAY_MSEC                                                        \
  0 // change the "int" to "long" If the * NUMBER_OF_SHIFT_CHIPS is higher
    // than 2.
#define BYTES_VAL_T unsigned int
#define ploadPin 16       // Connects to Parallel load pin the 165
#define clockEnablePin 99 // Connects to Clock Enable pin the 165
#define dataPin 3         // Connects to the Q7 pin the 165
#define clockPin 5        // Connects to the Clock pin the 165
BYTES_VAL_T pinValues;
BYTES_VAL_T oldPinValues;

long oldEncPosition = -999;
long newEncPosition;

bool display_draw = 1;
bool old_display_draw;
byte old_selectChn = 0;

long blinkTime;

int subMenuInitCursorX; // Variable um ein blinkendes Quadrat zu malen
int subMenuInitCursorY;
bool consoleState;
bool subMenu_timoState;
bool console_subMenu_state;
bool console_subSub_state;

int selectChn = 0;

// define pin connections
#define ADC_CS 4
#define SCK 13
#define MOSI 11
#define MISO 12

// I2C STUFF
#define TARGET_START 0x01
#define TARGET_END 0x7F
#define WIRE_PINS I2C_PINS_18_19
#define RGB_LED 29
#define RGB_INT 28

#define ADC_READ_INTERVAL 5
#define SHIFT_REG_INTERVAL 25
#define POTI_MIN 0
#define POTI_MAX 1023
// Function prototypes
void scan_i2c_bus(i2c_t3 &Wire, uint8_t all);
void print_bus_i2c_status(i2c_t3 &Wire);
void print_i2c_scan_status(struct i2cStruct *i2c, uint8_t target,
                           uint8_t &found, uint8_t all);
void init_pca9956();

ILI9341_t3 tft = ILI9341_t3(TFT_CS, TFT_DC);
Console console = Console(&tft);
Encoder myEnc(ENC_A, ENC_B);
// DISPLAY INIT

int buttVal[8], old_userVal[8];
int userVal[8], old_buttVal[8];

bool chnSet;

int potMax = 255;

byte EFXvalue[MAXEFX_PARAM];
int dsplEFX[8];

uint8_t DMXsendValue[MAXCHANNEL + 1]; // hier kommen die korrigierten DMX Values
                                      // (nach EFX Engine) rein

uint16_t display_dmx_channel[8];

// START Andreas versucht was neues
/*
        DMX-Channels: 512 [0..511 = 2^9]
        PatchBays: 8 [0..7 = 2^3]
        Devices: 32 [0..31]  ("mehr als 32 Geraete gleichzeitig braucht man
   nicht")

        DeviceModel: (a.k.a. device-database)
                char name;
                char className;
                char manufacturer;
                uint8_t numChannels;
                Device_Channel_t channels [0..32] of	char type;

        Device[0..31]:
                name: [char*]
                DeviceModel * model [Pointer]
                uint16_t startChannel [0..511 = 2^9]
                SUM: 32 * (2+2) = 128 Byte

        DmxChannel[0..511]:
                uint8_t value [0..255 = 2^8]
                bool used [0|1 = 2^1]
                Device_t * device [0..31 = 2^5]
                uint8_t deviceChannel [0..31 = 2^5]
                SUM: 512 * (1+1+2+1) = 2.5 kB

        PatchBay[0..7]:
                uint16_t patchedChannels [NUM_DMX_CHANNELS] [0..512]
                uint16_t memUsage [0|1..512 = 2^10]
                uint8_t faderVal [0..255 = 2^8]
                float faderPercentage [2^32 bzw. 4 Byte]
                SUM: 8 * ((512 * 2)+2+1+4) =  8 kB
                but maxMemUsage = 512 -> netto ~ 1 kB

*/

// must be declared before include of headerTypeDefinitions?
const uint16_t NUM_DMX_CHANNELS =
    512; // DMX-Spec: "There are 512 DMX channels. No more, no less."
const uint8_t NUM_PATCHBAYS =
    16; // How many patch bays do we want? (Schould be multiple of NUM_FADER!)
const uint8_t NUM_FADER =
    8; // This is defined by the hardware layout: How many faders are there?
const uint8_t NUM_SCENES = 8; // How many scenes can be stored simultaneously
const uint8_t MAX_CHANNELS_PER_DEVICE =
    32; // (Paule: "more channels per device are unlikely" -> spec says more
        // then 32 is unreliable)
const uint8_t MAX_CONNECTED_DEVICES =
    32; // (Paule: "you don't need more than 32 connected devices at any time")

// the following used structures are typedefined in headerTypeDefinitions.h
Dmx_Channel_t Dmx_Channels[NUM_DMX_CHANNELS];
Patch_Bay_t patchBays[NUM_PATCHBAYS];
extern Device_Model_t devicesAvailable[]; // wird in device db deklariert (zzt
                                          // altes_studiobay.ino)
Connected_Device_t connectedDevices[MAX_CONNECTED_DEVICES];
SliderBar_t gDisplaySliderBars[NUM_FADER]; // buffer for displaying fader bars
// uint8_t displaySliderBars_oldVals[8]; // buffer for detecting changes
DMX_Scene_t DMX_Scenes[NUM_SCENES];

char gTitle[50];        // buffer for page title sprintf()
bool gDoRefresh = true; // indicator if gDisplaySliderBars[] has to be refreshed

char alphaNum[] =
    "ABCDEFGHIJKLMNOPQRSTUVWXYZ_0123456789"; // for alpha-num text entering grid
// std::unordered_map<std::string, int> autoNames; // does not work yet -
// "arduino vs. unordered_map" problem
ILI9341_t3_font_t fonts[] = {
    FONT_8, //  0
    FONT_8, //  1
    FONT_8, //  2
    FONT_8, //  3
    FONT_8, //  4
    FONT_8, //  5
    FONT_8, //  6
    FONT_8, //  7
    FONT_8, //  8
    FONT_9, //  9
    FONT_10 // 10
};

char gThisView = ' '; // global identifyer for presently active view. S:
                      // singleChannel, P: patchBay, D: deviceControl
int8_t gSelectedFader =
    -1; // generally - if we want to use the faderID, like for specialized
        // submenu. Remember to reset to -1 on ENTERING submenu function!

// Positional variables, used in patchBay.
//   - target-channel, current channel-grid-page + position on current
//   channel-grid-page
//   - Define globally, so it can be reset from outside patchBay (like device
//   adding/removal)
int gSelected_DmxChannel = 0;   // ID in Dmx_Channels[]
int gCurrentPage = 0;           //  tracks ID of page in gChannelGrid[]
int gPositionOnCurrentPage = 0; // tracks ID of gSelected_DmxChannel in
                                // gChannelGrid[gCurrentPage].gridPageItems[]

// END Andreas versucht was neues

bool gScreenSaverState = 0;

boolean crmx_init = 1;

byte old_display_dmx_channelvalue[8];
byte old_displayEFXvalue[8];
byte old_slotMaxCorrector[MAXCHANNEL + 1];
byte old_display_patch_channel[8];
byte old_display_patch_channel_maxCorrector[8];

int startEFXaddr = 0;
int oldStartEFXaddr = 0;

int slotMaxCorrector[MAXCHANNEL + 1];
int display_patch_channel_maxCorrector[MAXPATCH + 1];

boolean buttRead = 0;
int potiGlatt[8];
boolean poti_catch_gotIt[8];
int last_pot_read[8];
boolean poti_catch_firstCall[8];
int savedStartAddr = 1;

int display_DMX_startadress = 1;
int old_display_DMX_startAdress = 0;
int startAddr = 1;

// byte patchFader = 0; only used in patchBay.ino - so make it local there...
boolean usedPatchChannels[MAXCHANNEL + 1];

int potiRead[8];
int old_potiRead[8];
int potiVal[8];

boolean consoleInit;

const int potSmoothReadings = 8;
int PotReadArray[8][potSmoothReadings], readIndex, total[8];

int battStatus = 0;
unsigned long gNoInteractionSince;

boolean buttonPressed(int button);
boolean buttonChnPressed(int button);
//
 Sd2Card card;
// SdVolume volume;
// SdFile root;

//
// DEBOUNCE
//

struct button_state {
  int sampleB;
  int sampleC;
  int LastDebounceResult;
};

// struct button_state button[] = {0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
//                                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
//                                 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
// //
// enum DISPLAY_FORMAT {
//   WELCOME,
//   DEVICENAME,
//   FX_NAME,
//   FX_WINDOWNAME,
//   CHANNELNAME,
//   CHANNELVALUE_STD,
//   CHANNELVALUE_MIN,
//   CHANNELVALUE_EFX,
//   MENU_1
// };

//
// MCP3008 ADC
//

SPISettings MCP3008(2000000, MSBFIRST, SPI_MODE0);

const int CS_MCP3008 = 4;
// ADC Chip Select
const byte adc_single_ch0 = (0x08); // ADC Channel 0
const byte adc_single_ch1 = (0x09); // ADC Channel 1
const byte adc_single_ch2 = (0x0A); // ADC Channel 2
const byte adc_single_ch3 = (0x0B); // ADC Channel 3
const byte adc_single_ch4 = (0x0C); // ADC Channel 4
const byte adc_single_ch5 = (0x0D); // ADC Channel 5
const byte adc_single_ch6 = (0x0E); // ADC Channel 6
const byte adc_single_ch7 = (0x0F); // ADC Channel 7

int adc_poti_reading[8];

//
// OBSOLETE STRUCTURES
//

typedef struct rect_t {
  int x;
  int y;
  int w;
  int h;
  int r = 0;

  int rect_color;
  int txt_bgColor;
  int txt_color;
} rect_t;

typedef struct coord_t {
  int x;
  int y;
} coord_t;

//
// LUMENRADIO TIMO FX
//

// CONFIG REGISTER ADDRESS
#define CONFIG_REG 0x00
#define STATUS_REG 0x01
#define IRQ_MASK_REG 0x02
#define IRQ_FLAGS_REG 0x03
#define DMX_WINDOW_REG 0x04
#define ASC_FRAME_REG 0x05
#define SIGNAL_QUALITY_REG 0x06
#define ANTENNA_REG 0x07
#define DMX_SPEC_REG 0x08
#define DMX_CONTROL_REG 0x09
#define VERSION_REG 0x10
#define RF_POWER_REG 0x11
// END CONFIG REGISTER ADDRESS

#define NO_OPERATION 0xFF
#define SPI_DEVICE_BUSY 0x80

// IRQ_MASK REGISTER
#define RX_DMX_IRQ_EN 0x01      // Complete DMX frame received
#define LOST_DMX_IRQ_EN 0x02    // loss of DMX
#define DMX_CHANGED_IRQ_EN 0x04 // DMX Changed in DMX Window
#define RF_LINK_IRQ_EN 0x08     // Radio Link Status Changed
#define ASC_IRQ_EN 0x10         // Alternate Start Code Frame Received
#define IDENTIFY_IRQ_EN 0x20    // Identify Device state changed
// END IRQ_MASK_REGISTER

// IRQ_FLAGS REGISTER
#define RX_DMX_IRQ 0x01      // Complete DMX frame received
#define LOST_DMX_IRQ 0x02    // Loss of DMX
#define DMX_CHANGED_IRQ 0x04 // DMX changed in DMX window
#define RF_LINK_IRQ 0x08     // Radio link status change
#define ASC_IRQ 0x10         // Alternative start code frame received
#define IDENTIFY_IRQ 0x20    // Identify device state changed
#define RESERVED_IRQ 0x40    // Reserved for future use
#define SPI_DEVICE_BUSY 0x80 // SPI Device busy, command MUST be restarted
// END IRQ_FLAGS REGISTER

//#define timo_csPin  19
//#define timo_irqPin 18
#define LinkSwitchPin 3

#define READ_DMX 0x81
#define READ_ASC 0x82
#define WRITE_DMX 0x91

#define READ_REG(x) (x)
#define WRITE_REG(x) (0x40 | x)

volatile int IRQIsOn = 0;
volatile int IRQIsOff = 0;
bool crmxState = 0;
bool crmxEnabled = 1;

unsigned int DMXStartAddr = 0;
unsigned int DMXWindow = 512;

uint8_t main_dmx_vals[512];
unsigned char nullBuffer[512];

boolean debug = false;

#endif
