#include <Arduino.h>
// eeprom stuff
// saving single universe and device settings to teensy - eeprom (2kb)


/*
	MEMORY LAYOUT:
	--------------
		available: 2 kByte EEPROM (Teensy 3.2)
							(0000..2047): 2048 Byte

		(0000..0511) Dmx_Channels[].faderValues
							512 x uint8_t = 512 x 1 Byte = 512 Byte
		(0512..1023) PatchBay[].patchedChannels[]
							range [-1|0..15]
							512 x [-1|0..15] = 512 x 1 Byte = 512 Byte
		(1024..1039) PatchBay[].faderValues
							NUM_PATCHBAYS x uint8_t = NUM_PATCHBAYS x 1 Byte = 16 Byte (zzt, 16 patchbays)
		(1040..1215) PatchBay[].name
							NUM_PATCHBAYS x char[11] = NUM_PATCHBAYS x 11 Byte = 176 Byte (zzt, 16 patchbays)
		(1215..1694) connectedDevices[]
							.name char[11] -> 11 Byte
							.startChannel uint16_t -> 2 Byte
							.deviceModelID uint16_t -> 2 Byte
							(max)32 x (11+2+2) = 32 x 15 Byte = 480 Byte
		(2044)	MenuState - Welches (Console-Menu) wurde zuletzt benutzt, Switch to fall into last menu after startup
		(2045+2046) CRC
							2 byte int, number of "1"-bits from address 0 to 2044
		(2047) FLAG_purposefullyData
							0|1  = false|true "Is there purposefully written data in the EEPROM?"
		available after storage: 352 Byte (zzt, 16 patchbays)
*/

bool debugOutput_eepromIno = false; // print info about data-size and -consistency in serial console
bool dataCommandsOutput_eepromIno = false; // prints all commands to restore data structures to serial console

// not yet used, but may proof usefull:
int16_t startPatchedChannels = 512; // see above memory map
int16_t startPatchBay_faderValues = 1024; // see above memory map
int16_t startPatchBay_name = 1040; // see above memory map
int16_t startConnectedDevices = 1215; // see above memory map
int16_t crcPosition = 2045; // see above memory map
int16_t purposefullyFlagPosition = 2047; // see above memory map



void EEPROMsave_for_autoSave() {   //funktion auf die schnelle kopiert um menu.comback zu umgehen (pfusch!)
//	int8_t first=0, second=0; // just for "half-byte-handling", presently unused
//	byte returnBuffer = 0;
	int16_t addressPointer = 0, oldAddressPointer = 0;

	// if (!confirmPopup("Are You Sure?")) {
	// 		//menuEeprom.comeBack();
	// } else {
	// menuEeprom.alertWindow("   !!!! saving setup !!!!!", 20, 40, 260, 60, 12,20);

		if(debugOutput_eepromIno) Serial.printf("\nStoring\n-----------------------\n");
		int storedBits = 0;
		// Dmx_Channels[].faderValues
		oldAddressPointer = addressPointer;
		if(debugOutput_eepromIno) Serial.printf("\nDmx_Channels[].faderValues %d x %d Byte from %04d", sizeof(Dmx_Channels)/sizeof(Dmx_Channels[0]), sizeof(Dmx_Channels[0].faderValue), addressPointer);
		for(int i = 0; i < NUM_DMX_CHANNELS; i++){
			EEPROM.update(addressPointer, Dmx_Channels[i].faderValue);
			addressPointer++;
			storedBits = storedBits + getToggledBitsFromByte(Dmx_Channels[i].faderValue, false);
		}
		if(debugOutput_eepromIno) Serial.printf(" to %04d (%d Byte)\n", addressPointer-1, addressPointer - oldAddressPointer);

		// PatchBay[].patchedChannels[]
		oldAddressPointer = addressPointer;
		if(debugOutput_eepromIno) Serial.printf("\nPatchBays[].patchedChannels[] (%d/2) x (4Bit+4Bit) from %04d", sizeof(Dmx_Channels)/sizeof(Dmx_Channels[0]), addressPointer);
		for(int i = 0; i < NUM_DMX_CHANNELS; i++){
			EEPROM.update(addressPointer, Dmx_Channels[i].patchBayID);
			addressPointer++;
			storedBits = storedBits + getToggledBitsFromByte(Dmx_Channels[i].patchBayID, false);
		}
		if(debugOutput_eepromIno) Serial.printf(" to %04d (%d Byte)\n", addressPointer-1, addressPointer - oldAddressPointer);

		// PatchBay[].faderValues
		oldAddressPointer = addressPointer;
		if(debugOutput_eepromIno) Serial.printf("\npatchBays[].faderValues %d x %d Byte from %04d", sizeof(patchBays)/sizeof(patchBays[0]), sizeof(patchBays[0].faderValue), addressPointer);
		for(int i = 0; i < NUM_PATCHBAYS; i++){
			EEPROM.update(addressPointer, patchBays[i].faderValue);
			addressPointer++;
			storedBits = storedBits + getToggledBitsFromByte(patchBays[i].faderValue, false);
		}
		if(debugOutput_eepromIno) Serial.printf(" to %04d (%d Byte)\n", addressPointer-1, addressPointer - oldAddressPointer);

		// PatchBay[].name
		oldAddressPointer = addressPointer;
		if(debugOutput_eepromIno) Serial.printf("\npatchBays[].name %d x 11 Byte from %04d", sizeof(patchBays)/sizeof(patchBays[0]), addressPointer);
		for(int i = 0; i < NUM_PATCHBAYS; i++){
			for(int j = 0; j < 11; j++){
				// name char[11]
				EEPROM.update(addressPointer, patchBays[i].name[j]);
				addressPointer++;
				storedBits = storedBits + getToggledBitsFromByte(patchBays[i].name[j], false);
			}
		}
		if(debugOutput_eepromIno) Serial.printf(" to %04d (%d Byte)\n", addressPointer-1, addressPointer - oldAddressPointer);

		// connectedDevices[]
		oldAddressPointer = addressPointer;
		if(debugOutput_eepromIno) Serial.printf("\nconnectedDevices[] %d x 15 Byte from %04d", sizeof(connectedDevices)/sizeof(connectedDevices[0]), addressPointer);
		for(int i = 0; i < MAX_CONNECTED_DEVICES; i++){
			for(int j = 0; j < 11; j++){
				// name char[11]
				EEPROM.update(addressPointer, connectedDevices[i].name[j]);
				addressPointer++;
				storedBits = storedBits + getToggledBitsFromByte(connectedDevices[i].name[j], false);
			}
			eeprom_putInt(addressPointer, connectedDevices[i].startChannel);
			addressPointer += 2;
			storedBits = storedBits + getToggledBitsFromByte(connectedDevices[i].startChannel, true);

			eeprom_putInt(addressPointer, connectedDevices[i].deviceModelID);
			addressPointer += 2;
			storedBits = storedBits + getToggledBitsFromByte(connectedDevices[i].deviceModelID, true);
		}
		if(debugOutput_eepromIno) Serial.printf(" to %04d (%d Byte)\n", addressPointer-1, addressPointer - oldAddressPointer);

		if(debugOutput_eepromIno) Serial.printf("CRC: %d bits set to 1\n", storedBits);
		writeCRC(storedBits);
		togglePurposefullyFlag();


		if(debugOutput_eepromIno) Serial.printf("\n%d Bytes stored. EEPROM usage: %d%% (free: %d Byte)", addressPointer,  addressPointer*100/2048, crcPosition - addressPointer);

		if(debugOutput_eepromIno) {
			Serial.printf("Verification data:\n---------------------\n");
			print_Dmx_Channels_element(0);
			print_Dmx_Channels_element(1);
			print_patchBays_element(0);
			print_patchBays_element(1);
			print_connectedDevices_element(0);
			print_connectedDevices_element(1);
			print_usedPatchChannels();
		}

	}
void EEPROMsave() {
	//int8_t first=0, second=0; // just for "half-byte-handling", presently unused
	//byte returnBuffer = 0;
	int16_t addressPointer = 0, oldAddressPointer = 0;

	if (!confirmPopup("Are You Sure?")) {
			//menuEeprom.comeBack();
	} else {
	menuEeprom.alertWindow("   !!!! saving setup !!!!!", 20, 40, 260, 60, 12,20);

		if(debugOutput_eepromIno) Serial.printf("\nStoring\n-----------------------\n");
		int storedBits = 0;
		// Dmx_Channels[].faderValues
		oldAddressPointer = addressPointer;
		if(debugOutput_eepromIno) Serial.printf("\nDmx_Channels[].faderValues %d x %d Byte from %04d", sizeof(Dmx_Channels)/sizeof(Dmx_Channels[0]), sizeof(Dmx_Channels[0].faderValue), addressPointer);
		for(int i = 0; i < NUM_DMX_CHANNELS; i++){
			EEPROM.update(addressPointer, Dmx_Channels[i].faderValue);
			addressPointer++;
			storedBits = storedBits + getToggledBitsFromByte(Dmx_Channels[i].faderValue, false);
		}
		if(debugOutput_eepromIno) Serial.printf(" to %04d (%d Byte)\n", addressPointer-1, addressPointer - oldAddressPointer);

		// PatchBay[].patchedChannels[]
		oldAddressPointer = addressPointer;
		if(debugOutput_eepromIno) Serial.printf("\nPatchBays[].patchedChannels[] (%d/2) x (4Bit+4Bit) from %04d", sizeof(Dmx_Channels)/sizeof(Dmx_Channels[0]), addressPointer);
		for(int i = 0; i < NUM_DMX_CHANNELS; i++){
			EEPROM.update(addressPointer, Dmx_Channels[i].patchBayID);
			addressPointer++;
			storedBits = storedBits + getToggledBitsFromByte(Dmx_Channels[i].patchBayID, false);
		}
		if(debugOutput_eepromIno) Serial.printf(" to %04d (%d Byte)\n", addressPointer-1, addressPointer - oldAddressPointer);

		// PatchBay[].faderValues
		oldAddressPointer = addressPointer;
		if(debugOutput_eepromIno) Serial.printf("\npatchBays[].faderValues %d x %d Byte from %04d", sizeof(patchBays)/sizeof(patchBays[0]), sizeof(patchBays[0].faderValue), addressPointer);
		for(int i = 0; i < NUM_PATCHBAYS; i++){
			EEPROM.update(addressPointer, patchBays[i].faderValue);
			addressPointer++;
			storedBits = storedBits + getToggledBitsFromByte(patchBays[i].faderValue, false);
		}
		if(debugOutput_eepromIno) Serial.printf(" to %04d (%d Byte)\n", addressPointer-1, addressPointer - oldAddressPointer);

		// PatchBay[].name
		oldAddressPointer = addressPointer;
		if(debugOutput_eepromIno) Serial.printf("\npatchBays[].name %d x 11 Byte from %04d", sizeof(patchBays)/sizeof(patchBays[0]), addressPointer);
		for(int i = 0; i < NUM_PATCHBAYS; i++){
			for(int j = 0; j < 11; j++){
				// name char[11]
				EEPROM.update(addressPointer, patchBays[i].name[j]);
				addressPointer++;
				storedBits = storedBits + getToggledBitsFromByte(patchBays[i].name[j], false);
			}
		}
		if(debugOutput_eepromIno) Serial.printf(" to %04d (%d Byte)\n", addressPointer-1, addressPointer - oldAddressPointer);

		// connectedDevices[]
		oldAddressPointer = addressPointer;
		if(debugOutput_eepromIno) Serial.printf("\nconnectedDevices[] %d x 15 Byte from %04d", sizeof(connectedDevices)/sizeof(connectedDevices[0]), addressPointer);
		for(int i = 0; i < MAX_CONNECTED_DEVICES; i++){
			for(int j = 0; j < 11; j++){
				// name char[11]
				EEPROM.update(addressPointer, connectedDevices[i].name[j]);
				addressPointer++;
				storedBits = storedBits + getToggledBitsFromByte(connectedDevices[i].name[j], false);
			}
			eeprom_putInt(addressPointer, connectedDevices[i].startChannel);
			addressPointer += 2;
			storedBits = storedBits + getToggledBitsFromByte(connectedDevices[i].startChannel, true);

			eeprom_putInt(addressPointer, connectedDevices[i].deviceModelID);
			addressPointer += 2;
			storedBits = storedBits + getToggledBitsFromByte(connectedDevices[i].deviceModelID, true);
		}
		if(debugOutput_eepromIno) Serial.printf(" to %04d (%d Byte)\n", addressPointer-1, addressPointer - oldAddressPointer);

		if(debugOutput_eepromIno) Serial.printf("CRC: %d bits set to 1\n", storedBits);
		writeCRC(storedBits);
		togglePurposefullyFlag();

		if(debugOutput_eepromIno) Serial.printf("\n%d Bytes stored. EEPROM usage: %d%% (free: %d Byte)", addressPointer,  addressPointer*100/2048, crcPosition - addressPointer);

		if(debugOutput_eepromIno) {
			Serial.printf("Verification data:\n---------------------\n");
			print_Dmx_Channels_element(0);
			print_Dmx_Channels_element(1);
			print_patchBays_element(0);
			print_patchBays_element(1);
			print_connectedDevices_element(0);
			print_connectedDevices_element(1);
			print_usedPatchChannels();
		}

		tft.setCursor(10,10);
		tft.setTextColor(ILI9341_WHITE, ILI9341_BLACK);
		delay(2000);
		menuEeprom.comeBack();
	}
}		//end of confirmPopup


void EEPROMclear() {
	// byte returnBuffer = 0;

	if (!confirmPopup("Are You Sure?")) {
		menuEeprom.comeBack();
	} else {
		if(debugOutput_eepromIno) Serial.printf("\nErasing\n-----------------------\n");
		if(debugOutput_eepromIno) Serial.printf("Write %d x 0 to EEPROM\n", startPatchedChannels);
		for (int i = 0; i < startPatchedChannels; i++) EEPROM.update(i, 0);
		if(debugOutput_eepromIno) Serial.printf("Write %d x -1 to EEPROM\n", startPatchBay_faderValues - startPatchedChannels);
		for (int i = startPatchedChannels; i < startPatchBay_faderValues; i++) EEPROM.update(i, -1);
		if(debugOutput_eepromIno) Serial.printf("Write %d x 0 to EEPROM\n", 2048 - startPatchBay_faderValues);
		for (int i = startPatchBay_faderValues; i < 2047; i++) EEPROM.update(i, 0);

		writeCRC(getToggledBitsFromMemory());
		togglePurposefullyFlag();

	}
	menuEeprom.comeBack();
}

void factoryReset() {				// CLEARS EEPROM AND RESTORES TO FRESHLY CLEANED EEPROM
EEPROMclear();
EEPROMrestore();
}

void debug_breakpoint_tft_counter(){
	static int buffer = 0;
	tft.printf(" Program reached Point Nr: %i \n", buffer + 1);
	buffer++;
}

void EEPROMrestoreFunction(){
	// re-create data structures out of EEPROM data
	//int8_t first=0, second=0; // just for "half-byte-handling", presently unused
int buffer_for_debug = 0;
	byte returnBuffer = 0;
	int16_t addressPointer = 0, oldAddressPointer = 0;


	resetGlobalDatastructures();

	// Dmx_Channels[].faderValues
	oldAddressPointer = addressPointer;
	if(debugOutput_eepromIno) Serial.printf("\nDmx_Channels[].faderValues %d x EEPROM.read %d Byte from %04d", sizeof(Dmx_Channels)/sizeof(Dmx_Channels[0]), sizeof(Dmx_Channels[0].faderValue), addressPointer);
	if(dataCommandsOutput_eepromIno) Serial.printf("\n-----<snip>-----\n");
	for(int i = 0; i < NUM_DMX_CHANNELS; i++){
		Dmx_Channels[i].faderValue = EEPROM.read(addressPointer);
		if(dataCommandsOutput_eepromIno && Dmx_Channels[i].faderValue > 0) Serial.printf("Dmx_Channels[%d].faderValue = %d;\n", i, Dmx_Channels[i].faderValue);
		addressPointer++;
	}

	if(dataCommandsOutput_eepromIno) Serial.printf("\n-----</snap>-----\n");
	if(debugOutput_eepromIno) Serial.printf(" to %04d (%d Byte)\n", addressPointer-1, addressPointer - oldAddressPointer);

	// PatchBays[].patchedChannels[]
	//            .numChannels
	// Dmx_Channels[].patchBayID
	// usedPatchChannels[]
	oldAddressPointer = addressPointer;
	if(debugOutput_eepromIno) Serial.printf("\nPatchBays[].patchedChannels[] (%d) x EEPROM.read %d Byte from %04d", sizeof(Dmx_Channels)/sizeof(Dmx_Channels[0]), sizeof(Dmx_Channels[0].patchBayID), addressPointer);
	if(dataCommandsOutput_eepromIno) Serial.printf("\n-----<snip>-----\n");
	for(int i = 0; i < NUM_DMX_CHANNELS; i++){
		int8_t returnBuffer2 = EEPROM.read(addressPointer);
		Dmx_Channels[i].patchBayID = returnBuffer2;

		if(returnBuffer2 != -1){		// this is true if  eeprom is blank (new device). bugged!
			patchBays[returnBuffer2].patchedChannels[patchBays[returnBuffer2].numChannels] = i + 1;
			patchBays[returnBuffer2].numChannels++;
			usedPatchChannels[i + 1 ] = 1;

			if(dataCommandsOutput_eepromIno) Serial.printf("patchBays[%d].patchedChannels[%d] = %d;\n", returnBuffer2, patchBays[returnBuffer2].numChannels-1, i + 1);
			if(dataCommandsOutput_eepromIno) Serial.printf("patchBays[%d].numChannels++;\n", returnBuffer2);
			if(dataCommandsOutput_eepromIno) Serial.printf("usedPatchChannels[%d] = 1;\n", i + 1);
		}
		addressPointer++;

	}

	if(dataCommandsOutput_eepromIno) Serial.printf("\n-----</snap>-----\n");
	if(debugOutput_eepromIno) Serial.printf(" to %04d (%d Byte)\n", addressPointer-1, addressPointer - oldAddressPointer);

	// patchBays[].faderValues
	oldAddressPointer = addressPointer;
	if(debugOutput_eepromIno) Serial.printf("\npatchBays[].faderValues %d x %d Byte from %04d", sizeof(patchBays)/sizeof(patchBays[0]), sizeof(patchBays[0].faderValue), addressPointer);

	if(dataCommandsOutput_eepromIno) Serial.printf("\n-----<snip>-----\n");
	for(int i = 0; i < NUM_PATCHBAYS; i++){
		patchBays[i].faderValue = EEPROM.read(addressPointer);
		if(dataCommandsOutput_eepromIno && patchBays[i].faderValue > 0) Serial.printf("patchBays[%d].faderValue = %d;\n", i, patchBays[i].faderValue);
		addressPointer++;
	}
	if(dataCommandsOutput_eepromIno) Serial.printf("\n-----</snap>-----\n");
	if(debugOutput_eepromIno) Serial.printf(" to %04d (%d Byte)\n", addressPointer-1, addressPointer - oldAddressPointer);

	// patchBays[].name
	oldAddressPointer = addressPointer;
	if(debugOutput_eepromIno) Serial.printf("\npatchBays[].name %d x 11 Byte from %04d", sizeof(patchBays)/sizeof(patchBays[0]), addressPointer);
	if(dataCommandsOutput_eepromIno) Serial.printf("\n-----<snip>-----\n");
	for(int i = 0; i < NUM_PATCHBAYS; i++){
		for(int j = 0; j < 11; j++){
			// name char[11]
			patchBays[i].name[j] = EEPROM.read(addressPointer);
			addressPointer++;
		}
	}


	if(dataCommandsOutput_eepromIno) Serial.printf("\n-----</snap>-----\n");
	if(debugOutput_eepromIno) Serial.printf(" to %04d (%d Byte)\n", addressPointer-1, addressPointer - oldAddressPointer);

	// connectedDevices[].name
	//                   .startChannel
	//                   .deviceModelID
	// Dmx_Channels[].deviceConnected
	//               .connectedDevice
	//               .deviceChannelID
	oldAddressPointer = addressPointer;
	if(debugOutput_eepromIno) Serial.printf("\nconnectedDevices[] %d x 15 Byte from %04d", sizeof(connectedDevices)/sizeof(connectedDevices[0]), addressPointer);

	if(dataCommandsOutput_eepromIno) Serial.printf("\n-----<snip>-----\n");
	for(int8_t i = 0; i < MAX_CONNECTED_DEVICES; i++){
		// connectedDevices[].name
		// connectedDevices[].startChannel
		// connectedDevices[].deviceModelID
		returnBuffer = EEPROM.read(addressPointer);
		if(returnBuffer != 0){
			connectedDevices[i].name[0] = returnBuffer;
			addressPointer++;
			for(int j = 1; j < 11; j++){
				// name char[11]
				connectedDevices[i].name[j] = EEPROM.read(addressPointer);
				addressPointer++;
			}
			if(dataCommandsOutput_eepromIno && strlen(connectedDevices[i].name) > 1) Serial.printf("connectedDevices[%d].name = \"%s\";\n", i, connectedDevices[i].name);
			connectedDevices[i].startChannel = eeprom_getInt(addressPointer);
			if(dataCommandsOutput_eepromIno && strlen(connectedDevices[i].name) > 1) Serial.printf("connectedDevices[%d].startChannel = %d;\n", i, connectedDevices[i].startChannel);
			addressPointer += 2;
			connectedDevices[i].deviceModelID = eeprom_getInt(addressPointer);
			if(dataCommandsOutput_eepromIno && strlen(connectedDevices[i].name) > 1) Serial.printf("connectedDevices[%d].deviceModelID = %d;\n", i, connectedDevices[i].deviceModelID);
			addressPointer += 2;

			if(strlen(connectedDevices[i].name) < 2) continue;
			// Dmx_Channels[].deviceConnected
			//               .connectedDevice
			//               .deviceChannelID

// bug is here in that function. hallo() appproved.
			for(int j = 0; j < devicesAvailable[connectedDevices[i].deviceModelID].numChannels; j++){
				hallo();
				Dmx_Channels[connectedDevices[i].startChannel + j].deviceConnected = true;
				Dmx_Channels[connectedDevices[i].startChannel + j].connectedDeviceID = i;
				Dmx_Channels[connectedDevices[i].startChannel + j].deviceChannelID = j;
				if(strlen(connectedDevices[i].name) > 1) {
					if(dataCommandsOutput_eepromIno) Serial.printf("Dmx_Channels[%d].deviceConnected = true;\n", connectedDevices[i].startChannel + j);
					if(dataCommandsOutput_eepromIno) Serial.printf("Dmx_Channels[%d].connectedDeviceID = %d;\n", connectedDevices[i].startChannel + j, i);
					if(dataCommandsOutput_eepromIno) Serial.printf("Dmx_Channels[%d].deviceChannelID = %d;\n", connectedDevices[i].startChannel + j, j);
				}
			}
		} else {
			// there is an empty device, so increase addressPointer to next device:
			addressPointer = addressPointer + 15;
		}
	}

	if(dataCommandsOutput_eepromIno) Serial.printf("\n-----</snap>-----\n");

	if(debugOutput_eepromIno) Serial.printf(" to %04d (%d Byte)\n", addressPointer-1, addressPointer - oldAddressPointer);

	if(debugOutput_eepromIno)  {
		Serial.printf("\nVerification data: (first 2 entries of all entities)\n---------------------------------------------------------------\n");
		print_Dmx_Channels_element(0);
		print_Dmx_Channels_element(1);
		print_patchBays_element(0);
		print_patchBays_element(1);
		print_connectedDevices_element(0);
		print_connectedDevices_element(1);
		print_usedPatchChannels();
	}

}
void EEPROMrestore() {
	if(debugOutput_eepromIno) Serial.printf("\nRestoring\n-----------------------\n");
	if(EEPROM.read(2047) == 1) {
		int crcFromMemory = eeprom_getInt(crcPosition); // crc is double byte 2045/2046
		int storedBits = getToggledBitsFromMemory();
		if(debugOutput_eepromIno) Serial.printf("\nStored CRC is %d, stored bits are %d.\n", crcFromMemory, storedBits);
		if(debugOutput_eepromIno && crcFromMemory != storedBits) Serial.printf("CRC ERROR. Don't restore.\n");
		if(crcFromMemory != storedBits)
		{
			menuEeprom.alertWindow("   !!!! CRC ERROR !!!!!", 20, 40, 260, 60, 12,20);
		} else {
			menuEeprom.alertWindow("   !!!! loading setup !!!!!", 20, 40, 260, 60, 12,20);
			EEPROMrestoreFunction();
		}
	} else {
		if(debugOutput_eepromIno) Serial.printf("\nNo purposefully stored data. Don't restore.\n");
		menuEeprom.alertWindow("   !!!! nothing saved !!!!!", 20, 40, 260, 60, 12,20);
	}
	delay(2000);
	menuEeprom.comeBack();
}

void resetGlobalDatastructures(){
	// resets the global data structures
	//     Dmx_Channels[]
	//     patchBays[]
	//     connectedDevices[]
	//     usedPatchChannels[]
	for(int i = 0; i < NUM_DMX_CHANNELS; i++){
		Dmx_Channels[i] = {};
		Dmx_Channels[i].deviceConnected = false;
		Dmx_Channels[i].patchBayID = -1;
	}
	for(int i = 0; i < NUM_PATCHBAYS; i++){
		patchBays[i] = {};
	}
	for(int i = 0; i < MAX_CONNECTED_DEVICES; i++){
		connectedDevices[i] = {};
		connectedDevices[i].deviceModelID = -1;
	}
	for(int i = 0; i < NUM_DMX_CHANNELS+1; i++){
		usedPatchChannels[i] = 0;
	}
}

// helper functions for accessing double-bytes
void eeprom_putInt(int adress, int val ) {
  /* writes 16 bit integers to eeprom.
     takes an int val and adress, returnes nothing
  */
  byte lower_8bits, upper_8bits;

  lower_8bits = val & 0xff;
  upper_8bits = (val >> 8) & 0xff;
  EEPROM.update(adress, lower_8bits);
  EEPROM.update(adress + 1, upper_8bits);
}
int eeprom_getInt(int adress) {
  /* reads 16 bit integers from eeprom.
     takes byte address, returnes integer.
  */
  byte lower_8bits, upper_8bits;
  lower_8bits = EEPROM.read(adress);
  upper_8bits = EEPROM.read(adress + 1);
  return (upper_8bits << 8) | lower_8bits;
}

// helper functions to shrink memory for small values in range [-1..14] (i.e. patchBayID)
//     BEWARE: a halfbyte only stores 16 distinct values. so it does not work for 16 patchBays PLUS '-1' !!
void store2halfBytes(int8_t first, int8_t second, byte& buffer){
	// stores 2 "half-bytes" of our special type [-1..14]
	//    <first>, <second>
	// in one byte of
	//    <buffer>
	buffer = first+1;
	buffer = buffer << 4;
	buffer += second+1;
}
void restore2halfBytes(byte buffer, int8_t& first, int8_t& second){
	// restores 2 "half-bytes" of our special type [-1..14]
	//    <first>, <second>
	// out of one byte of
	//    <buffer>
	first = buffer >> 4;
	second = buffer - (first*16);
	first--; second--;
}

// debug helper functions
void print_Dmx_Channels_element(int channelID){
	Serial.printf("Dmx_Channels[%d]\n", channelID);
	Serial.printf("    .deviceConnected = %s\n", (Dmx_Channels[channelID].deviceConnected) ? "true" : "false");
	Serial.printf("    .connectedDeviceID = %d\n",Dmx_Channels[channelID].connectedDeviceID);
	Serial.printf("    .deviceChannelID = %d\n", Dmx_Channels[channelID].deviceChannelID);
	Serial.printf("    .faderValue = %d\n", Dmx_Channels[channelID].faderValue);
	Serial.printf("    .patchBayID = %d\n", Dmx_Channels[channelID].patchBayID);
	Serial.println();
}
void print_patchBays_element(int patchBayID){
	Serial.printf("patchBays[%d]\n", patchBayID);
	Serial.printf("    .faderValue = %d\n", patchBays[patchBayID].faderValue);
	Serial.printf("    .numChannels = %d\n", patchBays[patchBayID].numChannels);
	Serial.printf("    .patchedChannels[] = ");
	for(int i = 0; i < patchBays[patchBayID].numChannels; i++){
		Serial.printf("%d, ", patchBays[patchBayID].patchedChannels[i]);
	}
	Serial.println();Serial.println();
}
void print_connectedDevices_element(int deviceID){
	Serial.printf("connectedDevices[%d]\n", deviceID);
	Serial.printf("    .name = %s\n", connectedDevices[deviceID].name);
	Serial.printf("    .deviceModelID = %d\n", connectedDevices[deviceID].deviceModelID);
	Serial.printf("    .startChannel = %d\n", connectedDevices[deviceID].startChannel);
	Serial.println();
}
void print_usedPatchChannels(){
	Serial.printf("usedPatchChannels[] = ");
	for(int i = 0; i < NUM_DMX_CHANNELS+1; i++){
		Serial.printf("%d, ", usedPatchChannels[i]);
	}
	Serial.println();Serial.println();
}

// CRC functions
int getToggledBitsFromByte(int i, bool twoBytes) {
	// count the number of bits set to "1" within a single or double byte
	// twoBytes = false -> i is  8 bit value
	//          = true  -> i is 16 bit value
	if (twoBytes){
		return ((i>>15)&1)+((i>>14)&1)+((i>>13)&1)+((i>>12)&1)+((i>>11)&1)+((i>>10)&1)+((i>>9)&1)+((i>>8)&1)+((i>>7)&1)+((i>>6)&1)+((i>>5)&1)+((i>>4)&1)+((i>>3)&1)+((i>>2)&1)+((i>>1)&1)+(i&1);
	} else {
		return ((i>>7)&1)+((i>>6)&1)+((i>>5)&1)+((i>>4)&1)+((i>>3)&1)+((i>>2)&1)+((i>>1)&1)+(i&1);
	}
}
int getToggledBitsFromMemory() {
	int storedBits = 0;
	for (int i=0; i<crcPosition; i++) storedBits = storedBits + getToggledBitsFromByte(EEPROM.read(i), false);
	return storedBits;
}
void writeCRC(int crc){
	if(debugOutput_eepromIno) Serial.printf("Write %d (2 bytes) at position %d/%d\n", crc, crcPosition, crcPosition+1);
	eeprom_putInt(crcPosition, crc);
}
void togglePurposefullyFlag(){
	// sets purposefully-flag to 1
	if(debugOutput_eepromIno) Serial.printf("Write 1 at position %d\n", purposefullyFlagPosition);
	EEPROM.update(purposefullyFlagPosition, 1); // this is purposefully written data
}
boolean saved = 0;

void autoSave_eeprom(uint16_t delay){
//static unsigned long eepromsaveTimer = gNoInteractionSince;
if (millis() - gNoInteractionSince < 100 && saved) {
	saved = false;		//reset saved state after Interaction
}

	if (!saved && (millis() - gNoInteractionSince > delay)) {
	saved = 1;
	EEPROMsave_for_autoSave();
	//autosaveCounter();
	}
}
