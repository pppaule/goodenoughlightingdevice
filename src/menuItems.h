#include <Arduino.h>

typedef void MenuFuncPtr(); // funktionspointer auf die menu functionen
typedef void (*MenuFuncPtr_withIntArg)(
    int); // funktionspointer auf die menu functionen mit einem (int-)argument

char *menu_main[] =
    { // mmenu items for main menu. [0] is the headline
        (char *)" G.E.L.D.                               pre 1.0 beta ",
        (char *)" Light-controller ",
        (char *)" Save / Restore",

        (char *)" Software Update "};

MenuFuncPtr *menuMain_func[] = {0, // funktionspointer hauptmenu
                                lightControlMenu,
                                eepromMenu,
                                reboot_device};

const uint8_t numMenu =
    (sizeof(menu_main) /
     sizeof(char *)); // die zahl der menuepunkte zur uebergabe an die library.
                      // habe aus der numMenu "-1" rausgenommen
TftMenuLib
    menuMain(menu_main, menuMain_func, numMenu,
             &tft); // globale Initialisierung des Hauptmenu als library instanz

// untermenu lightControlMenu

char *menu_lightControl[] = {
    (char *)" Lighting Control Menu",      (char *)" Single Channel Control ",
    (char *)" Patched Channel Control ",   (char *)" Scene Control ",
    (char *)" Lighting Device Controller", (char *)" Lighting Device Setup"};

MenuFuncPtr *func_lightControl[] = {
    0,          consoleDisplay, patchBay, sceneControl_console, deviceControl,
    deviceSetup};

const uint8_t num_lightControl =
    (sizeof(menu_lightControl) /
     sizeof(char *) /*-1*/); // habe aus der numMenu "-1" rausgenommen
TftMenuLib menuLightControl(menu_lightControl, func_lightControl,
                            num_lightControl, &tft);

// Untermenu file operations

char *menu_eeprom[] = {
    (char *)" Save / Restore Menu", (char *)" Save to Eeprom ",
    (char *)" Restore from Eeprom", (char *)" Factory Reset"};

MenuFuncPtr *func_eeprom[] = {
    0,
    EEPROMsave,
    EEPROMrestore,
    factoryReset,
};

const uint8_t num_eeprom =
    (sizeof(menu_eeprom) /
     sizeof(char *)); // habe aus der numMenu "-1" rausgenommen
TftMenuLib menuEeprom(menu_eeprom, func_eeprom, num_eeprom, &tft);

char *menu_CRMX[]{(char *)" Wireless Setup",
                  //  (char *)" Wireless Enable ",
                  (char *)" Link Devices", (char *)" Unlink all Devices",
                  (char *)" Radio Power", (char *)" DMX Break Time",
                  (char *)" DMX Refresh Rate", (char *)" Block WLAN Channels"};

// Untermenu Timo konfiguration
char *subMenu_consoleItems[]{(char *)" Selected DMX-Channel: ",
                             // (char *)" FX-Set",
                             // (char *)" Mute",
                             // (char *)" Solo",
                             // (char *)" Reset Channel",
                             (char *)" Save Scene"};

const int numSubConsole = (sizeof(subMenu_consoleItems) / sizeof(char *));
MenuFuncPtr_withIntArg subMenu_consoleFunc[numSubConsole] = {
    // 0,
    // 0,
    // 0,
    // 0,
    (MenuFuncPtr_withIntArg)
        saveSceneToRam}; // set them in patchBay.ino, because there the funcs
                         // are declared
// Light_Control->Patch_Bay->[configure single patchbay]

char *subMenu_patchBayItems[]{(char *)" Selected Patchfader: ",
                              (char *)" Patch Channels",
                              (char *)" List Connections",
                              (char *)" (Re-)Name",
                              (char *)" Move",
                              (char *)" Clear",
                              (char *)" Save Scene"};
const int numSubPatchBay = (sizeof(subMenu_patchBayItems) / sizeof(char *));
MenuFuncPtr_withIntArg subMenu_patchBayFunc[numSubPatchBay] = {
    0}; // set them in patchBay.ino, because there the funcs are declared
