// -------------------------------------------------------------------------------------------
// I2C Advanced Bus Scanner
// -------------------------------------------------------------------------------------------
//
// This creates an I2C master device which will scan the address space and report all
// devices which ACK.  It does not attempt to transfer data, it only reports which devices
// ACK their address.
//
// This version will sweep all existing I2C buses (eg. Wire, Wire1, Wire2, Wire3).
//
// Pull the control pin low to initiate the scan.  Result will output to Serial.
//
// This example code is in the public domain.
// -------------------------------------------------------------------------------------------

#include <i2c_t3.h>

// -------------------------------------------------------------------------------------------
// Defines - modify as needed for sweep range and bus pin config
//
#define TARGET_START 0x01
#define TARGET_END   0x7F

#define WIRE_PINS   I2C_PINS_18_19


// -------------------------------------------------------------------------------------------
// Function prototypes
void i2cInit(void);
void scan_bus(i2c_t3& Wire, uint8_t all);
void print_bus_status(i2c_t3& Wire);
void print_scan_status(struct i2cStruct* i2c, uint8_t target, uint8_t& found, uint8_t all);

// -------------------------------------------------------------------------------------------

void i2cInit() {		// this is the original GELD i2c initialization!
		Wire.begin( I2C_MASTER, 0x00, I2C_PINS_18_19, I2C_PULLUP_EXT, 400000 );
    Wire.setDefaultTimeout(10000);
}


void i2c_scan() {   //this works on geld_2019!
  Serial.print("---------------------------------------------------\n");
  Serial.print("Bus Status Summary\n");
  Serial.print("==================\n");
  Serial.print(" Bus    Mode   SCL  SDA   Pullup   Clock\n");
  print_bus_status(Wire);
  scan_bus(Wire, 0);
}


// -------------------------------------------------------------------------------------------
// scan bus
//
void scan_bus(i2c_t3& Wire, uint8_t all)
{
    uint8_t target, found = 0;

    Serial.print("---------------------------------------------------\n");
    if(Wire.bus == 0)		//checking bus-number
        Serial.print("Starting scan: Wire\n");
    else
        Serial.printf("Starting scan: Wire%d\n",Wire.bus);

    digitalWrite(LED_BUILTIN,HIGH); // LED on
    for(target = TARGET_START; target <= TARGET_END; target++) // sweep addr, skip general call
    {
        Wire.beginTransmission(target);       // slave addr
        Wire.endTransmission();               // no data, just addr
        print_scan_status(Wire.i2c, target, found, all);
    }
    digitalWrite(LED_BUILTIN,LOW); // LED off

    if(!found) Serial.print("No devices found.\n");
}

// -------------------------------------------------------------------------------------------
// print bus status
//
void print_bus_status(i2c_t3& Wire)
{
    struct i2cStruct* i2c = Wire.i2c;
    if(Wire.bus == 0)
        Serial.print("Wire   ");
    else
        Serial.printf("Wire%d  ",Wire.bus);
    switch(i2c->currentMode)
    {
    case I2C_MASTER: Serial.print("MASTER  "); break;
    case I2C_SLAVE:  Serial.print(" SLAVE  "); break;
    }
    Serial.printf(" %2d   %2d  ", Wire.i2c->currentSCL, Wire.i2c->currentSDA);
    switch(i2c->currentPullup)
    {
    case I2C_PULLUP_EXT: Serial.print("External  "); break;
    case I2C_PULLUP_INT: Serial.print("Internal  "); break;
    }
    Serial.printf("%d Hz\n",i2c->currentRate);
}

// -------------------------------------------------------------------------------------------
// print scan status
//

void print_scan_status(struct i2cStruct* i2c, uint8_t target, uint8_t& found, uint8_t all)
{
    switch(i2c->currentStatus)
    {
    case I2C_WAITING:  Serial.printf("Addr: 0x%02X ACK\n",target); found=1; break;
    case I2C_ADDR_NAK: if(all) { Serial.printf("Addr: 0x%02X\n",target); } break;
    case I2C_TIMEOUT:  if(all) { Serial.printf("Addr: 0x%02X Timeout\n",target); } break;
    default: break;
    }
}
