#include <Arduino.h>
#include "ILI9341_t3.h" // for font-type

// makros, die auch in libraries benutzt werden sollen,
//         muessen in dieser datei definiert werden.
// (die headers.h kann nicht in libs eingebunden werden!)
// fuer variablen + konstanten reicht die erwaehnung als "extern" hier
#define MAXDMXCHANNELS 512

// eigene farb-definitionen:
#define RUSSENROT 0xE022
#define RUSSENGRUEN 0x9592
#define RUSSENGELB 0xFEF1

#ifndef HEADER_GLOBAL_TYPEDEF
	#define HEADER_GLOBAL_TYPEDEF

	// define data structure for device database:
	typedef struct Device_Channel_t {
		// only for internal use by Device_Model_t
		const char * type;		// "RGB_B" Should be max 6 char, because of patchbay channel grid !!!
		bool isBinary;		// if only ON|OFF
		const char * unit;		// "%", "K", ...
		int16_t zeroVal;	//   0, 2200,...
		int16_t maxVal;	// 100, 5500,...
	} Device_Channel_t;
	typedef struct Device_Model_t {
		// (should somehow link to device-database)
		const char * name;			// "Skypanel SP60 Mode 1 (CCT & RGBW)"
		const char * className;		// "LED softlight"
		const char * manufacturer;    // "ARRI"
		const char * shortName; // "SP60": beware: MAX 7 char & NO spaces!!
		uint8_t numChannels;	// 12
		Device_Channel_t deviceChannels [32]; // deviceModel.deviceChannels[0].type = "RGB_B"
		// like to change it to
		// Device_Channel_t deviceChannels [MAX_CHANNELS_PER_DEVICE];
		// but then we get definition hickup
	} Device_Model_t;

	// define datastructure for connected devices:
	typedef struct Connected_Device_t {
		char name[11];					// "SuSpo3"
		int16_t deviceModelID = -1;	// [-1|0..maxDevicesAvailable] ID in devicesAvailable[]
		uint16_t startChannel;			// 8
	} Connected_Device_t;
	/*
		Connected_Device_t connectedDevices [MAX_CONNECTED_DEVICES];

		connectedDevices[0].name = "SuSpo3"
		connectedDevices[0].startChannel = 8
		sprintf(connectedDevices[0].name)
		 	--> "SuSpo3"
		sprintf(connectedDevices[0].deviceModel.name)
		 	--> "Super RGB Spot"
		sprintf(connectedDevices[0].deviceModel.manufacturer)
		 	--> "China 4 U"
		sprintf(connectedDevices[0].deviceModel.numChannels)
		 	--> 12
		sprintf(connectedDevices[0].deviceModel.deviceChannels[0].type)
		 	--> "RGB_B"
	*/

	// define datastructure for DMX channels:
	typedef struct Dmx_Channel_t {
		bool deviceConnected = false;
		//Connected_Device_t *connectedDevice;   // pointer to "SuSpo3"-Connected_Device_t
		int8_t connectedDeviceID;   // ID in connectedDevices[]
		uint8_t deviceChannelID;					// 0 .. connectedDevice.deviceModel.numChannels, ID of deviceModel.deviceChannels[]
		uint8_t faderValue;						// 0..255
		int8_t patchBayID = -1; // -1|0..7   ID of spatchBays[]. redundant cross-info. not needed for code, but for eeprom storage
	} Dmx_Channel_t;
	/*
		Dmx_Channel_t Dmx_Channels [NUM_DMX_CHANNELS];
		Dmx_Channels[0].connectedDevice = * connectedDevices[0];
		Dmx_Channels[0].deviceChannelID = 0;
		Dmx_Channels[0].faderValue = 64;
		sprintf(Dmx_Channels[0].connectedDevice.name)
		 	--> "SuSpo3"
		sprintf(Dmx_Channels[0].connectedDevice.deviceModel.name)
		 	--> "Super RGB Spot"
		sprintf(Dmx_Channels[0].connectedDevice.deviceModel.deviceChannels[ Dmx_Channels[0].deviceChannelID ].type)
		 	--> "RGB_B"
	*/

	// define datastructure for PatchBay:
	typedef struct Patch_Bay_t {
		char name[11];
		uint16_t patchedChannels[512]; // array of indices of Dmx_Channels[N]
		uint16_t numChannels;	// mem usage of this patchBay, for "no more than 512 in total"-restriction
		uint8_t faderValue;		// 0..255
		uint8_t targetValue = 255;
	} Patch_Bay_t;
	/*
		Patch_Bay_t patchBays [NUM_PATCHBAYS];
		patchBays[0].numChannels = 3
		patchBays[0].faderValue = 64
		patchBays[0].patchedChannels[0] = 0
		sprintf(Dmx_Channels[ patchBays[0].patchedChannels[0] ].connectedDevice.name)
		 	--> "SuSpo3"
		sprintf(Dmx_Channels[ patchBays[0].patchedChannels[0] ].connectedDevice.deviceModel.deviceChannels[ patchBays[0].patchedChannels[0].connectedDevice.deviceChannelID ].type)
		 	--> "RGB_B"
		sprintf(Dmx_Channels[ patchBays[0].patchedChannels[0] ].connectedDevice.deviceModel.deviceChannels[ patchBays[0].patchedChannels[0].connectedDevice.deviceChannelID ].isBinary)
		 	--> false
	*/

	// define datastructure for Channel-Info:
	typedef struct DMX_ChannelInfo_t {
		char connectionState[5]; // "used|rsvd|free"
		uint16_t channelNum;
		uint16_t displayText_ChannelNum;
		const char * displayText_Type;
		int16_t displayText_Value; // may be negative!!
		const char * displayText_Unit;
		uint8_t faderValue;
		int8_t connectedDeviceID;
	} DMX_ChannelInfo_t;
	/*
		This is only for the get_channelInfos(channelNum) function,
		which collects the channels data for display
		connectionState:
 			used 		- device connected, channel used
			reserved 	- device connected, channel not used
			free		- no device connected
	*/

	// define datastructure for slider bar displays
	typedef struct SliderBar_t {
		char barTextAbove[7]; // text above bar
		char barTextBesides[12]; // text besides bar
		char barTextBelow[7]; // text below bar
		uint8_t barValue; // bar height [0..255]
	} SliderBar_t;

	// define structure for gevice-grouped channel grid page item:
	typedef struct ChannelGridPage_Item_t {
		uint8_t row;
		uint8_t col;
		char type; // 'C': channel, 'H': header for device, 'F': footer
		uint16_t objectID; // channel- or device-ID
	} ChannelGridPage_Item_t;
	// define structure for gevice-grouped channel grid page:
	typedef struct ChannelGridPage_t {
		uint16_t firstItemID;
		uint8_t itemCount; // number of actual items on this page
		ChannelGridPage_Item_t gridPageItems[32]; // max 8x4 items on one page (adapt to presently used display grid row/col)
	} ChannelGridPage_t;
	/*
		ChannelGridPage_t channelGrid[30]
		would create an array of 30 pages of type <ChannelGridPage_t>,
		each of those will contain an array of 32 elements of type <ChannelGridPage_Item_t>
		totalling up to 30 x 196 = 5880 byte <-- ONLY NEEDED AT RUNTIME, but there it's a considerable chunk (~20% at freeSRAM of 27k outside of patchBay)
		30 pages is worst case:
			all 32 devices have just 1 channel and are start-channel-placed,
			so that there is always a line break, with a single unused channel inbetween
			(and displaying 8 rows of 4 columns per page!)
	*/

	typedef struct DMX_Scene_t {
		  boolean isEmpty = true;
		  uint8_t dmxChannelValues[512];
	} DMX_Scene_t;
	/*
		DMX_Scene_t DMX_Scenes[NUM_SCENES];
	*/

	// // catch parameters from headers.h, where they are given a value:


	extern const uint16_t NUM_DMX_CHANNELS;
	extern const uint8_t NUM_PATCHBAYS;
	extern const uint8_t NUM_FADER;
	extern const uint8_t MAX_CHANNELS_PER_DEVICE;
	extern const uint8_t MAX_CONNECTED_DEVICES;

	extern Device_Model_t devicesAvailable [];
	extern Connected_Device_t connectedDevices [];
	extern Dmx_Channel_t Dmx_Channels [];
	extern Patch_Bay_t patchBays [];
	extern SliderBar_t gDisplaySliderBars[];
	extern char alphaNum[];
	extern char gThisView;

	extern ILI9341_t3_font_t fonts[];
#endif

/*
	Compile WARNING: conversion from string constant to 'char*'
	============================================================

	A quick summary of String declaration/allocation:

		(1) This form is completely invalid and should be avoided at all costs.
			It opens the door to all sorts of bad things happening:

				char *text = "This is some text";

			 "Create a variable called text, which is a read-write pointer to this string literal,
			  which is held in read-only (code) space (in FlashMem)."

		(2) This form is the right form if you are wanting to make the data editable:

				char text[] = "This is some text";

			 "Create an array of type "char" and initialize it with the data "This is some text\0".
			 The size of the array will be big enough to store the data (in SRAM)"

		(3) This form is the right form if you want strings that won't be edited:

				const char *text = "This is some text";

			"Create a variable called "text" that is a read only pointer to this data in read only memory (FlashMem)."

		(4) This form seems wasteful of RAM but it does have its uses. Best forget it for now though.

				const char text[] = "This is some text";
*/

/*
	Constant Pointer vs. Constant Pointed-to Data
	===============================================

		Non-constant pointer to constant data:
		- Data pointed to CANNOT be changed
		- Pointer CAN be changed to point to another data. For example,

			int i1 = 8, i2 = 9;
			const int * iptr = &i1;  // non-constant pointer pointing to constant data
			// *iptr = 9;   // error: assignment of read-only location
			iptr = &i2;  // okay

		Constant pointer to non-constant data:
		- Data pointed to CAN be changed
		- Pointer CANNOT be changed to point to another data. For example,

			int i1 = 8, i2 = 9;
			int * const iptr = &i1;  // constant pointer pointing to non-constant data - must be initialized during declaration!
			*iptr = 9;   // okay
			// iptr = &i2;  // error: assignment of read-only variable

		Constant pointer to constant data:
		- Data pointed to CANNOT be changed
		- Pointer CANNOT be changed to point to another data. For example,

			int i1 = 8, i2 = 9;
			const int * const iptr = &i1;  // constant pointer pointing to constant data
			// *iptr = 9;   // error: assignment of read-only variable
			// iptr = &i2;  // error: assignment of read-only variable

		Non-constant pointer to non-constant data:
		- Data pointed to CAN be changed
		- Pointer CAN be changed to point to another data. For example,

			int i1 = 8, i2 = 9;
			int * iptr = &i1;  // non-constant pointer pointing to non-constant data
			*iptr = 9;   // okay
			iptr = &i2;  // okay

		Oh yeah - not to forget: An array IS a pointer ;-)

			char text[]; or char text[10];
			// is the same as
			char * text;

*/
