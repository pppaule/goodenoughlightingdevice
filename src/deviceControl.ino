
/*  deviceControl
 *  Setting up and Controlling a Library of DMX Devices for GELD
 *
 */


int gSelected_connectedDevice_here_DC = 0; // in "device control" we keep it at 0, the "bullshit-detector" can be done without the "-1"-check
int gDisplayGrid_startDeviceID_here_DC = 0;


void deviceControl() {
	if(!checkDevicePresence()) return;
	gThisView = 'D'; // D: deviceControl
	console.setBarColor(GELD_GREEN, GELD_GREY1);     //definition der formatierung
	console.setBarDimensions(10, 105, 4);
	console.setBarCoordinates(20,68);
	deviceControl_initScreen();
	// track + handle changes in the device-grid
	while (true){
		backgroundFuncs();

		connectedDevice_Selector_here_DC(); // ausgelagerte encoder abfrage

		if (buttonPressed(SELECT)) {
			if ( connectedDevices[gSelected_connectedDevice_here_DC].deviceModelID != -1){
				display_gSelected_connectedDevice();
				deviceControl_initScreen();
			}
		}

		if (buttonPressed(EXIT)) {
			//consoleInit = 0;
			menuLightControl.menuListInit();
			menuLightControl.menuListSelect();
			break; // Loop beenden!
		}
	}
}
bool checkDevicePresence(){
	// checks if devices are present, otherwise go to other view
	bool noDevice = true;
	for(int i = 0; i<MAX_CONNECTED_DEVICES; i++){
		if(connectedDevices[i].deviceModelID  != -1) {
			noDevice = false;
			break;
		}
	}
	if(noDevice){
		if(confirmPopup("No devices. Setup?")) deviceSetup();
		return false;
	} else {
		return true;
	}
}

// display + interaction functions:
void deviceControl_initScreen(){
	// main screen for connectedDevices[] select
	console.printHeader(" Device Control Menu ");
	display_DeviceGrid_here_DC();
}

void display_DeviceGrid_here_DC(){
	// display grid of connectedDevices[]:
	// TODO: @paule:
	// add int Console.gridSelector(rows, columns) function to consoleLib
	// add int Console.gridItem(const char* name, int bg_color, int border_color, int selct_color)
	// add struct gridItem[MAX_CONNECTED_DEVICES]
	int gridCols = 2; int gridRows = 8; // gridCols that work out-of-the-box: 1,2,3,4
	int labelWidth = 288/gridCols; int labelHeigth = 20;
	display_DeviceGridPage_here_DC(gridCols, gridRows, labelWidth, labelHeigth);
}

void display_DeviceGridPage_here_DC(int gridCols, int gridRows, int labelWidth, int labelHeigth) {
	for(int row = 0; row < gridRows; row++){
		for(int col = 0; col < gridCols; col++){
			// display grid element:
			int deviceNum = gDisplayGrid_startDeviceID_here_DC + (row * gridCols) + col;
			char text[8]; //sprintf(text, "");
			if ( connectedDevices[deviceNum].deviceModelID  != -1) {
				// device connected
				sprintf(text, "%03d-%03d", connectedDevices[deviceNum].startChannel + 1, connectedDevices[deviceNum].startChannel + devicesAvailable[connectedDevices[deviceNum].deviceModelID].numChannels);
				tft.fillRoundRect(col * (labelWidth+6) + 5, row * (labelHeigth+5) + 32, labelWidth, labelHeigth, 3, ILI9341_CYAN);
				console.printTextAt(col * (labelWidth+6) + 9, row * (labelHeigth+5) + 39, text, GELD_GREY2, ILI9341_CYAN, 8);
				console.printTextAt(col * (labelWidth+6) + 50, row * (labelHeigth+5) + 37, connectedDevices[deviceNum].name, GELD_GREY2, ILI9341_CYAN, 10);
			} else {
				// no device connected
				tft.fillRoundRect(col * (labelWidth+6) + 5, row * (labelHeigth+5) + 32, labelWidth, labelHeigth, 3, GELD_GREY2);
				console.printTextAt(col * (labelWidth+6) + 9, row * (labelHeigth+5) + 39, text, ILI9341_BLACK, GELD_GREY2, 8);
				console.printTextAt(col * (labelWidth+6) + 30, row * (labelHeigth+5) + 37, "[EMPTY]", ILI9341_BLACK, GELD_GREY2, 10);
			}
		}
	}
}


void connectedDevice_Selector_here_DC() {
	// select a deviceNum from displayed connectedDevice-grid and store in gSelected_connectedDevice

	// make that vars global? --> also used in display_DeviceGrid()...
	int gridCols = 2; int gridRows = 8; // gridCols that work out-of-the-box: 1,2,3,4
	int labelWidth = 288/gridCols; int labelHeigth = 20;

	int thisPage_selectedDeviceID = gSelected_connectedDevice_here_DC - gDisplayGrid_startDeviceID_here_DC; // a.k.a. position on this page of grid
	tft.drawRoundRect((thisPage_selectedDeviceID % gridCols) * (labelWidth+6) + 4, ( thisPage_selectedDeviceID / gridCols) * (labelHeigth+5) + 31, labelWidth+2, labelHeigth+2,3, GELD_RED);

	if (encoderChange(DOWN)) {
		tft.drawRoundRect((thisPage_selectedDeviceID % gridCols) * (labelWidth+6) + 4, ( thisPage_selectedDeviceID / gridCols) * (labelHeigth+5) + 31, labelWidth+2, labelHeigth+2,3, GELD_BLACK);
	 	gSelected_connectedDevice_here_DC++;
	 	if(gSelected_connectedDevice_here_DC > 31){
			gSelected_connectedDevice_here_DC = 0; // close ID-circle
			gDisplayGrid_startDeviceID_here_DC = 0; // close page-circle
			display_DeviceGridPage_here_DC(gridCols, gridRows, labelWidth, labelHeigth);
		}
		if (gSelected_connectedDevice_here_DC - gDisplayGrid_startDeviceID_here_DC >= (gridCols * gridRows) ) {
			// next page
			gDisplayGrid_startDeviceID_here_DC += (gridCols * gridRows);
			display_DeviceGridPage_here_DC(gridCols, gridRows, labelWidth, labelHeigth);
		}
		thisPage_selectedDeviceID = gSelected_connectedDevice_here_DC - gDisplayGrid_startDeviceID_here_DC; // recalc position on this page of grid
		tft.drawRoundRect((thisPage_selectedDeviceID % gridCols) * (labelWidth+6) + 4, ( thisPage_selectedDeviceID / gridCols) * (labelHeigth+5) + 31, labelWidth+2, labelHeigth+2,3, GELD_RED);
	}

	if (encoderChange(UP)) {
		tft.drawRoundRect((thisPage_selectedDeviceID % gridCols) * (labelWidth+6) + 4, ( thisPage_selectedDeviceID / gridCols) * (labelHeigth+5) + 31, labelWidth+2, labelHeigth+2,3, GELD_BLACK);
	 	gSelected_connectedDevice_here_DC--;
	 	if (gSelected_connectedDevice_here_DC < 0) {
			gSelected_connectedDevice_here_DC = 31; // close ID-circle
			gDisplayGrid_startDeviceID_here_DC = 32 - (gridCols * gridRows); // close page-circle
			display_DeviceGridPage_here_DC(gridCols, gridRows, labelWidth, labelHeigth);
		}
		if (gSelected_connectedDevice_here_DC - gDisplayGrid_startDeviceID_here_DC < 0 ) {
			// previous page
			gDisplayGrid_startDeviceID_here_DC -= (gridCols * gridRows);
			display_DeviceGridPage_here_DC(gridCols, gridRows, labelWidth, labelHeigth);
		}
		thisPage_selectedDeviceID = gSelected_connectedDevice_here_DC - gDisplayGrid_startDeviceID_here_DC; // recalc position on this page of grid
		tft.drawRoundRect((thisPage_selectedDeviceID % gridCols) * (labelWidth+6) + 4, ( thisPage_selectedDeviceID / gridCols) * (labelHeigth+5) + 31, labelWidth+2, labelHeigth+2,3, GELD_RED);
	}
}

void display_gSelected_connectedDevice(){
	// display selected devices channels + fader
	sprintf(gTitle, " Connected Device #%02d %s (Chn %d..%d)",
		gSelected_connectedDevice_here_DC + 1,
		connectedDevices[gSelected_connectedDevice_here_DC].name,
		connectedDevices[gSelected_connectedDevice_here_DC].startChannel+1,
		connectedDevices[gSelected_connectedDevice_here_DC].startChannel + devicesAvailable[connectedDevices[gSelected_connectedDevice_here_DC].deviceModelID].numChannels
	);
	//console.printHeader(title);

	// tft.setFont(FONT_8);
	int channelStartOffset = 0;
	reset_potiVals(channelStartOffset);
	console.update_gDisplaySliderBars(channelStartOffset, gSelected_connectedDevice_here_DC);
	console.initAndDisplay_SliderBarGroup(NUM_FADER, gTitle);
	while(true){
		backgroundFuncs();

		// check encoder
		if (encoderChange(UP)) {
			channelStartOffset = (channelStartOffset + NUM_FADER > devicesAvailable[connectedDevices[gSelected_connectedDevice_here_DC].deviceModelID].numChannels) ? 0 : channelStartOffset + NUM_FADER;
			gDoRefresh = true;
			reset_potiVals(channelStartOffset);
			console.printHeader(gTitle);
		}

		if (encoderChange(DOWN)) {
			channelStartOffset = (channelStartOffset == 0) ? (devicesAvailable[connectedDevices[gSelected_connectedDevice_here_DC].deviceModelID].numChannels - 1) / NUM_FADER * NUM_FADER: channelStartOffset - NUM_FADER;
			gDoRefresh = true;
			reset_potiVals(channelStartOffset);
			console.printHeader(gTitle);
		}

		readAndUpdate_DeviceControl_PotiVals(channelStartOffset);
		if(gDoRefresh){
			console.update_gDisplaySliderBars(channelStartOffset, gSelected_connectedDevice_here_DC);
			console.refresh_SliderBarGroup(NUM_FADER);
			gDoRefresh = false;
		}

		// check buttons
		// TODO: @andreas: SELECT kann zb ein "add to patchbay" machen, in verbindung mit den 1-8 buttons
		//                 dann die patchbay per grid/encoder wählen (reverse-analog zum vorgehen in der patchbay.ino)
		if (buttonPressed(EXIT))  {
			break;            // Console beenden!
		}
	}
}
// ENDE display + interaction functions:

void readAndUpdate_DeviceControl_PotiVals(uint8_t channelStartOffset){
	// reads fader-positions of connectedDevices[deviceNum]'s channels, +channelOffset
	//       and updates relevant Dmx_Channels[].faderValue
	for (int i = 0; i < NUM_FADER ; i++) {
		console_poti_map(i, MAIN);
		// potiVal[i]: 0..1024  potiRead[i]: 0..255
		potiVal[i] = poti_smooth(potiVal[i], i);
		potiRead[i] = poti_catch_val(poti_map_8bit(potiVal[i]), Dmx_Channels[connectedDevices[gSelected_connectedDevice_here_DC].startChannel + channelStartOffset + i].faderValue, poti_catch_firstCall[i], i); // wenn helligkeit
		poti_catch_firstCall[i] = false;

		if (potiRead[i] <= 0) potiRead[i] = 0 ;		// lower limit Korrektur.

		if (Dmx_Channels[connectedDevices[gSelected_connectedDevice_here_DC].startChannel + channelStartOffset + i].faderValue != potiRead[i]) {	// erfasse potiaenderung nur wenn neuer wert vorliegt.
			Dmx_Channels[connectedDevices[gSelected_connectedDevice_here_DC].startChannel + channelStartOffset + i].faderValue = potiRead[i]; // store global fader values:
			console.update_singleBar_in_gDisplaySliderBars(channelStartOffset, i, gSelected_connectedDevice_here_DC);
			console.refresh_SliderBar_inGroup(i);
		}
	}
}
