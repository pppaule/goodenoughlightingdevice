![alt text](geldlogo.png "G.el.l.e")

# Good Enough Lighting Device
Simple Teensy 3.2 based DMX-Controller.
Features TFT-control, grouping, EEPROM-save/restore and a fixture library.

## Description
The Idea is to build a simple lighting controller from easily accessible electronic parts and open code. All hard- and software documentation are produced or at least usable with open source tools and under open source licenses. It's a tough time for hardware-projects open or not these days, as what parts are available by the time might be unavailable the next day due to supply chain problems. The deal about the good enough lighting device being open is, to develop a robust and reliable tool, that doesn't depend on the availability of certain parts or some company server to download its fixture database, which is actually repairable and able to be extended with what you actually want and not somebody else. and if you feel to build your own effects, just go for it, and actually create your own rahmenbedingungen for your work. 

Quick-BOM (far from complete but the essentials):

Teensy 3.2  
2.4" ILI9341 TFT  
Rotary Encoder  
8 Fader-Potentiometers  
10 tactile push-buttons  

ic's:  
MCP3008 Analog Digital Converter  
PCA1525 Reference Voltage  
MCP23017 16 port I/O  
PCA9956 (optional) pwm led controller (1khz / 4 ports spare)  
MAX481 (RS-485 Transceiver)  

## Versioning
0.8rnd  All systems working.

## Issues
-   EEPROMrestoreFunction: does freeze when used on fresh Teensy. Workaroud, disable for first Flash, then reenable to restore EEPROM contents o boot-time.  
- patchbay_selector: device outlines falsely jump to next page when scrolling through devices.
- devices shall be added one by one. if you have to reconfigure, delete from the top of the stack until you come to the start of your problem and reconfigure from there.

## Installation
Install Atom and PlatformIO-Framework as according to this tutorial(link). It's running on Windows, Mac OS and Linux but I can't guarantee it's compiling on your platform.

## Authors and acknowledgment
Daniel Pauselius  
Andreas Hoffmann  
Hannes Waldschütz  

## Licence
Firmware is GPL-v3 (https://www.gnu.org/licenses/gpl-3.0.en.html)  
Documentation is CC- (link)  
Hardware is CERN Open Hardware Licence (https://ohwr.org/cernohl)  

![alt text](schematic.png ", gelle")
